# Master thesis source code and test results #

The repository has the following folder structure


Folder | Description
------------- | -------------
Results  | Result of experimental test runs
TestExecuter  | Source code for Java application of the TestExecuter
Services | Source code for the services of the architectural prototype. Including resources for building Docker images
JMeter | JMeter test and resources for building JMeter Docker image
Analyser | Java application that processes payload output of an experimental test run, and generate the data used for the Chord diagrams of Figure 16, Figure 17, Figure 19 and Figure 20

The content of the repository has been copied from other private repositories to this one, solely for the purpose of make it public as part of the paper. For this reason CI/CD is not setup or enabled for this repository.