00:00 ------------------ CONFIGURATION---------------------
00:00 SwarmNode count                10   
00:00 TestNode count                 6    
00:00 Swarm deployment               true 
00:00 Encrypted                      false
00:00 Manual test                    false
00:00 Churn enabled                  false
00:00 Churn type                     GRACEFUL
00:00 Stop grace period              600000000000
00:00 Gracefull shutdown             false
00:00 Gracefull shutdown timeout     600  
00:00 Keep-alive                     true 
00:00 Test duration                  300  
00:00 Test Threads                   100  
00:00 Test rampup                    30   
00:00 Disable load balancer          false
00:00 Enable Retry                   false
00:00 JMeter mode                    StrippedBatch
00:00 Get service logs               true 
00:00 -----------------------------------------------------
00:00 Run 10 EC2 instances for Docker Swarm cluster
00:01 EC2 Swarm Instances starting...
00:13 EC2 Swarm Instances started
00:14 Run 6 EC2 instances for JMeter cluster
00:15 EC2 JMeter instances starting...
00:23 Done!
00:23 Create loadbalancer
00:24 Created loadbalancer: Master-221409034.eu-west-1.elb.amazonaws.com
00:24 Adding instances to loadbalancer
00:25 Added 10 instances to loadbalancer
00:25 Waiting for all Docker instances to answer
00:32 Docker ready on all instances
00:33 Swarm initialized by ec2-52-18-152-240.eu-west-1.compute.amazonaws.com
00:34 Swarm manager added ec2-34-252-221-82.eu-west-1.compute.amazonaws.com
00:34 Swarm manager added ec2-34-242-240-178.eu-west-1.compute.amazonaws.com
00:35 Swarm worker added  ec2-52-213-201-154.eu-west-1.compute.amazonaws.com
00:35 Swarm worker added  ec2-34-247-164-77.eu-west-1.compute.amazonaws.com
00:35 Swarm worker added  ec2-34-246-200-180.eu-west-1.compute.amazonaws.com
00:36 Swarm worker added  ec2-34-248-0-2.eu-west-1.compute.amazonaws.com
00:36 Swarm worker added  ec2-54-246-213-16.eu-west-1.compute.amazonaws.com
00:36 Swarm worker added  ec2-34-248-170-241.eu-west-1.compute.amazonaws.com
00:37 Swarm worker added  ec2-34-245-97-252.eu-west-1.compute.amazonaws.com
00:37 Starting pulling prototype images for all servers
00:37 Starting to pull images for ec2-52-18-152-240.eu-west-1.compute.amazonaws.com
00:37 Starting to pull images for ec2-34-252-221-82.eu-west-1.compute.amazonaws.com
00:37 Starting to pull images for ec2-34-242-240-178.eu-west-1.compute.amazonaws.com
00:37 Starting to pull images for ec2-52-213-201-154.eu-west-1.compute.amazonaws.com
00:37 Starting to pull images for ec2-34-247-164-77.eu-west-1.compute.amazonaws.com
00:37 Starting to pull images for ec2-34-246-200-180.eu-west-1.compute.amazonaws.com
00:37 Starting to pull images for ec2-34-248-0-2.eu-west-1.compute.amazonaws.com
00:37 Starting to pull images for ec2-54-246-213-16.eu-west-1.compute.amazonaws.com
00:37 Starting to pull images for ec2-34-248-170-241.eu-west-1.compute.amazonaws.com
00:37 Starting to pull images for ec2-34-245-97-252.eu-west-1.compute.amazonaws.com
00:59 Finished pulling prototype images for all servers
00:59 Deploying containers to Swarm
00:59 Using un-encrypted overlay network
01:04 Creating service log: AUTH.log
01:09 Creating service log: SERVICE2.log
01:13 Creating service log: SERVICE3.log
01:17 Creating service log: SERVICE1.log
01:17 Finished Deploying containers to Swarm
01:17 Wait until all instances are Healthy
02:22 All instances are InService for loadbalancer
02:22 Waiting for all Docker instances to answer
02:23 Docker ready on all instances
02:23 Starting pulling test images for all servers
02:23 Starting to pull images for ec2-54-194-13-4.eu-west-1.compute.amazonaws.com
02:23 Starting to pull images for ec2-34-241-39-24.eu-west-1.compute.amazonaws.com
02:23 Starting to pull images for ec2-54-246-183-242.eu-west-1.compute.amazonaws.com
02:23 Starting to pull images for ec2-54-171-190-123.eu-west-1.compute.amazonaws.com
02:23 Starting to pull images for ec2-54-246-234-195.eu-west-1.compute.amazonaws.com
02:23 Starting to pull images for ec2-34-252-232-248.eu-west-1.compute.amazonaws.com
02:32 Finished pulling test images for all servers
02:32 Starting test against Master-221409034.eu-west-1.elb.amazonaws.com
02:38 Start test slave 1 on ec2-54-194-13-4.eu-west-1.compute.amazonaws.com
02:41 Start test slave 2 on ec2-34-241-39-24.eu-west-1.compute.amazonaws.com
02:42 Start test slave 3 on ec2-54-246-183-242.eu-west-1.compute.amazonaws.com
02:43 Start test slave 4 on ec2-54-171-190-123.eu-west-1.compute.amazonaws.com
02:44 Start test slave 5 on ec2-54-246-234-195.eu-west-1.compute.amazonaws.com
02:47 Client instance is ec2-34-252-232-248.eu-west-1.compute.amazonaws.com
02:47 Running test...
07:59 Test Done
08:00 Getting Docker log: bechhansen-master-slave_latest-exited-652a6ef5cde0022f0bf367a01b6a324dbad2cbfb287db1eef2d0d447c5de89bc.log
08:01 Getting Docker log: bechhansen-master-slave_latest-exited-bc9c84e47ea555f5530c30a6ad68557e66cfa8d8e4ff4867b9731a38ffa4e5b2.log
08:01 Getting Docker log: bechhansen-master-slave_latest-exited-8d8ddecf55fe3f08ac9103ad9340b3a08355d3155558773e6290da9517247a1c.log
08:02 Getting Docker log: bechhansen-master-slave_latest-exited-d306a723373abbeb91e6f722bc18e0bade156e6d64ed5082ae669d86935f65d4.log
08:02 Getting Docker log: bechhansen-master-slave_latest-exited-6b8b3ef14d280f924c49f9b426e153c3af8ece87f8e70240009488e2f4bb44d9.log
08:03 Getting Docker log: bechhansen-master-client_latest-exited-9d40a912df1a8a2bc2a38d26f559b6dcad5f24ad25e7c4416be8c97589c85277.log
08:03 Getting Docker log: bechhansen-master-service1_1-running-a242f877e0f14c8379116bbabab2e7feee0f7394387a98d294c5c82f163472f5.log
08:03 Getting Docker log: bechhansen-master-service3_1-running-66c9dc02a8001b09b2ed8fe37e6aec9b743398d91afbd4d7d5f6074fb7381fab.log
08:03 Getting Docker log: bechhansen-master-service2_1-running-78cfde7bda2197fdbdb05803f70d9900494628c506e37d2e166d23ec6cf82b85.log
08:04 Getting Docker log: bechhansen-master-auth_1-running-5eeb403d4eb407cca3058c699490149b3718c78fc2b6885111b84bfb435cb04a.log
08:04 Getting Docker log: bechhansen-master-service1_1-running-1befce1d5b971b2c26d4bf4ee3cded41628b818111710f7267cb069b5696bc1a.log
08:04 Getting Docker log: bechhansen-master-service3_1-running-588ea6489cf2bb72cd711c18f8d5241ea538a71d809969af6bc8f16f9a5ff26f.log
08:05 Getting Docker log: bechhansen-master-service2_1-running-f77dc8a007125d35bbbcb984928489490d3a0ad4726312cd91fe9010ae1f56dd.log
08:05 Getting Docker log: bechhansen-master-auth_1-running-549538da5ab50e41f238c794f1467f16b5697f41060c2217fc3eb4c5d0c36cbf.log
08:05 Getting Docker log: bechhansen-master-service1_1-running-d215225c70573ee327ed9270285ae06c2976afc802fa91b38fa197561af99ae0.log
08:05 Getting Docker log: bechhansen-master-service3_1-running-0bd88a7344b22b91300e106697ffe480e270957b3fdd26aee54713051b2f599d.log
08:06 Getting Docker log: bechhansen-master-service2_1-running-40d37730f06aad4c00b9cd26e3326f7a77e87c26892c11d298490d3877e30c49.log
08:06 Getting Docker log: bechhansen-master-auth_1-running-4d6ed502fcbd7a7faa26827d7d131e41697090734b9e31e3dc6cbbbe53b3c47f.log
08:06 Getting Docker log: bechhansen-master-service1_1-running-3883beb000c01f7b474271b9d47a3950fa59032c6e53430486473f004bcba4e3.log
08:07 Getting Docker log: bechhansen-master-service3_1-running-fb78814450c71051363d2eb4660b14f9d3f1b95bfb900c670e50cd38299f882e.log
08:07 Getting Docker log: bechhansen-master-service2_1-running-05d53bc7262338388fe65fcb5e18ab64ae2425c9347159988894d175141f9414.log
08:07 Getting Docker log: bechhansen-master-auth_1-running-f9338ea2269ef0fb4314f8a8fd1fb758143a7953fc1318882a4817e11a956dd9.log
08:07 Getting Docker log: bechhansen-master-service1_1-running-b797c37722a0681133ac8b1d43c8626c7265859a5840cd89953e9610005bc60b.log
08:08 Getting Docker log: bechhansen-master-service3_1-running-ddbca1abbf60b25f3b2702736d76981543610ce4f4b26c77009673d16a9f4474.log
08:08 Getting Docker log: bechhansen-master-service2_1-running-88187e4b4b4f0f09330b5c11b1affac43369fd13b18d790430db24f4c6f10fae.log
08:08 Getting Docker log: bechhansen-master-auth_1-running-ccae67c914b1cfa9a1a42160fc7b7eae2cd57367937966a2edc092929381c086.log
08:09 Getting Docker log: bechhansen-master-service1_1-running-fe24970ac3c444266f9d774756306895119475bb7543f8cc4476d5bfb007d234.log
08:09 Getting Docker log: bechhansen-master-service3_1-running-59d7514a6d3299dc97dc1b7ffb84641354d9c15863c5b690f3b34a917e984efb.log
08:09 Getting Docker log: bechhansen-master-service2_1-running-9a43c57c86b70077ca9db786b304cf29ae6a1b49960f991fbdb11e81ee89d364.log
08:09 Getting Docker log: bechhansen-master-auth_1-running-37bf5d3335b51102cb140d0ffe2cd949593cdba0f847e4f937e5a9e2305356e5.log
08:10 Getting Docker log: bechhansen-master-service1_1-running-d19154d93ce24413db22ced518e64a55a578914c3b5c59691421cf03b1f145e1.log
08:10 Getting Docker log: bechhansen-master-service3_1-running-fe0bf1327c278a41adc9306c54a7ede0e1ec00de3a970c3a04854ef59aa9bafb.log
08:10 Getting Docker log: bechhansen-master-service2_1-running-c7cadf6f4fbb8de520063a4f34cdd33f54bcdcc33d77a8d0c6a90ffb0483b4cd.log
08:10 Getting Docker log: bechhansen-master-auth_1-running-f65bfc862a8345ecacf6e0614c1e7bfbcc2dbff64bbc0326041013254b01486b.log
08:11 Getting Docker log: bechhansen-master-service1_1-running-a077dfc5a4c86202614332a9f25cbb9d62435c14f2a3c38b81976271ea5a5df5.log
08:11 Getting Docker log: bechhansen-master-service3_1-running-ebb73d7af5247787f8936245c4ed1c86e87ae2e3a4cfc6bcb7551b5b31a833b2.log
08:11 Getting Docker log: bechhansen-master-service2_1-running-2bc53a6c2916c927068754f06bedf96e91aef68e065e420f74b5ce8365d10b17.log
08:12 Getting Docker log: bechhansen-master-auth_1-running-d3a8b655c4836d7a994caf2724956ece7735b6141e904bdb3c179172a3b992ae.log
08:12 Getting Docker log: bechhansen-master-service1_1-running-d76c546acff80878b87c92859bca74db003b8b3cd6c54bd3191c35fcef764b2f.log
08:12 Getting Docker log: bechhansen-master-service3_1-running-639b1014a342ab2293e2b4de8fc80ac9410613e9b7b47ce4cb579d96c607b138.log
08:12 Getting Docker log: bechhansen-master-service2_1-running-c300b7b5561ea4e65809ea9d4057ceaf4bbf30df07cae35bc13f1b60ff2a0f1c.log
08:13 Getting Docker log: bechhansen-master-auth_1-running-4933c2e84c9ec4e0b91b8cf7cbc5df4f5ab948b82ffba1939b8358ddfaf99f41.log
08:13 Getting Docker log: bechhansen-master-service1_1-running-f2a0fb7b18b7ca0a73b1e82c82d9a5f26dfa8b643506f6e58240038027bcadc8.log
08:13 Getting Docker log: bechhansen-master-service3_1-running-48cfe2a87f64ce448ab903000b04d03c365b01c5137c9f0a2901770faf5f4b29.log
08:14 Getting Docker log: bechhansen-master-service2_1-running-fe1d7f76f488eba8222472eabb40cb7edb7f7e3206804dc52b4d48e11546c10d.log
08:14 Getting Docker log: bechhansen-master-auth_1-running-b551027f182d48aba762050afdf08359ca7f61c788d0ebe609b6021f0bbb1362.log
08:14 End log {SERVICE2=10, SERVICE3=10, SERVICE1=10, AUTH=10}
08:14 Terminating all EC2 instances
08:15 Terminated all EC2 instances
