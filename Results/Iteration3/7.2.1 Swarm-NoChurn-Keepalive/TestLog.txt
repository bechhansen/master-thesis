00:00 ------------------ CONFIGURATION---------------------
00:00 SwarmNode count                10   
00:00 TestNode count                 6    
00:00 Swarm deployment               true 
00:00 Encrypted                      false
00:00 Manual test                    false
00:00 Churn enabled                  false
00:00 Churn type                     GRACEFUL
00:00 Stop grace period              600000000000
00:00 Gracefull shutdown             false
00:00 Gracefull shutdown timeout     600  
00:00 Keep-alive                     true 
00:00 Test duration                  300  
00:00 Test Threads                   100  
00:00 Test rampup                    30   
00:00 Disable load balancer          false
00:00 Enable Retry                   false
00:00 JMeter mode                    StrippedBatch
00:00 Get service logs               true 
00:00 -----------------------------------------------------
00:00 Run 10 EC2 instances for Docker Swarm cluster
00:01 EC2 Swarm Instances starting...
00:09 EC2 Swarm Instances started
00:09 Run 6 EC2 instances for JMeter cluster
00:10 EC2 JMeter instances starting...
00:18 Done!
00:18 Create loadbalancer
00:19 Created loadbalancer: Master-701383053.eu-west-1.elb.amazonaws.com
00:19 Adding instances to loadbalancer
00:20 Added 10 instances to loadbalancer
00:20 Waiting for all Docker instances to answer
00:28 Docker ready on all instances
00:29 Swarm initialized by ec2-34-244-72-19.eu-west-1.compute.amazonaws.com
00:30 Swarm manager added ec2-52-213-95-165.eu-west-1.compute.amazonaws.com
00:30 Swarm manager added ec2-54-154-193-232.eu-west-1.compute.amazonaws.com
00:31 Swarm worker added  ec2-52-215-17-73.eu-west-1.compute.amazonaws.com
00:31 Swarm worker added  ec2-54-154-181-136.eu-west-1.compute.amazonaws.com
00:31 Swarm worker added  ec2-54-171-181-78.eu-west-1.compute.amazonaws.com
00:32 Swarm worker added  ec2-52-212-228-103.eu-west-1.compute.amazonaws.com
00:32 Swarm worker added  ec2-54-77-40-78.eu-west-1.compute.amazonaws.com
00:32 Swarm worker added  ec2-54-154-28-54.eu-west-1.compute.amazonaws.com
00:33 Swarm worker added  ec2-52-30-64-193.eu-west-1.compute.amazonaws.com
00:33 Starting pulling prototype images for all servers
00:33 Starting to pull images for ec2-34-244-72-19.eu-west-1.compute.amazonaws.com
00:33 Starting to pull images for ec2-52-213-95-165.eu-west-1.compute.amazonaws.com
00:33 Starting to pull images for ec2-54-154-193-232.eu-west-1.compute.amazonaws.com
00:33 Starting to pull images for ec2-52-215-17-73.eu-west-1.compute.amazonaws.com
00:33 Starting to pull images for ec2-54-154-181-136.eu-west-1.compute.amazonaws.com
00:33 Starting to pull images for ec2-54-171-181-78.eu-west-1.compute.amazonaws.com
00:33 Starting to pull images for ec2-52-212-228-103.eu-west-1.compute.amazonaws.com
00:33 Starting to pull images for ec2-54-77-40-78.eu-west-1.compute.amazonaws.com
00:33 Starting to pull images for ec2-54-154-28-54.eu-west-1.compute.amazonaws.com
00:33 Starting to pull images for ec2-52-30-64-193.eu-west-1.compute.amazonaws.com
00:53 Finished pulling prototype images for all servers
00:53 Deploying containers to Swarm
00:53 Using un-encrypted overlay network
00:58 Creating service log: AUTH.log
01:03 Creating service log: SERVICE2.log
01:07 Creating service log: SERVICE3.log
01:12 Creating service log: SERVICE1.log
01:12 Finished Deploying containers to Swarm
01:12 Wait until all instances are Healthy
01:57 All instances are InService for loadbalancer
01:57 Waiting for all Docker instances to answer
01:57 Docker ready on all instances
01:57 Starting pulling test images for all servers
01:57 Starting to pull images for ec2-52-16-57-179.eu-west-1.compute.amazonaws.com
01:57 Starting to pull images for ec2-34-253-200-97.eu-west-1.compute.amazonaws.com
01:57 Starting to pull images for ec2-54-171-51-39.eu-west-1.compute.amazonaws.com
01:57 Starting to pull images for ec2-34-245-93-163.eu-west-1.compute.amazonaws.com
01:57 Starting to pull images for ec2-63-32-95-42.eu-west-1.compute.amazonaws.com
01:57 Starting to pull images for ec2-3-248-210-61.eu-west-1.compute.amazonaws.com
02:07 Finished pulling test images for all servers
02:07 Starting test against Master-701383053.eu-west-1.elb.amazonaws.com
02:08 Start test slave 1 on ec2-52-16-57-179.eu-west-1.compute.amazonaws.com
02:11 Start test slave 2 on ec2-34-253-200-97.eu-west-1.compute.amazonaws.com
02:11 Start test slave 3 on ec2-54-171-51-39.eu-west-1.compute.amazonaws.com
02:13 Start test slave 4 on ec2-34-245-93-163.eu-west-1.compute.amazonaws.com
02:13 Start test slave 5 on ec2-63-32-95-42.eu-west-1.compute.amazonaws.com
02:17 Client instance is ec2-3-248-210-61.eu-west-1.compute.amazonaws.com
02:17 Running test...
07:30 Test Done
07:31 Getting Docker log: bechhansen-master-slave_latest-exited-2e1d6423ab198d25abac415d7146e258712ca55ed1590e3f5cf70741d2ea39c7.log
07:32 Getting Docker log: bechhansen-master-slave_latest-exited-f486137b2fda8b46fe2fa0e80274c092c71270cb6c62ae94c6c5ab9fef9c88f9.log
07:32 Getting Docker log: bechhansen-master-slave_latest-exited-e9b21700682e60210f3a286ccaf4e812239d017663277464960be0fbb4d73577.log
07:32 Getting Docker log: bechhansen-master-slave_latest-exited-b742e7b40c35aca7a2facd1b0fc3550d45be372a5749c115404ecf10658ce579.log
07:33 Getting Docker log: bechhansen-master-slave_latest-exited-c9b93efabc0b3fd5346efa6080cd7ea950ecbb7e154d1d54b2a42f090ddb2e67.log
07:33 Getting Docker log: bechhansen-master-client_latest-exited-7bc59b65cfa6ca6c812d3e512c7efec19db6d5bd9939502e0fb0e1fcaa1ba177.log
07:34 Getting Docker log: bechhansen-master-service1_1-running-999ad6a67367b3251231be65e8f2f061b50d8b23f9d6f5c4376fe6a6535d1597.log
07:34 Getting Docker log: bechhansen-master-service3_1-running-7528692d6951558e91e3910173e9ef7810b0b06f9dd9d07e40eb5077dc94e171.log
07:34 Getting Docker log: bechhansen-master-service2_1-running-a96f02397af87c6f14eb163aae9338039b4e2c62644169e4c633f134816ee1be.log
07:34 Getting Docker log: bechhansen-master-auth_1-running-1165cd96436e077a2cf42828299a7c46c766503ea9396af0c142c5492c74a645.log
07:35 Getting Docker log: bechhansen-master-service1_1-running-b26524af75e920327b95bf4a29ac4810615c5c8c204679f85d16d596bb2df5ac.log
07:35 Getting Docker log: bechhansen-master-service3_1-running-0eb1d1c070807460d2589c001b5238e8ae7884f906771ea4dbcc33324fe8103d.log
07:35 Getting Docker log: bechhansen-master-service2_1-running-371b9be3e7d66842df27170ffc6e2de09faae54294a3f74ddc7e9cfce0073c61.log
07:36 Getting Docker log: bechhansen-master-auth_1-running-16f2f70118ef4e23fa2618502645a261ec77b974e3c279d46694d43fe1e27825.log
07:36 Getting Docker log: bechhansen-master-service1_1-running-28884a347eecf5f257fa52cdca63677acdc18f8681b7a3c8f1ed49967b810cc5.log
07:36 Getting Docker log: bechhansen-master-service3_1-running-1b549cd8912060c42c3f4eccb90364854d43baa05aacd9e9c44ff44ab51bce3c.log
07:37 Getting Docker log: bechhansen-master-service2_1-running-bfedd27039f49f8d27b24c187eecece755b0fbb454eb86d64993d4e4fbe7196f.log
07:37 Getting Docker log: bechhansen-master-auth_1-running-fa5f25c575c381244918ea7ccc203de74ce5217dc23e91de4bec4dbc605df002.log
07:37 Getting Docker log: bechhansen-master-service1_1-running-0f8eef17cb1bfa52408ed587d6a349114d14e9a844fc27514a3cc91849450bce.log
07:37 Getting Docker log: bechhansen-master-service3_1-running-970aeec10c5310476cf9a12f6d4c5cd51622026cb35366fb3d3e8a575c953ecc.log
07:38 Getting Docker log: bechhansen-master-service2_1-running-ccdf7bf6b42926732b163bdc6d46da6ca35e45a15b6074e7fe45d06d80200344.log
07:38 Getting Docker log: bechhansen-master-auth_1-running-b28b766558b20dffeb5a1100d3c501c58ffe0ead16e3606382aede8cc7465915.log
07:38 Getting Docker log: bechhansen-master-service1_1-running-3e4b7103414061c9370465fd1080ed8b73b190912f62a8dabb122b8e01b340e5.log
07:39 Getting Docker log: bechhansen-master-service3_1-running-d5af16d739ef81ee57ed89d76163194915fe5bba0e6d2978bf2b72c8b71107b8.log
07:39 Getting Docker log: bechhansen-master-service2_1-running-7e01db6a2f1b64dbbca21d8df79684ad860ca81f37f8deb31d38cd0a45875751.log
07:39 Getting Docker log: bechhansen-master-auth_1-running-403eddb74f5c182a35be91eff8082da8ec1b306425aa0313e3fb41660f98a45b.log
07:39 Getting Docker log: bechhansen-master-service1_1-running-7617954b853d6142e250732228119d91485a19906fd38b330bf6139c90f6acd3.log
07:40 Getting Docker log: bechhansen-master-service3_1-running-c411f52a688b78666c5721772adc3a35360ab63401b115469dca4487adf38712.log
07:40 Getting Docker log: bechhansen-master-service2_1-running-4166fdb526539dedd714718f9bb54270b9a176344ce7faf922ed7541357dcc1c.log
07:40 Getting Docker log: bechhansen-master-auth_1-running-b7228fbd0e3e432d4e6f5b964b95ba92d3f7e2f7255134ebb80f5afe7caa404c.log
07:41 Getting Docker log: bechhansen-master-service1_1-running-55b14fccafb2d09ecb28bf7cd2d6acefb1eef772fa7dd1d6d53f93bb3b543a8a.log
07:41 Getting Docker log: bechhansen-master-service3_1-running-6f1f4ab08a68d067408c2e25c6b966ace536a3f8b5ad21f0a94c416ada1f85cb.log
07:41 Getting Docker log: bechhansen-master-service2_1-running-f1abc7ea5f8561de2907d5cf9b6617bf58499fe1c0322d96d6c0f58340e434ed.log
07:41 Getting Docker log: bechhansen-master-auth_1-running-53be3c424ad7a8f01d1c8b293a382822bee0b04c4071f00828d2e2789f015dd6.log
07:42 Getting Docker log: bechhansen-master-service1_1-running-8444a7a384a95ded4c6ccbf181f1e23dd9d07fd9be5097284412814b5dc8a8bd.log
07:42 Getting Docker log: bechhansen-master-service3_1-running-900442ede78931409b716e619fafe23aa975d75168591ca0886f5b03fee298fc.log
07:43 Getting Docker log: bechhansen-master-service2_1-running-9310842a86440bd6bc48c471faa403962cd74c1a125c312e9d1da82cb98d854d.log
07:43 Getting Docker log: bechhansen-master-auth_1-running-2970e5584c99fe67c40e2917025e27d08e4cfe6997db8e41d1aa102195ad8c81.log
07:43 Getting Docker log: bechhansen-master-service1_1-running-fa7421a6c55a9eaf6963c46c792c81dd688ba2add8e9b12cd2b4d4fb4e425ca4.log
07:44 Getting Docker log: bechhansen-master-service3_1-running-9c988a974cd9a2e2c133126c94c2605bf747482c7e776b7374811536262c50b2.log
07:44 Getting Docker log: bechhansen-master-service2_1-running-3e55d4c68f2950f70ed4dc8442914717f4f23a5b88e2ec1b1c8ddedc1a638fcf.log
07:44 Getting Docker log: bechhansen-master-auth_1-running-46550b862af7789ebfc8add51b131a320029e2ac2246ae9db10bc24a9caf0fcc.log
07:45 Getting Docker log: bechhansen-master-service1_1-running-5a3eec95b6e383585e90f3b9317911ebc1a5ac3cbf543000a85e065bd3bf5146.log
07:45 Getting Docker log: bechhansen-master-service3_1-running-cae22817eb6e6bf5da9844b5c528f34f5319c5c6572a20c1bcb092d12a0f4839.log
07:45 Getting Docker log: bechhansen-master-service2_1-running-2851d341f7955a7198ae836a10aa4925f35a5936dac3892bf9ee973b7b31e307.log
07:45 Getting Docker log: bechhansen-master-auth_1-running-71c5fc2a5e9bfefbaf64055d2b53055f2d7b51bc1bcb3fc489eccefd7e794e65.log
07:46 End log {SERVICE2=10, SERVICE3=10, SERVICE1=10, AUTH=10}
07:46 Terminating all EC2 instances
07:47 Terminated all EC2 instances
