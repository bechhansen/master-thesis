00:00 ------------------ CONFIGURATION---------------------
00:00 SwarmNode count                10   
00:00 TestNode count                 6    
00:00 Swarm deployment               true 
00:00 Encrypted                      false
00:00 Manual test                    false
00:00 Churn enabled                  true 
00:00 Churn type                     GRACEFUL
00:00 Stop grace period              600000000000
00:00 Gracefull shutdown             true 
00:00 Gracefull shutdown timeout     600  
00:00 Keep-alive                     true 
00:00 Test duration                  300  
00:00 Test Threads                   100  
00:00 Test rampup                    30   
00:00 Disable load balancer          true 
00:00 Enable Retry                   false
00:00 JMeter mode                    StrippedBatch
00:00 Get service logs               true 
00:00 -----------------------------------------------------
00:00 Run 10 EC2 instances for Docker Swarm cluster
00:01 EC2 Swarm Instances starting...
00:10 EC2 Swarm Instances started
00:10 Run 6 EC2 instances for JMeter cluster
00:12 EC2 JMeter instances starting...
00:23 Done!
00:23 Create loadbalancer
00:24 Created loadbalancer: Master-1773761122.eu-west-1.elb.amazonaws.com
00:24 Adding instances to loadbalancer
00:25 Added 10 instances to loadbalancer
00:25 Waiting for all Docker instances to answer
00:26 Docker ready on all instances
00:27 Swarm initialized by ec2-52-48-159-40.eu-west-1.compute.amazonaws.com
00:28 Swarm manager added ec2-63-32-43-64.eu-west-1.compute.amazonaws.com
00:28 Swarm manager added ec2-34-245-28-115.eu-west-1.compute.amazonaws.com
00:29 Swarm worker added  ec2-34-247-159-91.eu-west-1.compute.amazonaws.com
00:29 Swarm worker added  ec2-34-249-116-241.eu-west-1.compute.amazonaws.com
00:29 Swarm worker added  ec2-34-254-192-35.eu-west-1.compute.amazonaws.com
00:30 Swarm worker added  ec2-34-247-67-222.eu-west-1.compute.amazonaws.com
00:30 Swarm worker added  ec2-52-17-116-39.eu-west-1.compute.amazonaws.com
00:31 Swarm worker added  ec2-54-154-204-41.eu-west-1.compute.amazonaws.com
00:31 Swarm worker added  ec2-34-250-203-35.eu-west-1.compute.amazonaws.com
00:31 Starting pulling prototype images for all servers
00:31 Starting to pull images for ec2-52-48-159-40.eu-west-1.compute.amazonaws.com
00:31 Starting to pull images for ec2-63-32-43-64.eu-west-1.compute.amazonaws.com
00:31 Starting to pull images for ec2-34-245-28-115.eu-west-1.compute.amazonaws.com
00:31 Starting to pull images for ec2-34-247-159-91.eu-west-1.compute.amazonaws.com
00:31 Starting to pull images for ec2-34-249-116-241.eu-west-1.compute.amazonaws.com
00:31 Starting to pull images for ec2-34-254-192-35.eu-west-1.compute.amazonaws.com
00:31 Starting to pull images for ec2-34-247-67-222.eu-west-1.compute.amazonaws.com
00:31 Starting to pull images for ec2-52-17-116-39.eu-west-1.compute.amazonaws.com
00:31 Starting to pull images for ec2-54-154-204-41.eu-west-1.compute.amazonaws.com
00:31 Starting to pull images for ec2-34-250-203-35.eu-west-1.compute.amazonaws.com
00:53 Finished pulling prototype images for all servers
00:53 Deploying containers to Swarm
00:53 Using un-encrypted overlay network
00:58 Creating service log: AUTH.log
01:03 Creating service log: SERVICE2.log
01:07 Creating service log: SERVICE3.log
01:11 Creating service log: SERVICE1.log
01:11 Finished Deploying containers to Swarm
01:11 Wait until all instances are Healthy
02:06 All instances are InService for loadbalancer
02:06 Waiting for all Docker instances to answer
02:06 Docker ready on all instances
02:06 Starting pulling test images for all servers
02:06 Starting to pull images for ec2-34-242-13-101.eu-west-1.compute.amazonaws.com
02:06 Starting to pull images for ec2-34-254-201-214.eu-west-1.compute.amazonaws.com
02:06 Starting to pull images for ec2-34-247-187-64.eu-west-1.compute.amazonaws.com
02:06 Starting to pull images for ec2-34-248-108-79.eu-west-1.compute.amazonaws.com
02:06 Starting to pull images for ec2-52-214-157-127.eu-west-1.compute.amazonaws.com
02:06 Starting to pull images for ec2-34-244-208-159.eu-west-1.compute.amazonaws.com
02:16 Finished pulling test images for all servers
02:16 Start GracefulChurnGenerator
02:16 Starting test against ec2-34-250-203-35.eu-west-1.compute.amazonaws.com
02:18 Start test slave 1 on ec2-34-242-13-101.eu-west-1.compute.amazonaws.com
02:20 Start test slave 2 on ec2-34-254-201-214.eu-west-1.compute.amazonaws.com
02:21 Start test slave 3 on ec2-34-247-187-64.eu-west-1.compute.amazonaws.com
02:23 Start test slave 4 on ec2-34-248-108-79.eu-west-1.compute.amazonaws.com
02:25 Start test slave 5 on ec2-52-214-157-127.eu-west-1.compute.amazonaws.com
02:29 Client instance is ec2-34-244-208-159.eu-west-1.compute.amazonaws.com
02:29 Running test...
02:47 Rollingupdate SERVICE1 to bechhansen/master-service1:2 - Order: START_FIRST. Parallelism: 2
02:52 Scale SERVICE2 to 11
02:52 SERVICE2 scaled
02:57 Rollingupdate SERVICE3 to bechhansen/master-service3:2 - Order: START_FIRST. Parallelism: 2
03:02 Scale AUTH to 11
03:02 AUTH scaled
03:22 Scale SERVICE2 to 10
03:23 SERVICE2 scaled
03:28 Rollingupdate SERVICE2 to bechhansen/master-service2:2 - Order: START_FIRST. Parallelism: 2
03:32 Scale AUTH to 10
03:33 AUTH scaled
03:39 Rollingupdate AUTH to bechhansen/master-auth:2 - Order: START_FIRST. Parallelism: 1
05:32 SERVICE1 rolled
05:37 Scale SERVICE1 to 11
05:38 SERVICE1 scaled
05:43 SERVICE3 rolled
05:48 Scale SERVICE3 to 11
05:49 SERVICE3 scaled
06:08 Scale SERVICE1 to 10
06:09 SERVICE1 scaled
06:14 Rollingupdate SERVICE1 to bechhansen/master-service1:1 - Order: START_FIRST. Parallelism: 2
06:15 SERVICE2 rolled
06:19 Scale SERVICE3 to 10
06:20 SERVICE3 scaled
06:20 Scale SERVICE2 to 11
06:21 SERVICE2 scaled
06:25 Rollingupdate SERVICE3 to bechhansen/master-service3:1 - Order: START_FIRST. Parallelism: 2
06:51 Scale SERVICE2 to 10
06:52 SERVICE2 scaled
06:58 Rollingupdate SERVICE2 to bechhansen/master-service2:1 - Order: START_FIRST. Parallelism: 2
07:42 Test Done
07:43 Getting Docker log: bechhansen-master-slave_latest-exited-de1ecddaf30ab0071e2944a37d62447ed1e76b4e9f93c22635532fef6996fea4.log
07:43 Getting Docker log: bechhansen-master-slave_latest-exited-1f223f2c727dc7aefa6d0f1f29f87ec1ef3d4a46cf49005fcb08d4fead8e355a.log
07:44 Getting Docker log: bechhansen-master-slave_latest-exited-e87f6cfad9b8eb773fb00630b389b1c8ffd7a12a9cd4d0ba096e3a7306e83e96.log
07:44 Getting Docker log: bechhansen-master-slave_latest-exited-c73d7ef868f88f46f16756361d4a7e40f391fc8221ba3164058eb3dc10023a0f.log
07:45 Getting Docker log: bechhansen-master-slave_latest-exited-9532951b23144d79c6a9bf6a9aee3256508a40dba79d55dbe1dcc56216cb933d.log
07:45 Getting Docker log: bechhansen-master-client_latest-exited-c461c8666352c098b6b8274e4f510457affbc0936dcf70ad4bc17ffb61b16d96.log
07:45 Getting Docker log: bechhansen-master-service3_1-running-d8dec9b478d373b701377dae692c38bb400f3b825c244cd4c0fd36a5736ed222.log
07:46 Getting Docker log: bechhansen-master-service2_2-running-8f0c669e53ce524e77d7fdaf76de53e74d9ba5dbc45be66dd56e6811440a9022.log
07:46 Getting Docker log: bechhansen-master-service1_2-running-875af186c5a6eeda57698d7fbe864eee307e26aa72b280f91cb088e97c6ab866.log
07:46 Getting Docker log: bechhansen-master-auth_2-running-242d17965f47b2ce6d8745c65e168d93f0176d98c0cbd779d569a73d62a735de.log
07:46 Getting Docker log: bechhansen-master-auth_1-running-e4bd2506418191371b4db06face924998697b290661b19008ee9cf909e45a762.log
07:47 Getting Docker log: bechhansen-master-service1_1-running-d457ec5502c956e36b492ecfe02d5090448bcfd7dd92d310aaf43a9c3f2625b2.log
07:47 Getting Docker log: bechhansen-master-auth_2-running-6ea8cbe5e5376a4c831b8d6968be36cc0b9a8f82627486844a7a015370a28217.log
07:47 Getting Docker log: bechhansen-master-service3_2-running-5968dae3b5ca6bb0b5ae5d7fa33f3fce1d870ecd5892aaae01725984e60372b3.log
07:48 Getting Docker log: bechhansen-master-service2_2-running-4dffa89fd0f361190258923dceccb7cf9c664b7fe45ed8b0f9ff0fb0cf20be42.log
07:48 Getting Docker log: bechhansen-master-service1_1-running-8fcf762e2fe673a766684a7379b30cc03bc336be0a4616e258cca56637a6cf46.log
07:48 Getting Docker log: bechhansen-master-service2_2-running-a27fce69395deeba07777cd5e435a32aedeff9972e0b5c21566ca40b648587cb.log
07:49 Getting Docker log: bechhansen-master-service1_2-running-451b1057c7c4d4c9b7679d7e0f7e2e3eb756b4c1940f2863119c92836b4cc0a0.log
07:49 Getting Docker log: bechhansen-master-service3_2-running-c33e0f6e1ebce1d58805b1e6fafbf15fea86a0a891cca1dc7e6e6693762ccad5.log
07:49 Getting Docker log: bechhansen-master-auth_1-running-3e535e735fddac6d28c15f5db55fe80b54b82c5236d1f70c09e9ad1047fdd7da.log
07:50 Getting Docker log: bechhansen-master-auth_2-running-faef1e0a69dee88d0696709f6cd94e354f285a3720290338bedda306a711c938.log
07:50 Getting Docker log: bechhansen-master-service3_2-running-ca2c5e9637e274a64d7f14f288099c2bfc1f23f6507d64ea23987fbffe8dd3b8.log
07:50 Getting Docker log: bechhansen-master-service2_2-running-44a55e031fabb11960cb0eaa247d08b4a3bbcdbce3bcbcc519330241978a08fa.log
07:50 Getting Docker log: bechhansen-master-service1_2-running-0e503ce7aa38a4f473809abd1215b7b1f826959a00982b949113731a2d536f2b.log
07:51 Getting Docker log: bechhansen-master-service1_1-running-dcc0fb2520387656ea2eaf14949a2b927bca6b58f9d5ee8beb88906bc2031b03.log
07:51 Getting Docker log: bechhansen-master-service3_1-running-18266c19aedb23f53562bc125bb22cbf703df0a2082fca717d59201a4e3df0c2.log
07:51 Getting Docker log: bechhansen-master-service1_1-running-4b640ae9d52d45631f1e3e0046a8b0ecf263166082e106691a5ea59231f6eb61.log
07:52 Getting Docker log: bechhansen-master-auth_2-running-e07f3b5df25a720ca45abc198bc6c0fb57edd993f7f432f58228d9a827bd9745.log
07:52 Getting Docker log: bechhansen-master-service2_2-running-57fd7a6b3507def973d7124397399e4672e40df0dceeb0e60ef2f718255716aa.log
07:52 Getting Docker log: bechhansen-master-service3_1-running-ba59967f49bc22244feb6a40a727801eba6ef00c0c88d39365d0205c53c195c8.log
07:53 Getting Docker log: bechhansen-master-service1_1-running-b32b4ba367a50428c0220c192c1c2fc815c03be2a5ee46da5e723523c7881641.log
07:53 Getting Docker log: bechhansen-master-service2_1-running-cfd14ab8596e4ef6f68c19ac77a77b0e0ee2164ce4957d8fba43d5f4952ca886.log
07:53 Getting Docker log: bechhansen-master-service2_2-running-53824c0eef31f18949936bd78f9b6a563396fa4e45f8f4fa4be838cebabb981a.log
07:53 Getting Docker log: bechhansen-master-auth_2-running-5bcfc65a1b5294f2dbbc1ed039662c6ea4fb87fedb1d4652b06de144749bc987.log
07:54 Getting Docker log: bechhansen-master-service2_1-running-669c68ce7d99863b096eb522e9231ca246f1ee13aea95c60634c89aee6849e8b.log
07:54 Getting Docker log: bechhansen-master-service3_1-running-54f98377bd65f0c22173b42cd2e55c6e801c8c5f27d9558bc6d76bf796231312.log
07:54 Getting Docker log: bechhansen-master-service1_2-running-4608d37b38efd0cb78fa65f4e1c756e332fe2032da2c83745bc57f43b5595130.log
07:54 Getting Docker log: bechhansen-master-auth_2-running-ab1804ab217c444f2974d8402589ab726c94c1eac8a41535743112239ba2c5ad.log
07:55 Getting Docker log: bechhansen-master-auth_2-running-903c953fa32820bd74cb819ee7aa93c4c7a3dcdc02ea51e8c8c10144f6fab573.log
07:55 Getting Docker log: bechhansen-master-service3_1-running-17a78d65f33295468260bfd71b78998153e65b91e3e2fb5731488d319bdbb2bf.log
07:55 Getting Docker log: bechhansen-master-service3_2-running-0d3d9bff364d1e39d100ebe2486062a952c6cb5f8dd4df12621b276274a78bde.log
07:56 Getting Docker log: bechhansen-master-service2_2-running-fa2e4c12186feafd1c6854cf5b1d7cab335074e27a21257b2076e8cea7b3931a.log
07:56 Getting Docker log: bechhansen-master-service1_2-running-38b8cb53831293a5f26503d1e920b5687f67b6b1556f531675459ab86182229c.log
07:56 Getting Docker log: bechhansen-master-service3_1-running-344e3509225f604b892ed203bcd352462678bfba5e690e0ca9b5bbc305d41010.log
07:57 Getting Docker log: bechhansen-master-service1_2-running-79b2cf676947d2cc7c33d61f4b0e07b6aeceeea27b1d697cca106100b33ba459.log
07:57 Getting Docker log: bechhansen-master-service3_2-running-1083101f872a851963709a1dbc2858be3ac48d05d4c5dd820fa02fa52b430c3b.log
07:57 Getting Docker log: bechhansen-master-service2_2-running-084d1ecf5c35325e80c4850e75655de897cd88b3370c77ec351dba3a78d89493.log
07:57 Getting Docker log: bechhansen-master-auth_1-running-c1746f5498c65958d930e484eb06df5f3e0462ea6697f140899597348daf234b.log
07:58 Getting Docker log: bechhansen-master-service2_1-running-328faaab938a0c52b23d324fa877ef4f21c1363ad8be995c1540938254d3d136.log
07:58 Getting Docker log: bechhansen-master-service2_1-running-66ac02302fe354517088cebd2f237b6834a4dd5609307cd0715fc97a440ae00e.log
07:58 Getting Docker log: bechhansen-master-service1_1-running-b53578eb6d6ee2f2cd2e9116e3fb0dd61b43d0427653090920a95eca7bc2f489.log
07:58 Getting Docker log: bechhansen-master-auth_2-running-bbff09f72aec807c40557d904cecc3cfb13e3eeac01c5ff91cb946bf526c90cb.log
07:59 Getting Docker log: bechhansen-master-service3_2-running-be03441fdf57332ca63c9ae0bdf6b2366c76a69a5f7e68fe69154c2ffb61b688.log
07:59 End log {SERVICE1=14, SERVICE3=12, SERVICE2=12, AUTH=11}
07:59 Terminating all EC2 instances
08:00 Terminated all EC2 instances
