00:00 ------------------ CONFIGURATION---------------------
00:00 SwarmNode count                10   
00:00 TestNode count                 6    
00:00 Swarm deployment               true 
00:00 Encrypted                      false
00:00 Manual test                    false
00:00 Churn enabled                  true 
00:00 Churn type                     GRACEFUL
00:00 Stop grace period              null 
00:00 Gracefull shutdown             false
00:00 Gracefull shutdown timeout     30   
00:00 Keep-alive                     true 
00:00 Test duration                  300  
00:00 Test Threads                   100  
00:00 Test rampup                    30   
00:00 Disable load balancer          false
00:00 Enable Retry                   false
00:00 JMeter mode                    StrippedBatch
00:00 Get service logs               true 
00:00 -----------------------------------------------------
00:00 Run 10 EC2 instances for Docker Swarm cluster
00:01 EC2 Swarm Instances starting...
00:10 EC2 Swarm Instances started
00:10 Run 6 EC2 instances for JMeter cluster
00:11 EC2 JMeter instances starting...
00:19 Done!
00:20 Create loadbalancer
00:20 Created loadbalancer: Master-1850055001.eu-west-1.elb.amazonaws.com
00:21 Adding instances to loadbalancer
00:21 Added 10 instances to loadbalancer
00:21 Waiting for all Docker instances to answer
00:30 Docker ready on all instances
00:31 Swarm initialized by ec2-34-251-135-226.eu-west-1.compute.amazonaws.com
00:32 Swarm manager added ec2-18-202-219-62.eu-west-1.compute.amazonaws.com
00:33 Swarm manager added ec2-34-246-250-18.eu-west-1.compute.amazonaws.com
00:33 Swarm worker added  ec2-34-247-81-200.eu-west-1.compute.amazonaws.com
00:34 Swarm worker added  ec2-34-252-116-200.eu-west-1.compute.amazonaws.com
00:34 Swarm worker added  ec2-34-244-35-136.eu-west-1.compute.amazonaws.com
00:34 Swarm worker added  ec2-34-253-31-210.eu-west-1.compute.amazonaws.com
00:34 Swarm worker added  ec2-34-245-22-88.eu-west-1.compute.amazonaws.com
00:35 Swarm worker added  ec2-54-194-34-172.eu-west-1.compute.amazonaws.com
00:35 Swarm worker added  ec2-18-203-254-34.eu-west-1.compute.amazonaws.com
00:35 Starting pulling prototype images for all servers
00:35 Starting to pull images for ec2-34-251-135-226.eu-west-1.compute.amazonaws.com
00:35 Starting to pull images for ec2-18-202-219-62.eu-west-1.compute.amazonaws.com
00:35 Starting to pull images for ec2-34-246-250-18.eu-west-1.compute.amazonaws.com
00:35 Starting to pull images for ec2-34-247-81-200.eu-west-1.compute.amazonaws.com
00:35 Starting to pull images for ec2-34-252-116-200.eu-west-1.compute.amazonaws.com
00:35 Starting to pull images for ec2-34-244-35-136.eu-west-1.compute.amazonaws.com
00:35 Starting to pull images for ec2-34-253-31-210.eu-west-1.compute.amazonaws.com
00:35 Starting to pull images for ec2-34-245-22-88.eu-west-1.compute.amazonaws.com
00:35 Starting to pull images for ec2-54-194-34-172.eu-west-1.compute.amazonaws.com
00:35 Starting to pull images for ec2-18-203-254-34.eu-west-1.compute.amazonaws.com
00:55 Finished pulling prototype images for all servers
00:55 Deploying containers to Swarm
00:55 Using un-encrypted overlay network
01:00 Creating service log: AUTH.log
01:05 Creating service log: SERVICE2.log
01:09 Creating service log: SERVICE3.log
01:13 Creating service log: SERVICE1.log
01:13 Finished Deploying containers to Swarm
01:13 Wait until all instances are Healthy
02:05 All instances are InService for loadbalancer
02:05 Waiting for all Docker instances to answer
02:06 Docker ready on all instances
02:06 Starting pulling test images for all servers
02:06 Starting to pull images for ec2-34-246-178-94.eu-west-1.compute.amazonaws.com
02:06 Starting to pull images for ec2-34-241-192-25.eu-west-1.compute.amazonaws.com
02:06 Starting to pull images for ec2-63-34-20-37.eu-west-1.compute.amazonaws.com
02:06 Starting to pull images for ec2-34-245-34-132.eu-west-1.compute.amazonaws.com
02:06 Starting to pull images for ec2-54-194-240-107.eu-west-1.compute.amazonaws.com
02:06 Starting to pull images for ec2-34-247-191-65.eu-west-1.compute.amazonaws.com
02:16 Finished pulling test images for all servers
02:16 Start GracefulChurnGenerator
02:16 Starting test against Master-1850055001.eu-west-1.elb.amazonaws.com
02:18 Start test slave 1 on ec2-34-246-178-94.eu-west-1.compute.amazonaws.com
02:20 Start test slave 2 on ec2-34-241-192-25.eu-west-1.compute.amazonaws.com
02:21 Start test slave 3 on ec2-63-34-20-37.eu-west-1.compute.amazonaws.com
02:23 Start test slave 4 on ec2-34-245-34-132.eu-west-1.compute.amazonaws.com
02:25 Start test slave 5 on ec2-54-194-240-107.eu-west-1.compute.amazonaws.com
02:27 Client instance is ec2-34-247-191-65.eu-west-1.compute.amazonaws.com
02:27 Running test...
02:46 Rollingupdate SERVICE1 to bechhansen/master-service1:2 - Order: START_FIRST. Parallelism: 2
02:51 Scale SERVICE2 to 11
02:52 SERVICE2 scaled
02:56 Rollingupdate SERVICE3 to bechhansen/master-service3:2 - Order: START_FIRST. Parallelism: 2
03:01 Scale AUTH to 11
03:02 AUTH scaled
03:23 Scale SERVICE2 to 10
03:23 SERVICE2 scaled
03:29 Rollingupdate SERVICE2 to bechhansen/master-service2:2 - Order: START_FIRST. Parallelism: 2
03:33 Scale AUTH to 10
03:33 AUTH scaled
03:38 Rollingupdate AUTH to bechhansen/master-auth:2 - Order: START_FIRST. Parallelism: 1
05:32 SERVICE1 rolled
05:38 Scale SERVICE1 to 11
05:38 SERVICE1 scaled
05:43 SERVICE3 rolled
05:49 Scale SERVICE3 to 11
05:49 SERVICE3 scaled
06:08 Scale SERVICE1 to 10
06:09 SERVICE1 scaled
06:14 Rollingupdate SERVICE1 to bechhansen/master-service1:1 - Order: START_FIRST. Parallelism: 2
06:15 SERVICE2 rolled
06:19 Scale SERVICE3 to 10
06:20 SERVICE3 scaled
06:21 Scale SERVICE2 to 11
06:21 SERVICE2 scaled
06:25 Rollingupdate SERVICE3 to bechhansen/master-service3:1 - Order: START_FIRST. Parallelism: 2
06:52 Scale SERVICE2 to 10
06:52 SERVICE2 scaled
06:58 Rollingupdate SERVICE2 to bechhansen/master-service2:1 - Order: START_FIRST. Parallelism: 2
07:40 Test Done
07:41 Getting Docker log: bechhansen-master-slave_latest-exited-782a1b0805e7a6a969bb865ff6964314de6b757a71ea29cdfc5508ec663b47c5.log
07:41 Getting Docker log: bechhansen-master-slave_latest-exited-f36442e533728c869a4780efca015abd323e967eadc2a8518701797d0bd56ed1.log
07:42 Getting Docker log: bechhansen-master-slave_latest-exited-68891b5f6a9dc2fb7b1e1ebc1ab5ab56f5ce530344c3fb079af405b095d6bb6e.log
07:42 Getting Docker log: bechhansen-master-slave_latest-exited-af6ee48dd0d540e98f549cc15e4fe39d101f09650861291f6dec937de78ccab1.log
07:43 Getting Docker log: bechhansen-master-slave_latest-exited-2231eabdcc6da7f793ddb64f2996c21a76d431fa2e27bf5b21c09ef8a487a0bc.log
07:43 Getting Docker log: bechhansen-master-client_latest-exited-247ef85e8355c2efd35ad65a1f143012dc6cffdb698f78dbc65d6cbd34449e37.log
07:44 Getting Docker log: bechhansen-master-service2_1-running-a42da420c5a8d3aa654e18f0db44f750ff3a2160739537a93468b8863ad1856e.log
07:44 Getting Docker log: bechhansen-master-auth_2-running-73f29c3574f8d2b34f10ae529563b43307a69d0fda1fb4336a7d6f8c4f2580d5.log
07:44 Getting Docker log: bechhansen-master-service1_1-running-26b5f5423bb46ff9f77fb2544e99a1db7b29011a34401b9da37b9d7692e896e9.log
07:44 Getting Docker log: bechhansen-master-service1_1-running-49cc6ed47128163b2eb2338e636d718e77a3b5c445b8f180f62c6beb53f287d8.log
07:45 Getting Docker log: bechhansen-master-service3_2-running-e371707e0414a999740fec53d8fdbc8a093a527670d1036fe9317b03e76ef8dc.log
07:45 Getting Docker log: bechhansen-master-service3_1-running-522a32feca1b400cd9bde3e4c9a0c8bcfc18d54eb00c587d7ecc7803ce84b3b5.log
07:45 Getting Docker log: bechhansen-master-service2_1-running-9ed6270f7d4fb882efb3cd502abb32f41a3e8207a1d45abe089d38657418330d.log
07:45 Getting Docker log: bechhansen-master-service1_2-running-716f1544785e411c8d6666e79178e9bab18aa90cff199684f8f13232ea0f8e7e.log
07:46 Getting Docker log: bechhansen-master-service2_2-running-a43dfa2e30b01d3328c9680495d52e1bb123ad48a68b0a117ed1535fb74f4322.log
07:46 Getting Docker log: bechhansen-master-auth_1-running-6d568b1457a793320de1ea5a86a7299944132a87c29a92838685aab6213dcc7a.log
07:46 Getting Docker log: bechhansen-master-service1_1-running-cadc0b4526b44a3c5498681b9f6717d0ec08a21bd18aa93cbefe7f32a1f55209.log
07:47 Getting Docker log: bechhansen-master-service3_1-running-ee94ce6fd22ebfdb5c835875777f3c047c65b8f725c79be9aeb92998633a0c3e.log
07:47 Getting Docker log: bechhansen-master-service3_2-running-692869f1cf9ac951f3acdfdc74bf609985fe14bfd1494b59e2fb8c47c06d1416.log
07:47 Getting Docker log: bechhansen-master-service2_2-running-22b0d1d6aec8d26c3251024a7fb701f298c2cd747c23b7791b95de5f52dc967d.log
07:47 Getting Docker log: bechhansen-master-auth_2-running-da8316a47a2eb70499e084c2de9be4aff41e5206148ced02d2d78ff6e49476f6.log
07:48 Getting Docker log: bechhansen-master-auth_2-running-ae0e462205d1165fe24a4c17e0a729600bf637564cc9c3dac90615c293054287.log
07:48 Getting Docker log: bechhansen-master-service2_2-running-a468c18c1b6aba29e1cb9775d84f6d45cc247831d06cad4f0598156d426092e3.log
07:48 Getting Docker log: bechhansen-master-service3_2-running-bc32f328ca87f0a846fa106f13a1ba19e2ea5b2ec18055db1d5ebdaac5d419dd.log
07:49 Getting Docker log: bechhansen-master-service1_2-running-89e6fab47e46d12e695cb7d01edafa54c2bf435a1b01d0d1f75be0c1c6a926a0.log
07:49 Getting Docker log: bechhansen-master-service3_1-running-d34c427993ae492fd2cea0fc0b6a4061a68f6ebec34aff62831a6c4abf0d0a6a.log
07:49 Getting Docker log: bechhansen-master-service2_2-running-ae1edf35e31f1ea87c77541ca1fe628ea7584ff9a1ac5d07bac79aedb7325ea0.log
07:49 Getting Docker log: bechhansen-master-service1_1-running-69525a3af58faf848e340a08c76ad2f89db41cb0dffc6dd69f52d8bea26be03e.log
07:50 Getting Docker log: bechhansen-master-service1_2-running-f932f7fe1feb7c1b8ce910b8cb1419715d6c503e6095c735139c31c720bb9866.log
07:50 Getting Docker log: bechhansen-master-auth_1-running-27f25d6656b08b2bc519c90b4d63031f862e60d6746634364bbbe89ce9c4a86f.log
07:50 Getting Docker log: bechhansen-master-service3_1-running-10a9fcb306942d20f32a1d13fded6cba891c4854cc69d8dd23f263fa83bac137.log
07:51 Getting Docker log: bechhansen-master-auth_2-running-17cd16aa1da86a80706d544e4b2f0462ba0c78e50b835c3a29e70c1ed4c0c9f9.log
07:51 Getting Docker log: bechhansen-master-service2_2-running-268d0667ade17499be83db5957cc19ee24a9fe4bcb6b171465bf06be2178c553.log
07:51 Getting Docker log: bechhansen-master-service1_2-running-60b44c962cd7a3a467a90a311704dcbef3cc57b945f67e017f10d28141a16876.log
07:52 Getting Docker log: bechhansen-master-service3_2-running-a9981e1f339f71a48163782aabae6b390e97211b0d8ca66479bba2275042599c.log
07:52 Getting Docker log: bechhansen-master-service2_2-running-28767ccb00df021947fd641f8dfff60bd873f6b7658e108a7a9c1e708e82748d.log
07:52 Getting Docker log: bechhansen-master-service1_2-running-e5845915ee44bada6d6acf63dedd7c06ba7ade83402221978c69cd738bb53848.log
07:52 Getting Docker log: bechhansen-master-auth_2-running-8538bd32cdc527b96da9729d3412168bc0cdd5ee110bfe3047bed284470b30bd.log
07:52 Getting Docker log: bechhansen-master-auth_1-running-dc34141399e9dfb1f196bfc1f91284591a329e39f8479d1e924fe52cc767db9c.log
07:53 Getting Docker log: bechhansen-master-service1_1-running-92a996c7f35eebc02978d341c98d093c83838e991bfdb9a2f630e42245371f81.log
07:53 Getting Docker log: bechhansen-master-auth_2-running-f3d037cfc8d180536e7220c7e9e86c6c0f6167a50cec738c489d7224e82e45b0.log
07:53 Getting Docker log: bechhansen-master-service3_2-running-bba60b0954d516faf9c506fe1d602cfb88dfb59c3274e73f0ba6d52658523e56.log
07:54 Getting Docker log: bechhansen-master-service2_2-running-c932a4fe2d1b182f5e03448ffa1d4d2ea57b7715484c23fb4d12846716fe9dc6.log
07:54 Getting Docker log: bechhansen-master-service2_1-running-86f466c4a3e097a0d0d8f88b4fc93681be14a36b0b892a59ef68c00ea6b4de3e.log
07:54 Getting Docker log: bechhansen-master-service3_1-running-812a4f4fa34f23671da8a55f38e9d4df5a1b57f6c79685279edf0364e19cca75.log
07:55 Getting Docker log: bechhansen-master-service1_1-running-e07842e21cf6f0fd501d3d5f5b9aed4b10da14422325e91061426422ce3f4f53.log
07:55 Getting Docker log: bechhansen-master-auth_2-running-b0653ed7fe1eaecffea76ba451bc031ff314cd34a098650f216370d801a3892a.log
07:55 Getting Docker log: bechhansen-master-service3_2-running-1d2acc855552e65f8a64021515eb7b8a7284b0d501952a0bc7ca4ef094e64897.log
07:55 Getting Docker log: bechhansen-master-service3_1-running-6412ab1168db76c09601d7d607f563fcad9dc503885de8a3632f5e7a8dcf66fa.log
07:56 Getting Docker log: bechhansen-master-service2_1-running-4cd27966b2f764f870daaceeb5761c40e2cc9d8360058273644b5b064eac29c4.log
07:56 Getting Docker log: bechhansen-master-service1_2-running-6c3d94bb4a3219c779915a95564827dd9aa6a9b71cb8683af8280c4b45a46d1d.log
07:56 Getting Docker log: bechhansen-master-auth_2-running-15e70405fbeb3ae738c8392a63be500dd2206565433dc95587e5e9fa99a4c99a.log
07:56 Getting Docker log: bechhansen-master-service2_2-running-a287bc7e64a241b95a6fd57159f7974cdaa3786e79b56ddefa736d9d5437934a.log
07:57 End log {SERVICE1=14, AUTH=11, SERVICE2=12, SERVICE3=12}
07:57 Terminating all EC2 instances
07:58 Terminated all EC2 instances
