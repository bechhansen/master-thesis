00:00 ------------------ CONFIGURATION---------------------
00:00 SwarmNode count                10   
00:00 TestNode count                 6    
00:00 Swarm deployment               true 
00:00 Encrypted                      true 
00:00 Manual test                    false
00:00 Churn enabled                  false
00:00 Churn type                     GRACEFUL
00:00 Stop grace period              600000000000
00:00 Gracefull shutdown             false
00:00 Gracefull shutdown timeout     600  
00:00 Keep-alive                     false
00:00 Test duration                  300  
00:00 Test Threads                   100  
00:00 Test rampup                    30   
00:00 Disable load balancer          false
00:00 Enable Retry                   false
00:00 JMeter mode                    StrippedBatch
00:00 Get service logs               true 
00:00 -----------------------------------------------------
00:00 Run 10 EC2 instances for Docker Swarm cluster
00:02 EC2 Swarm Instances starting...
00:11 EC2 Swarm Instances started
00:12 Run 6 EC2 instances for JMeter cluster
00:13 EC2 JMeter instances starting...
00:21 Done!
00:21 Create loadbalancer
00:22 Created loadbalancer: Master-1402913414.eu-west-1.elb.amazonaws.com
00:22 Adding instances to loadbalancer
00:22 Added 10 instances to loadbalancer
00:22 Waiting for all Docker instances to answer
00:28 Docker ready on all instances
00:29 Swarm initialized by ec2-34-240-15-6.eu-west-1.compute.amazonaws.com
00:30 Swarm manager added ec2-34-255-3-50.eu-west-1.compute.amazonaws.com
00:31 Swarm manager added ec2-34-251-199-130.eu-west-1.compute.amazonaws.com
00:31 Swarm worker added  ec2-34-251-166-92.eu-west-1.compute.amazonaws.com
00:32 Swarm worker added  ec2-34-245-185-27.eu-west-1.compute.amazonaws.com
00:32 Swarm worker added  ec2-34-245-115-188.eu-west-1.compute.amazonaws.com
00:32 Swarm worker added  ec2-54-194-30-88.eu-west-1.compute.amazonaws.com
00:33 Swarm worker added  ec2-34-244-188-2.eu-west-1.compute.amazonaws.com
00:33 Swarm worker added  ec2-63-32-43-131.eu-west-1.compute.amazonaws.com
00:33 Swarm worker added  ec2-54-171-188-76.eu-west-1.compute.amazonaws.com
00:33 Starting pulling prototype images for all servers
00:33 Starting to pull images for ec2-34-240-15-6.eu-west-1.compute.amazonaws.com
00:33 Starting to pull images for ec2-34-255-3-50.eu-west-1.compute.amazonaws.com
00:33 Starting to pull images for ec2-34-251-199-130.eu-west-1.compute.amazonaws.com
00:33 Starting to pull images for ec2-34-251-166-92.eu-west-1.compute.amazonaws.com
00:33 Starting to pull images for ec2-34-245-185-27.eu-west-1.compute.amazonaws.com
00:33 Starting to pull images for ec2-34-245-115-188.eu-west-1.compute.amazonaws.com
00:33 Starting to pull images for ec2-54-194-30-88.eu-west-1.compute.amazonaws.com
00:33 Starting to pull images for ec2-34-244-188-2.eu-west-1.compute.amazonaws.com
00:33 Starting to pull images for ec2-63-32-43-131.eu-west-1.compute.amazonaws.com
00:33 Starting to pull images for ec2-54-171-188-76.eu-west-1.compute.amazonaws.com
00:53 Finished pulling prototype images for all servers
00:53 Deploying containers to Swarm
00:53 Using encrypted overlay network
00:59 Creating service log: AUTH.log
01:03 Creating service log: SERVICE2.log
01:08 Creating service log: SERVICE3.log
01:12 Creating service log: SERVICE1.log
01:12 Finished Deploying containers to Swarm
01:12 Wait until all instances are Healthy
02:01 All instances are InService for loadbalancer
02:01 Waiting for all Docker instances to answer
02:01 Docker ready on all instances
02:01 Starting pulling test images for all servers
02:01 Starting to pull images for ec2-34-244-213-150.eu-west-1.compute.amazonaws.com
02:01 Starting to pull images for ec2-34-245-81-44.eu-west-1.compute.amazonaws.com
02:01 Starting to pull images for ec2-34-253-77-178.eu-west-1.compute.amazonaws.com
02:01 Starting to pull images for ec2-52-210-155-170.eu-west-1.compute.amazonaws.com
02:01 Starting to pull images for ec2-34-246-195-144.eu-west-1.compute.amazonaws.com
02:01 Starting to pull images for ec2-34-248-49-12.eu-west-1.compute.amazonaws.com
02:11 Finished pulling test images for all servers
02:11 Starting test against Master-1402913414.eu-west-1.elb.amazonaws.com
02:13 Start test slave 1 on ec2-34-244-213-150.eu-west-1.compute.amazonaws.com
02:15 Start test slave 2 on ec2-34-245-81-44.eu-west-1.compute.amazonaws.com
02:17 Start test slave 3 on ec2-34-253-77-178.eu-west-1.compute.amazonaws.com
02:18 Start test slave 4 on ec2-52-210-155-170.eu-west-1.compute.amazonaws.com
02:19 Start test slave 5 on ec2-34-246-195-144.eu-west-1.compute.amazonaws.com
02:22 Client instance is ec2-34-248-49-12.eu-west-1.compute.amazonaws.com
02:22 Running test...
07:34 Test Done
07:35 Getting Docker log: bechhansen-master-slave_latest-exited-02f8a921a3480b039ed2f075ad49c55b1911965effd9ef4578888186c8ab132a.log
07:36 Getting Docker log: bechhansen-master-slave_latest-exited-b72242c6294456712891de4436894dcf48c756596c401d177e194c226a15ccfa.log
07:36 Getting Docker log: bechhansen-master-slave_latest-exited-5839bd17f8eeb3fdb076d3dd28dd58a1a932bc272fa4cb71ee0f64d44114f071.log
07:37 Getting Docker log: bechhansen-master-slave_latest-exited-128cf276b72dd06dfab61f6b7d85325e8d63446028b031ba5b6cc7f34c4fac96.log
07:37 Getting Docker log: bechhansen-master-slave_latest-exited-b8d4a74e3e79349b4f140e578a9d00fd9f91788006866c60b8fa1a7b4414472f.log
07:37 Getting Docker log: bechhansen-master-client_latest-exited-1dbe101da036d9f362301fafc15a3f9e6088ad82ef74e4e65e09c38245576adb.log
07:38 Getting Docker log: bechhansen-master-service1_1-running-75df1fd8292bdb3409f8bfe6240427d55a5cec156adb51c1559491b6e65c3ba4.log
07:38 Getting Docker log: bechhansen-master-service3_1-running-5fe0d72b39c6a0e3313d6d1f103bdbe155038724b232a9b006c9cc1058ebd423.log
07:38 Getting Docker log: bechhansen-master-service2_1-running-ebacce9dbd0b426d127633989420fc727ad538e8cbb5153e5fafb1461334c872.log
07:39 Getting Docker log: bechhansen-master-auth_1-running-999a041420a368c34dde89bf87cbc9f0474fca625b5ecc66374a85d82acfe339.log
07:39 Getting Docker log: bechhansen-master-service1_1-running-802716427d114149f76838a665730d7f946426114cb065ce7184d80364d89b6e.log
07:39 Getting Docker log: bechhansen-master-service3_1-running-de834770be3f20d6bf609aa54e74762de2f7514e0c395153e6f708387f06bb39.log
07:40 Getting Docker log: bechhansen-master-service2_1-running-fcd6604728e33b1ffad5fc07f98658900eee22a329a94038159c99a1a568c118.log
07:40 Getting Docker log: bechhansen-master-auth_1-running-0ecb07526e84422fb4e9f026c0fd85c9102847c3c3ab93f71fe3befd26b8875a.log
07:40 Getting Docker log: bechhansen-master-service1_1-running-beda5a929070cbcf0e598df57e79c9f3c9adc7820d490818190c404361fb3987.log
07:40 Getting Docker log: bechhansen-master-service3_1-running-10498f4181a7a66208dc28388f19c997b7e9f9328c321e8bbfe25d70e1f086fe.log
07:41 Getting Docker log: bechhansen-master-service2_1-running-ef94ec542b40fd935df30491625291198caeb9ffee8d1ffb3f585a3d18365f57.log
07:41 Getting Docker log: bechhansen-master-auth_1-running-ac2221cce06b9bff8ebaf3148980452b5fa85a8f5dad303ded611f6f6c0e46ad.log
07:41 Getting Docker log: bechhansen-master-service1_1-running-481e974a8ee6a591282fe6003675c7cc0f91c468911c3b548a81ec4c7f95cd81.log
07:42 Getting Docker log: bechhansen-master-service3_1-running-4dccdac0d3d46cba6206c89f2151efed4b143ac782b44085ebac1a62797d6f30.log
07:42 Getting Docker log: bechhansen-master-service2_1-running-7b0eb1bacf19bf0241a8f7bf897caf5aab7f7e868cd4fa96f186399ac31860f5.log
07:42 Getting Docker log: bechhansen-master-auth_1-running-06c3a149d175fce64346cb8afd26f89a2879360c1640dffae29507e46c5be16e.log
07:42 Getting Docker log: bechhansen-master-service1_1-running-d28f056183084620083530c31559b13d3f440adb219926aede56e9efba170bc5.log
07:43 Getting Docker log: bechhansen-master-service3_1-running-84a9fff4a2aa8b5b4a6a4976e2dda8c8692d7f2bcc44abdeabeb5dbdeddc8c24.log
07:43 Getting Docker log: bechhansen-master-service2_1-running-3ab61ca1bfa1d0dfeb8541efa0e549d9b6d7b0a0da0f14c3d794e0496f10732c.log
07:43 Getting Docker log: bechhansen-master-auth_1-running-3299d4def00f825e0d4eb4177b7799f5bcd610bf3d7f43725b378c2bdae2bc2b.log
07:44 Getting Docker log: bechhansen-master-service1_1-running-edd36753542986b4694fa2ed035cb1123b648246bbed5226e986acdbd28e3452.log
07:44 Getting Docker log: bechhansen-master-service3_1-running-03439c57571d0f3c3d4e7f89afa005e64cc14113d4a9d4ae2f0ef8f1b3f8c218.log
07:44 Getting Docker log: bechhansen-master-service2_1-running-2b150b5a8761b8752d481c8d50b06f5b8eecc7145912be75c714dc988c34588e.log
07:44 Getting Docker log: bechhansen-master-auth_1-running-ac59409cfee1558a36d81e8d44daafde0216b25b23bc020d32584037b050a05b.log
07:45 Getting Docker log: bechhansen-master-service1_1-running-6be4731c7c22413391cc1cf20dad588e2e69191fad529bbfd08db5c3dcbcd693.log
07:45 Getting Docker log: bechhansen-master-service3_1-running-85ae5041d53845a8f7239718d18b6a19143091294d17af93fa461827234cacea.log
07:45 Getting Docker log: bechhansen-master-service2_1-running-59542c83225f69697721e0c4f7859cba525532b524f6c6fe059a7991efa4e7fe.log
07:45 Getting Docker log: bechhansen-master-auth_1-running-e7e16f2fa9715e8b7c07279f257c4cd0782a1f07f6b90d5540482f641a4120f5.log
07:46 Getting Docker log: bechhansen-master-service1_1-running-91171662b1e206c725b60fc79fce680af2dd9559856a46c0469812fb893c1942.log
07:46 Getting Docker log: bechhansen-master-service3_1-running-00ad3211c102a9624b3391949571170f8303ff0f7a21394ec73fc071f0e47a8f.log
07:46 Getting Docker log: bechhansen-master-service2_1-running-0df84c7b1fadbb30f88555198031a6b23add1608e623326dd64f4af0b51d373f.log
07:46 Getting Docker log: bechhansen-master-auth_1-running-f6c6be3b96bb84a66efb790a4d7affe85d687769d762a3733e8f90aff41b8291.log
07:47 Getting Docker log: bechhansen-master-service1_1-running-aad6d93e44a352c6fa74e9977ad4d9b610d88d6e935e9b849db4f38df65c148d.log
07:47 Getting Docker log: bechhansen-master-service3_1-running-13de03d0f5932e78a21c3b9fa13d22e0e11e4f3d1a83d7fcf52e204a9c648d70.log
07:47 Getting Docker log: bechhansen-master-service2_1-running-3d0e78420f7cc62c7c4b5dc092bcb2b0f7a971181f8a6519ca72c64882e3672a.log
07:48 Getting Docker log: bechhansen-master-auth_1-running-e93506b7ba512388028ff51c1664f58030bfdac4797a005ece40d80162c93d49.log
07:48 Getting Docker log: bechhansen-master-service1_1-running-85774df6df16736ff3f6aaf63b3e6b19bc857bd85c8187dd8707539f00e440f7.log
07:48 Getting Docker log: bechhansen-master-service3_1-running-b6ae4047e5d2abfda3f48812a7ca6850a56a69fd515227147a226fc0c3d673b4.log
07:49 Getting Docker log: bechhansen-master-service2_1-running-05e178542cd4757e4913231c3477e932005d37d332244479f79348b2032f6949.log
07:49 Getting Docker log: bechhansen-master-auth_1-running-0fba9cc9f2b8ce400e2dff722378f95daefdba48cb77eddc6cc082df5c925bc8.log
07:49 End log {SERVICE1=10, AUTH=10, SERVICE2=10, SERVICE3=10}
07:49 Terminating all EC2 instances
07:50 Terminated all EC2 instances
