00:00 ------------------ CONFIGURATION---------------------
00:00 SwarmNode count                10   
00:00 TestNode count                 6    
00:00 Swarm deployment               true 
00:00 Encrypted                      false
00:00 Manual test                    false
00:00 Churn enabled                  true 
00:00 Churn type                     UNGRACEFUL_INTERNAL
00:00 Stop grace period              null 
00:00 Gracefull shutdown             true 
00:00 Gracefull shutdown timeout     30   
00:00 Keep-alive                     false
00:00 Test duration                  300  
00:00 Test Threads                   100  
00:00 Test rampup                    30   
00:00 Disable load balancer          false
00:00 Enable Retry                   true 
00:00 JMeter mode                    StrippedBatch
00:00 Get service logs               true 
00:00 -----------------------------------------------------
00:00 Run 10 EC2 instances for Docker Swarm cluster
00:01 EC2 Swarm Instances starting...
00:09 EC2 Swarm Instances started
00:09 Run 6 EC2 instances for JMeter cluster
00:11 EC2 JMeter instances starting...
00:20 Done!
00:20 Create loadbalancer
00:21 Created loadbalancer: Master-1027237555.eu-west-1.elb.amazonaws.com
00:21 Adding instances to loadbalancer
00:22 Added 10 instances to loadbalancer
00:22 Waiting for all Docker instances to answer
00:24 Docker ready on all instances
00:26 Swarm initialized by ec2-63-33-190-92.eu-west-1.compute.amazonaws.com
00:26 Swarm manager added ec2-34-240-127-26.eu-west-1.compute.amazonaws.com
00:27 Swarm manager added ec2-34-252-167-83.eu-west-1.compute.amazonaws.com
00:27 Swarm worker added  ec2-34-242-27-158.eu-west-1.compute.amazonaws.com
00:28 Swarm worker added  ec2-52-17-42-207.eu-west-1.compute.amazonaws.com
00:28 Swarm worker added  ec2-34-246-182-236.eu-west-1.compute.amazonaws.com
00:28 Swarm worker added  ec2-54-154-185-203.eu-west-1.compute.amazonaws.com
00:29 Swarm worker added  ec2-34-242-42-74.eu-west-1.compute.amazonaws.com
00:29 Swarm worker added  ec2-54-229-99-105.eu-west-1.compute.amazonaws.com
00:29 Swarm worker added  ec2-63-35-226-48.eu-west-1.compute.amazonaws.com
00:29 Starting pulling prototype images for all servers
00:29 Starting to pull images for ec2-63-33-190-92.eu-west-1.compute.amazonaws.com
00:29 Starting to pull images for ec2-34-240-127-26.eu-west-1.compute.amazonaws.com
00:29 Starting to pull images for ec2-34-252-167-83.eu-west-1.compute.amazonaws.com
00:29 Starting to pull images for ec2-34-242-27-158.eu-west-1.compute.amazonaws.com
00:29 Starting to pull images for ec2-52-17-42-207.eu-west-1.compute.amazonaws.com
00:29 Starting to pull images for ec2-34-246-182-236.eu-west-1.compute.amazonaws.com
00:29 Starting to pull images for ec2-54-154-185-203.eu-west-1.compute.amazonaws.com
00:29 Starting to pull images for ec2-34-242-42-74.eu-west-1.compute.amazonaws.com
00:29 Starting to pull images for ec2-54-229-99-105.eu-west-1.compute.amazonaws.com
00:29 Starting to pull images for ec2-63-35-226-48.eu-west-1.compute.amazonaws.com
00:49 Finished pulling prototype images for all servers
00:49 Deploying containers to Swarm
00:49 Using un-encrypted overlay network
00:54 Creating service log: AUTH.log
00:59 Creating service log: SERVICE2.log
01:03 Creating service log: SERVICE3.log
01:08 Creating service log: SERVICE1.log
01:08 Finished Deploying containers to Swarm
01:08 Wait until all instances are Healthy
02:01 All instances are InService for loadbalancer
02:01 Waiting for all Docker instances to answer
02:01 Docker ready on all instances
02:01 Starting pulling test images for all servers
02:01 Starting to pull images for ec2-63-32-59-166.eu-west-1.compute.amazonaws.com
02:01 Starting to pull images for ec2-34-244-150-75.eu-west-1.compute.amazonaws.com
02:01 Starting to pull images for ec2-52-50-139-179.eu-west-1.compute.amazonaws.com
02:01 Starting to pull images for ec2-34-246-177-231.eu-west-1.compute.amazonaws.com
02:01 Starting to pull images for ec2-18-202-226-97.eu-west-1.compute.amazonaws.com
02:01 Starting to pull images for ec2-34-248-167-227.eu-west-1.compute.amazonaws.com
02:12 Finished pulling test images for all servers
02:12 Start UngracefullChurnInternalGenerator churn
02:12 Starting test against Master-1027237555.eu-west-1.elb.amazonaws.com
02:13 Start test slave 1 on ec2-63-32-59-166.eu-west-1.compute.amazonaws.com
02:15 Start test slave 2 on ec2-34-244-150-75.eu-west-1.compute.amazonaws.com
02:17 Start test slave 3 on ec2-52-50-139-179.eu-west-1.compute.amazonaws.com
02:18 Start test slave 4 on ec2-34-246-177-231.eu-west-1.compute.amazonaws.com
02:20 Start test slave 5 on ec2-18-202-226-97.eu-west-1.compute.amazonaws.com
02:23 Client instance is ec2-34-248-167-227.eu-west-1.compute.amazonaws.com
02:23 Running test...
02:42 Killing SERVICE2 + on ec2-52-17-42-207.eu-west-1.compute.amazonaws.com
02:47 Killing SERVICE3 + on ec2-34-242-42-74.eu-west-1.compute.amazonaws.com
02:48 SERVICE2 has been recreated 10/10
02:53 SERVICE3 has been recreated 10/10
03:13 Killing SERVICE2 + on ec2-52-17-42-207.eu-west-1.compute.amazonaws.com
03:18 Killing SERVICE3 + on ec2-52-17-42-207.eu-west-1.compute.amazonaws.com
03:19 SERVICE3 has been recreated 10/10
03:20 SERVICE2 has been recreated 10/10
03:44 Killing SERVICE3 + on ec2-52-17-42-207.eu-west-1.compute.amazonaws.com
03:45 SERVICE3 has been recreated 10/10
03:45 Killing SERVICE2 + on ec2-34-242-27-158.eu-west-1.compute.amazonaws.com
03:52 SERVICE2 has been recreated 10/10
04:10 Killing SERVICE3 + on ec2-63-33-190-92.eu-west-1.compute.amazonaws.com
04:17 SERVICE3 has been recreated 10/10
04:17 Killing SERVICE2 + on ec2-34-252-167-83.eu-west-1.compute.amazonaws.com
04:18 SERVICE2 has been recreated 10/10
04:42 Killing SERVICE3 + on ec2-63-35-226-48.eu-west-1.compute.amazonaws.com
04:43 Killing SERVICE2 + on ec2-63-35-226-48.eu-west-1.compute.amazonaws.com
04:44 SERVICE2 has been recreated 10/10
04:49 SERVICE3 has been recreated 10/10
05:09 Killing SERVICE2 + on ec2-34-246-182-236.eu-west-1.compute.amazonaws.com
05:14 Killing SERVICE3 + on ec2-34-246-182-236.eu-west-1.compute.amazonaws.com
05:15 SERVICE3 has been recreated 10/10
05:16 SERVICE2 has been recreated 10/10
05:40 Killing SERVICE3 + on ec2-34-240-127-26.eu-west-1.compute.amazonaws.com
05:41 Killing SERVICE2 + on ec2-34-240-127-26.eu-west-1.compute.amazonaws.com
05:42 SERVICE2 has been recreated 10/10
05:47 SERVICE3 has been recreated 10/10
06:07 Killing SERVICE2 + on ec2-34-240-127-26.eu-west-1.compute.amazonaws.com
06:08 SERVICE2 has been recreated 10/10
06:12 Killing SERVICE3 + on ec2-63-35-226-48.eu-west-1.compute.amazonaws.com
06:13 SERVICE3 has been recreated 10/10
06:33 Killing SERVICE2 + on ec2-34-252-167-83.eu-west-1.compute.amazonaws.com
06:38 Killing SERVICE3 + on ec2-63-33-190-92.eu-west-1.compute.amazonaws.com
06:40 SERVICE2 has been recreated 10/10
06:45 SERVICE3 has been recreated 10/10
07:05 Killing SERVICE2 + on ec2-54-154-185-203.eu-west-1.compute.amazonaws.com
07:10 Killing SERVICE3 + on ec2-54-229-99-105.eu-west-1.compute.amazonaws.com
07:12 SERVICE2 has been recreated 10/10
07:17 SERVICE3 has been recreated 10/10
07:37 Killing SERVICE2 + on ec2-34-242-42-74.eu-west-1.compute.amazonaws.com
07:38 SERVICE2 has been recreated 10/10
07:42 Killing SERVICE3 + on ec2-63-33-190-92.eu-west-1.compute.amazonaws.com
07:43 SERVICE3 has been recreated 10/10
08:03 Killing SERVICE2 + on ec2-34-242-42-74.eu-west-1.compute.amazonaws.com
08:04 SERVICE2 has been recreated 10/10
08:08 Killing SERVICE3 + on ec2-54-154-185-203.eu-west-1.compute.amazonaws.com
08:09 SERVICE3 has been recreated 10/10
08:21 Test Done
08:22 Getting Docker log: bechhansen-master-slave_latest-exited-99879ad467f500278fa3b5360427562a8b3732e608b5edebec842ffb27f9bddb.log
08:23 Getting Docker log: bechhansen-master-slave_latest-exited-2923e83a0654fbfaae83a10e65ec211c7967ae136376902c51a1e903324329f7.log
08:23 Getting Docker log: bechhansen-master-slave_latest-exited-2bba2923c692e8b2078523cb6126cbcd78b88b563c57758378350cd7ab9ced8a.log
08:24 Getting Docker log: bechhansen-master-slave_latest-exited-64ac14fc70be64d2661e7fbab434734736cad8d520e4af70bfd7fe553dbcf6bf.log
08:24 Getting Docker log: bechhansen-master-slave_latest-exited-b1e55e47409e7ce1da70f67ac832329845eebace26e08e22230a7ab266bc781e.log
08:24 Getting Docker log: bechhansen-master-client_latest-exited-801be0187baf51f1595ad5ee88959ac29ac0308b1eb4bec9b21bc08e5ddf8992.log
08:25 Getting Docker log: bechhansen-master-service3_1-running-a06c76244d91766d9390c746563f63ab306b0672742bb0f7a62c69eadbfb7384.log
08:25 Getting Docker log: bechhansen-master-service1_1-running-addfdf598eb7a1b35a338d9842c40fde6a2dbf2c8da7f5955a05ae8e95922fa5.log
08:25 Getting Docker log: bechhansen-master-service2_1-running-f4379ecbb34bb44d8ab06a36a20b8fbf9332329f16c6f7cf2a1e1c97384b2351.log
08:26 Getting Docker log: bechhansen-master-auth_1-running-e49e92583550b2f4d898e081cb596464fff73e8206703d8525e9f82fd2f5e647.log
08:26 Getting Docker log: bechhansen-master-service2_1-running-1eb66a87e65c77761654157ddeb53cc32dae1f124b7ecf35b383589ca72cdcf7.log
08:26 Getting Docker log: bechhansen-master-service3_1-running-40cc87bb027564b34f4a58da5523bafffa9789ce1df24152454266ad0aef2fc0.log
08:27 Getting Docker log: bechhansen-master-service1_1-running-b3d439568f4ba41e601e3e551d0129aa66e4f4aa44f32c27a79f1b0649e3bb9a.log
08:27 Getting Docker log: bechhansen-master-auth_1-running-eb4cf27f85486ee4404aed48cf2933de20d42fec0615d6b69878e97b3a80eb67.log
08:27 Getting Docker log: bechhansen-master-service2_1-running-5b953fa46b9a6eb61a0df3730bb85600db70bd6b581f17a0cd9ace3c8b99da7a.log
08:28 Getting Docker log: bechhansen-master-service1_1-running-486f91b38917327ffd2adcc4f7e1ed6f4f46f8cdab7c879bd7d64dd11ff88833.log
08:28 Getting Docker log: bechhansen-master-service3_1-running-8c42eb0725965f91e6427a6b1b7fa0213ef340c920e92df253e27be63775807a.log
08:28 Getting Docker log: bechhansen-master-auth_1-running-3544b4021d1c9040701ae5a001d8cda1bfe0b8e25229871d6cbc3cf25d16eb17.log
08:29 Getting Docker log: bechhansen-master-service2_1-running-466455df6d509db7e20d3367c998ddaf70daeb4bfa378795883cab64e0981a69.log
08:29 Getting Docker log: bechhansen-master-service1_1-running-c0e94207abe9ec04ff900ac4a54367112f87619c04219c4c8c1dc530e262dec0.log
08:29 Getting Docker log: bechhansen-master-service3_1-running-4f0094b5b7fa345abf70623dfb313dede3b50304fae53cb7b020eca33b4f88f5.log
08:29 Killing SERVICE2 + on ec2-63-35-226-48.eu-west-1.compute.amazonaws.com
08:29 Getting Docker log: bechhansen-master-auth_1-running-81808af9cedc1f764dbb4ed9ddb76699ae69c494e5347db9db2176fdc0ea51f2.log
08:30 Getting Docker log: bechhansen-master-service3_1-running-3b5cd56fc7bb0d79daf57d9999000a5f6cfb18d58338e5c76e5ab4e8bce03c49.log
08:30 Getting Docker log: bechhansen-master-service2_1-running-2d62896e82637b3f91fcb7c5723ed1253e7f882e2351cd8bad4ba83997c8561b.log
08:30 SERVICE2 has been recreated 10/10
08:30 Getting Docker log: bechhansen-master-service1_1-running-1b70ec267071643ce23e34b661a2e9f82a13e78e4fd188aba0eb72752868fff7.log
08:30 Getting Docker log: bechhansen-master-auth_1-running-84cf7dbde6a2f6a2c489913785834976776afe4882f3bf173491762335eab6e5.log
08:31 Getting Docker log: bechhansen-master-service3_1-running-69cc312c9b50f441459f73015afa7a73dfbeb61f09467cde0a67ff3ac60aa5b1.log
08:31 Getting Docker log: bechhansen-master-service2_1-running-8c7226a5a543631e4d414a444c64fb73f1bf6f403c9a5103fd94ab7ff92e2cd8.log
08:31 Getting Docker log: bechhansen-master-service1_1-running-5e818756e0a0431f2c5a6eeb8d796b49ca4e26f582e21e000eefe5fead8d2e3b.log
08:31 Getting Docker log: bechhansen-master-auth_1-running-8948e5f72c0d1557a85d1b5c552f83be693ff4c0543c6e319d04e6d9aa480a6d.log
08:32 Getting Docker log: bechhansen-master-service3_1-running-cbff6d0177b7594f523ce828ba82c6dd1bac30ab06d64dd3c93ca38124a55341.log
08:32 Getting Docker log: bechhansen-master-service2_1-running-73b45d8b38aa5a5334ff959b4bc7adff5252b1b5f15397ecc07eb8f81ce20fde.log
08:32 Getting Docker log: bechhansen-master-service1_1-running-620ec3a77a6a29ba537b99a2fd3119f0ea0efeb0e64582a34099e16b453234ca.log
08:33 Getting Docker log: bechhansen-master-auth_1-running-c6501633cb50b3d0d214b48e7e1164359c2fe0d3365fc3ce0fe413a3876927fc.log
08:33 Getting Docker log: bechhansen-master-service2_1-running-a6dae24abb1154e71eb83c73e8d398070d6d3f1eb0b42c53348632ce1eee3848.log
08:33 Getting Docker log: bechhansen-master-service3_1-running-bcaf9a23ccc4aa3da3b7159a4d8101ad1ef6e4585f6a9e89d1d228db553048d6.log
08:34 Getting Docker log: bechhansen-master-service1_1-running-7c0a0151e10e31c8b82903281c9e65fc8357c1cd51c66fcc7c63cc6d11044783.log
08:34 Getting Docker log: bechhansen-master-auth_1-running-04dcac911a601545e881780f4bd1f8f975adc0fb109304741ba9be8466d6dfb1.log
08:34 Getting Docker log: bechhansen-master-service3_1-running-57c347a8918d4f0d38ec23c75a81e2f0ba8c9fb8f88da4b879ae44e2324ace43.log
08:34 Killing SERVICE3 + on ec2-54-154-185-203.eu-west-1.compute.amazonaws.com
08:34 Getting Docker log: bechhansen-master-service1_1-running-b803c96629edb5c1ac6dc22d05a294e2b7dcab4eea83d0f4f9c9b718bfa0992a.log
08:35 Getting Docker log: bechhansen-master-service2_1-running-cd21f01663817257e4dd794365b2ca247b7c4773e674e8a71533a0faed1105a5.log
08:35 Getting Docker log: bechhansen-master-auth_1-running-cd824958bf412acf73f7ac76b7460c865e57dd4723b4c85098f630ff1ee0b130.log
08:35 SERVICE3 has been recreated 10/10
08:36 Getting Docker log: bechhansen-master-service2_1-created-5f55053b9f0f0e6d93a5afe149577200b95ab9473f00de7e3a60f270ef25b10b.log
08:36 Getting Docker log: bechhansen-master-service3_1-running-1da76fbfb2aca1faf1499dabdd647f99b330aa300d001b9e24156ec7d745185e.log
08:37 Getting Docker log: bechhansen-master-service1_1-running-f243f32799d2be1aab212b62350a54c7bbd1b42cf2673a12516adcfa51f6a7b8.log
08:37 Getting Docker log: bechhansen-master-auth_1-running-5887cd7840d36ca85249a525325d413d4ff5150316544a401261e5421f22e535.log
08:37 End log {SERVICE2=10, AUTH=10, SERVICE3=10, SERVICE1=10}
08:37 Terminating all EC2 instances
08:38 Terminated all EC2 instances
