00:00 ------------------ CONFIGURATION---------------------
00:00 SwarmNode count                10   
00:00 TestNode count                 6    
00:00 Swarm deployment               true 
00:00 Encrypted                      false
00:00 Manual test                    false
00:00 Churn enabled                  true 
00:00 Churn type                     UNGRACEFUL_INTERNAL
00:00 Stop grace period              null 
00:00 Gracefull shutdown             true 
00:00 Gracefull shutdown timeout     30   
00:00 Keep-alive                     false
00:00 Test duration                  300  
00:00 Test Threads                   100  
00:00 Test rampup                    30   
00:00 Disable load balancer          false
00:00 Enable Retry                   true 
00:00 JMeter mode                    StrippedBatch
00:00 Get service logs               true 
00:00 -----------------------------------------------------
00:00 Run 10 EC2 instances for Docker Swarm cluster
00:01 EC2 Swarm Instances starting...
00:38 EC2 Swarm Instances started
00:38 Run 6 EC2 instances for JMeter cluster
00:39 EC2 JMeter instances starting...
01:02 Done!
01:03 Create loadbalancer
01:04 Created loadbalancer: Master-367549416.eu-west-1.elb.amazonaws.com
01:04 Adding instances to loadbalancer
01:04 Added 10 instances to loadbalancer
01:04 Waiting for all Docker instances to answer
01:04 Docker ready on all instances
01:06 Swarm initialized by ec2-34-244-157-46.eu-west-1.compute.amazonaws.com
01:06 Swarm manager added ec2-54-154-225-139.eu-west-1.compute.amazonaws.com
01:07 Swarm manager added ec2-54-229-46-72.eu-west-1.compute.amazonaws.com
01:07 Swarm worker added  ec2-34-240-77-37.eu-west-1.compute.amazonaws.com
01:08 Swarm worker added  ec2-34-245-34-13.eu-west-1.compute.amazonaws.com
01:08 Swarm worker added  ec2-34-255-11-148.eu-west-1.compute.amazonaws.com
01:08 Swarm worker added  ec2-54-171-84-73.eu-west-1.compute.amazonaws.com
01:09 Swarm worker added  ec2-18-203-224-170.eu-west-1.compute.amazonaws.com
01:09 Swarm worker added  ec2-52-30-240-104.eu-west-1.compute.amazonaws.com
01:09 Swarm worker added  ec2-34-242-147-144.eu-west-1.compute.amazonaws.com
01:09 Starting pulling prototype images for all servers
01:09 Starting to pull images for ec2-34-244-157-46.eu-west-1.compute.amazonaws.com
01:09 Starting to pull images for ec2-54-154-225-139.eu-west-1.compute.amazonaws.com
01:09 Starting to pull images for ec2-54-229-46-72.eu-west-1.compute.amazonaws.com
01:09 Starting to pull images for ec2-34-240-77-37.eu-west-1.compute.amazonaws.com
01:09 Starting to pull images for ec2-34-245-34-13.eu-west-1.compute.amazonaws.com
01:09 Starting to pull images for ec2-34-255-11-148.eu-west-1.compute.amazonaws.com
01:09 Starting to pull images for ec2-54-171-84-73.eu-west-1.compute.amazonaws.com
01:09 Starting to pull images for ec2-18-203-224-170.eu-west-1.compute.amazonaws.com
01:09 Starting to pull images for ec2-52-30-240-104.eu-west-1.compute.amazonaws.com
01:09 Starting to pull images for ec2-34-242-147-144.eu-west-1.compute.amazonaws.com
01:31 Finished pulling prototype images for all servers
01:31 Deploying containers to Swarm
01:31 Using un-encrypted overlay network
01:37 Creating service log: AUTH.log
01:41 Creating service log: SERVICE2.log
01:46 Creating service log: SERVICE3.log
01:50 Creating service log: SERVICE1.log
01:50 Finished Deploying containers to Swarm
01:50 Wait until all instances are Healthy
02:57 All instances are InService for loadbalancer
02:57 Waiting for all Docker instances to answer
02:57 Docker ready on all instances
02:57 Starting pulling test images for all servers
02:57 Starting to pull images for ec2-34-251-147-243.eu-west-1.compute.amazonaws.com
02:57 Starting to pull images for ec2-63-35-223-173.eu-west-1.compute.amazonaws.com
02:57 Starting to pull images for ec2-34-254-188-78.eu-west-1.compute.amazonaws.com
02:57 Starting to pull images for ec2-52-208-134-103.eu-west-1.compute.amazonaws.com
02:57 Starting to pull images for ec2-18-203-248-164.eu-west-1.compute.amazonaws.com
02:57 Starting to pull images for ec2-52-17-170-102.eu-west-1.compute.amazonaws.com
03:08 Finished pulling test images for all servers
03:08 Start UngracefullChurnInternalGenerator churn
03:08 Starting test against Master-367549416.eu-west-1.elb.amazonaws.com
03:10 Start test slave 1 on ec2-34-251-147-243.eu-west-1.compute.amazonaws.com
03:11 Start test slave 2 on ec2-63-35-223-173.eu-west-1.compute.amazonaws.com
03:12 Start test slave 3 on ec2-34-254-188-78.eu-west-1.compute.amazonaws.com
03:13 Start test slave 4 on ec2-52-208-134-103.eu-west-1.compute.amazonaws.com
03:15 Start test slave 5 on ec2-18-203-248-164.eu-west-1.compute.amazonaws.com
03:18 Client instance is ec2-52-17-170-102.eu-west-1.compute.amazonaws.com
03:18 Running test...
03:38 Killing SERVICE2 + on ec2-52-30-240-104.eu-west-1.compute.amazonaws.com
03:43 Killing SERVICE3 + on ec2-34-244-157-46.eu-west-1.compute.amazonaws.com
03:44 SERVICE2 has been recreated 10/10
03:49 SERVICE3 has been recreated 10/10
04:09 Killing SERVICE2 + on ec2-52-30-240-104.eu-west-1.compute.amazonaws.com
04:14 Killing SERVICE3 + on ec2-34-242-147-144.eu-west-1.compute.amazonaws.com
04:16 SERVICE2 has been recreated 10/10
04:21 SERVICE3 has been recreated 10/10
04:41 Killing SERVICE2 + on ec2-18-203-224-170.eu-west-1.compute.amazonaws.com
04:46 Killing SERVICE3 + on ec2-34-242-147-144.eu-west-1.compute.amazonaws.com
04:48 SERVICE2 has been recreated 10/10
04:53 SERVICE3 has been recreated 10/10
05:13 Killing SERVICE2 + on ec2-34-245-34-13.eu-west-1.compute.amazonaws.com
05:18 Killing SERVICE3 + on ec2-34-245-34-13.eu-west-1.compute.amazonaws.com
05:20 SERVICE2 has been recreated 10/10
05:25 SERVICE3 has been recreated 10/10
05:45 Killing SERVICE2 + on ec2-52-30-240-104.eu-west-1.compute.amazonaws.com
05:50 Killing SERVICE3 + on ec2-34-245-34-13.eu-west-1.compute.amazonaws.com
05:52 SERVICE2 has been recreated 10/10
05:57 SERVICE3 has been recreated 10/10
06:17 Killing SERVICE2 + on ec2-34-242-147-144.eu-west-1.compute.amazonaws.com
06:22 Killing SERVICE3 + on ec2-34-240-77-37.eu-west-1.compute.amazonaws.com
06:23 SERVICE3 has been recreated 10/10
06:24 SERVICE2 has been recreated 10/10
06:48 Killing SERVICE3 + on ec2-54-171-84-73.eu-west-1.compute.amazonaws.com
06:49 Killing SERVICE2 + on ec2-34-242-147-144.eu-west-1.compute.amazonaws.com
06:50 SERVICE2 has been recreated 10/10
06:55 SERVICE3 has been recreated 10/10
07:15 Killing SERVICE2 + on ec2-34-255-11-148.eu-west-1.compute.amazonaws.com
07:20 Killing SERVICE3 + on ec2-18-203-224-170.eu-west-1.compute.amazonaws.com
07:22 SERVICE2 has been recreated 10/10
07:27 SERVICE3 has been recreated 10/10
07:47 Killing SERVICE2 + on ec2-34-244-157-46.eu-west-1.compute.amazonaws.com
07:52 Killing SERVICE3 + on ec2-52-30-240-104.eu-west-1.compute.amazonaws.com
07:54 SERVICE2 has been recreated 10/10
07:59 SERVICE3 has been recreated 10/10
08:19 Killing SERVICE2 + on ec2-34-255-11-148.eu-west-1.compute.amazonaws.com
08:24 Killing SERVICE3 + on ec2-52-30-240-104.eu-west-1.compute.amazonaws.com
08:25 SERVICE3 has been recreated 10/10
08:26 SERVICE2 has been recreated 10/10
08:50 Killing SERVICE3 + on ec2-18-203-224-170.eu-west-1.compute.amazonaws.com
08:51 SERVICE3 has been recreated 10/10
08:51 Killing SERVICE2 + on ec2-34-245-34-13.eu-west-1.compute.amazonaws.com
08:52 SERVICE2 has been recreated 10/10
09:16 Killing SERVICE3 + on ec2-34-240-77-37.eu-west-1.compute.amazonaws.com
09:17 SERVICE3 has been recreated 10/10
09:17 Killing SERVICE2 + on ec2-18-203-224-170.eu-west-1.compute.amazonaws.com
09:18 SERVICE2 has been recreated 10/10
09:31 Test Done
09:32 Getting Docker log: bechhansen-master-slave_latest-exited-c624457445c6981f245869ddfe623c99401f68dbf763fd108d3f4f7437c1e276.log
09:33 Getting Docker log: bechhansen-master-slave_latest-exited-552809acfe81e5cf08b2c5b4051494ac570aa15b42e57a66a703f73557596679.log
09:34 Getting Docker log: bechhansen-master-slave_latest-exited-079f3b029880358e741c4cdecc76df08d2b1f2a0becb9b9c27b955b106f64384.log
09:34 Getting Docker log: bechhansen-master-slave_latest-exited-e97b792fb9c54fd4c9d70b022459fdcace1d4f758ebffdf8001cde1fd99d8e5f.log
09:34 Getting Docker log: bechhansen-master-slave_latest-exited-53a1a090a15332c9337397ab0a34cb9b6cde0866c90f0e221fb81aa6254b0cde.log
09:35 Getting Docker log: bechhansen-master-client_latest-exited-02f2cefa3588a4a218709a66ab1252bd8d9c4efd20c6cd90529ca9ff36e7d994.log
09:35 Getting Docker log: bechhansen-master-service2_1-running-1d95819ab7d24e96251a796704b940d91d66012f64f5c0d8c5b4d7991b4d272c.log
09:36 Getting Docker log: bechhansen-master-service3_1-running-c72e5e587375d05c573c415184900e205cd789233a9ef5040429eb28a0ba84d0.log
09:36 Getting Docker log: bechhansen-master-service1_1-running-0fac902200dd70708fc1ba0da3462dbe8c14b0ba66c74944b590824125ebc29c.log
09:36 Getting Docker log: bechhansen-master-auth_1-running-a7e32b3b28a585080b956525c33252638fd4db389fd19526034e24e87eec21aa.log
09:36 Getting Docker log: bechhansen-master-service1_1-running-bde7c5d5948b8b20008883a1729aed6216263ca59f81c00dd09a7c7085403d95.log
09:37 Getting Docker log: bechhansen-master-service3_1-running-d92681d03728134aa7cbb372a8174ef9a092e590cac9b846c381dc7d2b03db0b.log
09:37 Getting Docker log: bechhansen-master-service2_1-running-b908d2ce502cb68acaf849236e479aa7b3e27376ec39594e6043f8fd7ff66817.log
09:37 Getting Docker log: bechhansen-master-auth_1-running-309507569def6144a4bc32c0eab7146af91b62c5526ffbd36bdb42c3441b0f83.log
09:38 Getting Docker log: bechhansen-master-service1_1-running-3bdbeb1bef92598947e6f522e3a80961687c074ae1be012da82ad18810d61073.log
09:38 Getting Docker log: bechhansen-master-service3_1-running-4ad8193d015af3598da2b0d52b6bda6ffc04ff3e192ec36411995d01b5578172.log
09:38 Getting Docker log: bechhansen-master-service2_1-running-2cb30d19cc1f9debe4fd00d791e0290d7fb5e1e9a6ae0876e3b98a4455f6d61a.log
09:38 Getting Docker log: bechhansen-master-auth_1-running-b1455a5eaf5124d558f46af1f3a3992a8f8f9232d9c8221524e3ca6b4a0dcafa.log
09:39 Getting Docker log: bechhansen-master-service3_1-running-eaacdfb6578fd2cce43d5f7d1fff3214cea27508ec76a89f0840533a19bde74a.log
09:39 Getting Docker log: bechhansen-master-service1_1-running-ef0ecc5d4978191a15654adad7442d3a34d0b3cd4f6d75965bae0c5dc2528bd9.log
09:39 Getting Docker log: bechhansen-master-service2_1-running-5d764db27351e040d7fce8af31af3062adac052e101ad03ea1b575c55bc498b1.log
09:40 Getting Docker log: bechhansen-master-auth_1-running-fece64eac9245fd70b9eaf86adbc72de37cc54c074191402f45754f1cb756840.log
09:40 Getting Docker log: bechhansen-master-service2_1-running-0a979b19f4ac4036f5279c686d5f8e9737d025b97aadbec8c27cd38ea680b3f5.log
09:40 Getting Docker log: bechhansen-master-service3_1-running-0314b949321fb984ee2bd5d1277e3affe83ea9f01dad46bed7d65edea0bdadb0.log
09:40 Getting Docker log: bechhansen-master-service1_1-running-f1bf290b48dda53f408d77ac77de48f23f116e504ad1903ae29dcd841b9cd000.log
09:41 Getting Docker log: bechhansen-master-auth_1-running-91bb89f1200d57a2f36680fbc33007f8a0715dc482c8ade9c3a4798dcad5078b.log
09:41 Getting Docker log: bechhansen-master-service2_1-running-4d2701d2183e2ef3c5100eb1845fb5dc7ec397c07f06ee65f218b763727b36e6.log
09:41 Getting Docker log: bechhansen-master-service1_1-running-77dfb87f5c38603d99851e0ef5332975ae767df58dc199730bf49bc05b9d7a2e.log
09:42 Getting Docker log: bechhansen-master-service3_1-running-6fcf0672d81afe2c5ce58998e4d1b9046ca1c98cca052507c309d4cb680352eb.log
09:42 Getting Docker log: bechhansen-master-auth_1-running-e6fb13b0695dd2c640911f0b1c6aa04dc65add4a9baacda14c4c0a378522d875.log
09:42 Killing SERVICE3 + on ec2-54-154-225-139.eu-west-1.compute.amazonaws.com
09:42 Getting Docker log: bechhansen-master-service3_1-running-0452c433fd943865c54105990165fe6236f92c591694344d3ea242cad6f0c88b.log
09:42 Getting Docker log: bechhansen-master-service1_1-running-4dbc3938435cb50062e4b1295e11fc507c1d2d972e234f1b70ab39f6a804b788.log
09:43 Getting Docker log: bechhansen-master-service2_1-running-f27b200dc95a84c9e963f5abba2a300582ef997846954c3db40d281038ba2530.log
09:43 Getting Docker log: bechhansen-master-auth_1-running-345602232b8eed6312c460361bed663b528ac4d943e7783354ada08e29bcd5ef.log
09:43 Killing SERVICE2 + on ec2-54-154-225-139.eu-west-1.compute.amazonaws.com
09:43 Getting Docker log: bechhansen-master-service2_1-running-02437385b9e027f7a4e697908fc8903e89b10187ef1fa4d04c351b8035707d95.log
09:44 Getting Docker log: bechhansen-master-service3_1-running-ed5dcf2e68214ee27b860bb42812d7bd8d0faffaf914aff1cb8a03734bc7cb0a.log
09:44 Getting Docker log: bechhansen-master-service1_1-running-0808f253540557ded2fbf2b6bb287d72850c92eff5f98c883596787f5ec1ef3d.log
09:44 Getting Docker log: bechhansen-master-auth_1-running-fcca35f5c13a3a1c5f9ee4b9707e8905eda79b39dffd74684ed94b90fcb20cb7.log
09:45 Getting Docker log: bechhansen-master-service3_1-running-6fe034005f40b05987df7c3c5a54c6d1c323617300d4f062cca1acaa05874b85.log
09:45 Getting Docker log: bechhansen-master-service2_1-running-4e15ede7cc3d1f6b662b92118e6da32201bea098224fbd91e3ef0a4be3419e52.log
09:45 Getting Docker log: bechhansen-master-service1_1-running-0fd3688d1d01ee9a2603d8353c1f3a209ea752170a1e490e018f8ccfafb20a93.log
09:45 Getting Docker log: bechhansen-master-auth_1-running-30eed5f3a780402c07888ade49f8ec9a1d13ba6d9d218357b6598e23e6536fce.log
09:46 Getting Docker log: bechhansen-master-service2_1-running-ce8223583448112f9f69651cb78a615631d5d67c2220fdd06c2acd06c7238579.log
09:46 Getting Docker log: bechhansen-master-service3_1-running-748ce6bc9dc2d348c05ebbbf811ae6421404621d5f79ec77ac837b0d6e52eb66.log
09:46 Getting Docker log: bechhansen-master-service1_1-running-76be91c589d4ccf9033c019677655bd046b8c5bd9e66bce5a8f6117b1b46a03f.log
09:46 Getting Docker log: bechhansen-master-auth_1-running-437e3c959e26ee5795d6ab58e9a0f9331240b32c6781d829651cd5902238f3ec.log
09:47 End log {SERVICE2=10, AUTH=10, SERVICE3=10, SERVICE1=10}
09:47 Terminating all EC2 instances
09:48 Terminated all EC2 instances
