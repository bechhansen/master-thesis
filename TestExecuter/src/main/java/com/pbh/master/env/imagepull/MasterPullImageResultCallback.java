package com.pbh.master.env.imagepull;

import com.amazonaws.services.ec2.model.Instance;
import com.github.dockerjava.core.command.PullImageResultCallback;
import com.pbh.master.env.Factory;
import com.pbh.master.env.Service;
import com.pbh.master.env.log.ProgressLogger;

public class MasterPullImageResultCallback extends PullImageResultCallback {
	
	private Service service;
	private String tag;
	private String server;
	private ProgressLogger logger;

	public MasterPullImageResultCallback(Factory factory, Service service, String tag, Instance server) {
		this.service = service;
		this.tag = tag;
		this.server = server.getPublicDnsName();
		logger = factory.getProgressLogger();
	}

	@Override
    public void onError(Throwable throwable) {
    	logger.info(throwable.getMessage());
        super.onError(throwable);
    }

	@Override
	public void onComplete() {
		//logger.info("Image pulled: " + service.getImage() + ":" + tag + " on " + server);
		super.onComplete();
	}

}
