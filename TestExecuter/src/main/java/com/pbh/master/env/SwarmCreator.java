package com.pbh.master.env;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.amazonaws.services.ec2.model.Instance;
import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.model.Swarm;
import com.github.dockerjava.api.model.SwarmCAConfig;
import com.github.dockerjava.api.model.SwarmDispatcherConfig;
import com.github.dockerjava.api.model.SwarmOrchestration;
import com.github.dockerjava.api.model.SwarmRaftConfig;
import com.github.dockerjava.api.model.SwarmSpec;
import com.github.dockerjava.api.model.TaskDefaults;
import com.github.dockerjava.core.DefaultDockerClientConfig;
import com.github.dockerjava.core.DockerClientBuilder;
import com.pbh.master.env.log.ProgressLogger;

public class SwarmCreator {

	private static final int MANAGERMAX = 3;
	private List<Instance> instances;
	private List<String> managers = new ArrayList<>();

	private int count;
	private Swarm swarm;
	private ProgressLogger logger;

	public SwarmCreator(Factory factory, List<Instance> instances) {
		this.instances = instances;
		logger = factory.getProgressLogger();
	}

	public void createSwarm() {

		for (Instance host : instances) {

			forHost(host);

			count++;
		}
		// TODO Auto-generated method stub

	}

	private void forHost(Instance host) {
		
		DefaultDockerClientConfig config = DefaultDockerClientConfig.createDefaultConfigBuilder()
				.withDockerCertPath("/Users/pbh/Projects/Master/Hovedopgave/Project/AWS/cert")
				.withDockerTlsVerify("1")
				.withDockerHost("tcp://" + host.getPublicDnsName() + ":2376").build();
		DockerClient dockerClient = DockerClientBuilder.getInstance(config).build();
		
		
		if (count == 0) {
	
			dockerClient.initializeSwarmCmd(createSwarmSpec())
			.withAdvertiseAddr(host.getPrivateIpAddress() + ":2377")
			.withListenAddr("0.0.0.0:2377")
			.withForceNewCluster(false)
			.exec();
			swarm = dockerClient.inspectSwarmCmd().exec();
			
			managers.add(host.getPrivateIpAddress());
			
			logger.info("Swarm initialized by " + host.getPublicDnsName());
	
		} else if (count < MANAGERMAX) {
			joinSwarm(dockerClient, host, swarm.getJoinTokens().getManager());	
			managers.add(host.getPrivateIpAddress());
			
			logger.info("Swarm manager added " + host.getPublicDnsName());
		
		} else {
			joinSwarm(dockerClient, host, swarm.getJoinTokens().getWorker());
			logger.info("Swarm worker added  " + host.getPublicDnsName());
		}
	}

	private void joinSwarm(DockerClient dockerClient, Instance host, String token) {
		List<String> remotes = managers.stream().map(i -> i + ":2377").collect(Collectors.toList());
		
		dockerClient.joinSwarmCmd()
		.withJoinToken(token)
		.withAdvertiseAddr(host.getPrivateIpAddress() + ":2377")
		.withListenAddr("0.0.0.0:2377")
		.withRemoteAddrs(remotes).exec();
	}

	private SwarmSpec createSwarmSpec() {
		
		SwarmCAConfig caConfig = new SwarmCAConfig();
		SwarmDispatcherConfig dispatcher = new SwarmDispatcherConfig();
		SwarmOrchestration orchestration = new SwarmOrchestration();
		SwarmRaftConfig raft = new SwarmRaftConfig();
		TaskDefaults taskDefaults = new TaskDefaults();
		
		SwarmSpec spec = new SwarmSpec()
		.withName("default")
		.withCaConfig(caConfig)
		.withDispatcher(dispatcher)
		.withOrchestration(orchestration)
		.withRaft(raft)
		.withTaskDefaults(taskDefaults);

		return spec;
	}
	
	/*public static void main(String[] args) {
		SwarmCreator swarmCreator = new SwarmCreator(Collections.singletonList("ec2-63-32-111-68.eu-west-1.compute.amazonaws.com"));
		swarmCreator.createSwarm();
	}*/

}
