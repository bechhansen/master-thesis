package com.pbh.master.env.log;

import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

import com.pbh.master.env.Service;

public class SwarmTaskLogRecord {

	private Map<Service, Set<String>> currentTaskMap;
	private Map<Service, Set<String>> historyTaskMap;

	public SwarmTaskLogRecord(Map<Service, Set<String>> currentTaskMap, Map<Service, Set<String>> historyTaskMap) {
		this.currentTaskMap = currentTaskMap;
		this.historyTaskMap = historyTaskMap;		
	}
	
	public String toString() {
		
		StringBuilder sb = new StringBuilder();
		
		sb.append(System.currentTimeMillis());
		
		Stream.of(Service.values()).filter(service -> !service.isTest()).forEach(service -> {
			sb
			.append(";")
			.append(currentTaskMap.get(service).size())
			.append(";")
			.append(historyTaskMap.get(service).size());
		});
		
		return sb.append(System.lineSeparator()).toString();
	}
}
