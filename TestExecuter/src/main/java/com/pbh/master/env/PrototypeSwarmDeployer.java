package com.pbh.master.env;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.amazonaws.services.ec2.model.Instance;
import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.CreateNetworkResponse;
import com.github.dockerjava.api.command.CreateServiceResponse;
import com.github.dockerjava.api.command.LogSwarmObjectCmd;
import com.github.dockerjava.api.model.ContainerSpec;
import com.github.dockerjava.api.model.EndpointResolutionMode;
import com.github.dockerjava.api.model.EndpointSpec;
import com.github.dockerjava.api.model.NetworkAttachmentConfig;
import com.github.dockerjava.api.model.PortConfig;
import com.github.dockerjava.api.model.PortConfigProtocol;
import com.github.dockerjava.api.model.ServiceModeConfig;
import com.github.dockerjava.api.model.ServiceReplicatedModeOptions;
import com.github.dockerjava.api.model.ServiceSpec;
import com.github.dockerjava.api.model.TaskSpec;
import com.github.dockerjava.api.model.UpdateConfig;
import com.pbh.master.env.log.LogServiceCallback;
import com.pbh.master.env.log.ProgressLogger;

public class PrototypeSwarmDeployer implements PrototypeDeployer {

	private List<Instance> instances;
	private Factory factory;
	private ConfigurationProvider conf;
	private ProgressLogger logger;

	public PrototypeSwarmDeployer(ConfigurationProvider conf, Factory factory, List<Instance> instances) {
		this.conf = conf;
		this.factory = factory;
		this.instances = instances;
		logger = factory.getProgressLogger();
	}

	public Optional<ServiceDepoyerResponse> deploy() {
		
		if (instances != null && !instances.isEmpty()) {
		
			logger.info("Deploying containers to Swarm");
			
			DockerClient dockerClient = factory.getClient(instances.get(0));
			
			CreateNetworkResponse network = createOverlayNetwork(dockerClient);
			
			String authId = deployContainers(dockerClient, Service.AUTH, "1", 8080, network);
			String service2Id = deployContainers(dockerClient, Service.SERVICE2, "1", 8082, network);
			String service3Id = deployContainers(dockerClient, Service.SERVICE3, "1", 8083, network);
			String service1Id = deployContainers(dockerClient, Service.SERVICE1, "1", 8081, network);
			
			logger.info("Finished Deploying containers to Swarm");
			
			return Optional.of(new ServiceDepoyerResponse(authId, service1Id, service2Id, service3Id));
		}
		
		return Optional.empty();
	}

	private CreateNetworkResponse createOverlayNetwork(DockerClient dockerClient) {
		return dockerClient.createNetworkCmd()
		.withName("Master")
		.withDriver("overlay")
		.withOptions(getNetworkOptions())
		.exec();
	}

	private Map<String, String> getNetworkOptions() {
		Map<String, String> options = new HashMap<String, String>();
		if (conf.isEncrypted()) {
			options.put("encrypted", "");
			options.put("secure", "");
			logger.info("Using encrypted overlay network");
		} else {
			logger.info("Using un-encrypted overlay network");
		}
		return options;
	}

	private String deployContainers(DockerClient dockerClient, Service service, String version, int port, CreateNetworkResponse network) {
		ServiceModeConfig modeConfig = new ServiceModeConfig();
		ServiceReplicatedModeOptions replicated = new ServiceReplicatedModeOptions();
		replicated.withReplicas(instances.size());
		modeConfig.withReplicated(replicated);
		
		EndpointSpec endpointSpec = new EndpointSpec();
		List<PortConfig> ports = new ArrayList<>();
		ports.add(new PortConfig().
				withPublishedPort(port).
				withProtocol(PortConfigProtocol.TCP).withTargetPort(8080));
		endpointSpec.withPorts(ports);
		endpointSpec.withMode(EndpointResolutionMode.VIP);
		
		List<NetworkAttachmentConfig> networks = assignToNetwork(network);
		
		List<String> env = getEnvironmentVariables();
		ContainerSpec containerSpec = new ContainerSpec()
				.withImage(service.getImage() + ":" + version)
				.withEnv(env);
		
		Long withStopGracePeriod = conf.withStopGracePeriod();
		if (withStopGracePeriod != null) {
			containerSpec.withStopGracePeriod(withStopGracePeriod);
		}
		
		TaskSpec taskTemplate = new TaskSpec().withContainerSpec(containerSpec);
		UpdateConfig updateConfig = new UpdateConfig();
		updateConfig.withDelay(0L).withParallelism(service.getUpdateParallelism()).
		withOrder(service.getUpdateOrder()).withMaxFailureRatio(0f);
		
		ServiceSpec serviceSpec = new ServiceSpec().withName(service.name().toLowerCase()).
		withMode(modeConfig).withEndpointSpec(endpointSpec).
		withNetworks(networks).withUpdateConfig(updateConfig).
		withTaskTemplate(taskTemplate);
		
		CreateServiceResponse exec = dockerClient.createServiceCmd(serviceSpec).exec();
		String id = exec.getId();		
		serviceLogsConfig(service, id);	
		return id; 
	}

	private void serviceLogsConfig(Service service, String id) {
		if (conf.followServiceLogs() ) {
			DockerClient client = factory.getClient(instances.get(0));
			LogSwarmObjectCmd cmd = client.logServiceCmd(id).withDetails(true).withFollow(true).withStdout(true).withStderr(true).withTimestamps(false);
			LogServiceCallback logExec = cmd.exec(new LogServiceCallback(factory, conf, service));
			try {
				logExec.awaitStarted();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private List<NetworkAttachmentConfig> assignToNetwork(CreateNetworkResponse network) {
		List<NetworkAttachmentConfig> networks = new ArrayList<>();
		NetworkAttachmentConfig net = new NetworkAttachmentConfig();
		networks.add(net);
		net.withTarget(network.getId());
		return networks;
	}

	private List<String> getEnvironmentVariables() {
		List<String> env = new ArrayList<>();
		env.add("DOCKERHOST={{.Node.Hostname}}");
		
		if(conf.prototypeGraceFullShutdown()) {
			env.add("GRACEFUL_SHUTDOWN=TRUE");
		}
		
		if(conf.enableRetry()) {
			env.add("RETRY=TRUE");
		}
		
		if(conf.keepAlive()) {
			env.add("KEEP_ALIVE=TRUE");
		} else {
			env.add("KEEP_ALIVE=FALSE");
		}
		
		env.add("JAVA_OPTS=-Xmx256m -Xmx128m");
		
		Integer prototypeGraceFullShutdownTimeout = conf.prototypeGraceFullShutdownTimeout();
		if(prototypeGraceFullShutdownTimeout != null) {
			env.add("GRACEFUL_SHUTDOWN_TIMEOUT=" + prototypeGraceFullShutdownTimeout);
		}
		return env;
	}
}
