package com.pbh.master.env.log;

import java.util.Collection;
import java.util.function.Function;

public class NOOPTaskLogger implements TaskLogger {

	@Override
	public void startLogging() {

	}

	@Override
	public void endLogging() {

	}

	@Override
	public void addImageStatusListener(Function<Collection<String>, Boolean> listener) {
		
	}

}
