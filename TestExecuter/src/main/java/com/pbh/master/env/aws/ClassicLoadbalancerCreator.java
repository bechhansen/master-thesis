package com.pbh.master.env.aws;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.elasticloadbalancing.AmazonElasticLoadBalancing;
import com.amazonaws.services.elasticloadbalancing.AmazonElasticLoadBalancingClientBuilder;
import com.amazonaws.services.elasticloadbalancing.model.ConfigureHealthCheckRequest;
import com.amazonaws.services.elasticloadbalancing.model.CreateLoadBalancerRequest;
import com.amazonaws.services.elasticloadbalancing.model.CreateLoadBalancerResult;
import com.amazonaws.services.elasticloadbalancing.model.DeleteLoadBalancerRequest;
import com.amazonaws.services.elasticloadbalancing.model.DescribeInstanceHealthRequest;
import com.amazonaws.services.elasticloadbalancing.model.DescribeInstanceHealthResult;
import com.amazonaws.services.elasticloadbalancing.model.HealthCheck;
import com.amazonaws.services.elasticloadbalancing.model.InstanceState;
import com.amazonaws.services.elasticloadbalancing.model.Listener;
import com.amazonaws.services.elasticloadbalancing.model.RegisterInstancesWithLoadBalancerRequest;
import com.amazonaws.services.elasticloadbalancing.model.RegisterInstancesWithLoadBalancerResult;
import com.pbh.master.env.Factory;
import com.pbh.master.env.log.ProgressLogger;

public class ClassicLoadbalancerCreator implements LoadbalancerCreator {

	private static final String LOADBALANCER_NAME = "Master";
	private AWSCredentialsProvider credentialsProvider;
	private ProgressLogger logger;

	public ClassicLoadbalancerCreator(Factory factory, AWSCredentialsProvider credentialsProvider) {
		this.credentialsProvider = credentialsProvider;
		factory.getProgressLogger();
	}

	public String createLoadbalancer(List<Instance> instances) {
		
		AmazonElasticLoadBalancing loadbalancerClient = createLoadbalancerClient();	
		String loadBalancerName = createLoadbalancer(loadbalancerClient);
		registerInstancesInLoadbalancer(loadbalancerClient, instances);
		return loadBalancerName;
	}
	
	private String createLoadbalancer(AmazonElasticLoadBalancing loadbalancerClient) {
		logger.info("Create loadbalancer");
		CreateLoadBalancerRequest request = new CreateLoadBalancerRequest();
		request.setLoadBalancerName(LOADBALANCER_NAME);
		
		Listener authListener = new Listener();
		authListener.setInstancePort(8080);
		authListener.setLoadBalancerPort(8080);
		authListener.setInstanceProtocol("HTTP");
		authListener.setProtocol("HTTP");
		
		Listener service1Listener = new Listener();
		service1Listener.setInstancePort(8081);
		service1Listener.setLoadBalancerPort(8081);
		service1Listener.setInstanceProtocol("HTTP");
		service1Listener.setProtocol("HTTP");
		Collection<Listener> listeners = Stream.of(authListener, service1Listener).collect(Collectors.toSet());
		request.setListeners(listeners);
		
		Collection<String> securityGroups = new ArrayList<>();
		securityGroups.add("sg-0a3795da85c6e34b9");
		request.setSecurityGroups(securityGroups );
		//request.setAvailabilityZones(Collections.singleton("eu-west-1a"));
		request.setSubnets(Collections.singleton("subnet-05f5e12a71efa3259"));
		
		
		
		CreateLoadBalancerResult createLoadBalancer = loadbalancerClient.createLoadBalancer(request);
		String dnsName = createLoadBalancer.getDNSName();
		logger.info("Created loadbalancer: " + dnsName);
		
		ConfigureHealthCheckRequest configureHealthCheckRequest = new ConfigureHealthCheckRequest();
		configureHealthCheckRequest.setLoadBalancerName(LOADBALANCER_NAME);
		HealthCheck healthCheck = new HealthCheck();
		healthCheck.setHealthyThreshold(2);
		healthCheck.setTimeout(5);
		healthCheck.setUnhealthyThreshold(10);
		healthCheck.setInterval(10);
		healthCheck.setTarget("HTTP:8081/healthcheck");
		
		configureHealthCheckRequest.setHealthCheck(healthCheck);
		loadbalancerClient.configureHealthCheck(configureHealthCheckRequest);
		logger.info("Configured loadbalancer healthcheck");
		
		return dnsName;
	}

	private void registerInstancesInLoadbalancer(AmazonElasticLoadBalancing loadbalancerClient, List<Instance> runningInstances) {
		logger.info("Adding instances to loadbalancer");
		
		List<com.amazonaws.services.elasticloadbalancing.model.Instance> instancesForLB = runningInstances.
				stream().
				map(instance -> new com.amazonaws.services.elasticloadbalancing.model.Instance(instance.getInstanceId())).
				collect(Collectors.toList());
		
		RegisterInstancesWithLoadBalancerRequest lbRequest = new RegisterInstancesWithLoadBalancerRequest();
		lbRequest.setLoadBalancerName(LOADBALANCER_NAME);
		lbRequest.setInstances(instancesForLB);
		
		RegisterInstancesWithLoadBalancerResult result = loadbalancerClient.registerInstancesWithLoadBalancer(lbRequest);
		
		logger.info("Added " + result.getInstances().size() + " instances to loadbalancer");
		
	}
	
	private AmazonElasticLoadBalancing createLoadbalancerClient() {
		return AmazonElasticLoadBalancingClientBuilder
            	.standard()
            	.withCredentials(credentialsProvider)
            	.withRegion(Regions.EU_WEST_1)
            	.build();

	}
	
	public void waitUntilLoadBalancerReady() {
		
		AmazonElasticLoadBalancing client = createLoadbalancerClient();
		
		DescribeInstanceHealthRequest req = new DescribeInstanceHealthRequest();
		req.withLoadBalancerName(LOADBALANCER_NAME);
		
		logger.info("Wait until all instances are InService");
		
		while (true) {
		
			DescribeInstanceHealthResult health = client.describeInstanceHealth(req);
			List<InstanceState> instanceStates = health.getInstanceStates();
			
			boolean allMatch = instanceStates.stream().allMatch(state -> state.getState().equals("InService"));
			if (allMatch) {
				logger.info("All instances are InService for loadbalancer");
				break;
			} else {
				try {
					Thread.sleep(1000);
					//System.out.print(".");
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	public void removeLoadBalancer() {
		AmazonElasticLoadBalancing loadbalancerClient = createLoadbalancerClient();	
		DeleteLoadBalancerRequest deleteLoadBalancerRequest = new DeleteLoadBalancerRequest(LOADBALANCER_NAME);
		loadbalancerClient.deleteLoadBalancer(deleteLoadBalancerRequest );
		
		logger.info("Removed load balancer");
		
	}
}
