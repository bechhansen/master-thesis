package com.pbh.master.env;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import com.pbh.master.env.log.ProgressLogger;

public class ConfigurationProvider {

	private ProgressLogger logger;
	private String date;

	public ConfigurationProvider(Factory factory) {

		factory.setConfigurationProvider(this);

		ZonedDateTime now = ZonedDateTime.now();
		date = DateTimeFormatter.ofPattern("yyyy-MM-dd HH.mm").format(now);

		logger = factory.getProgressLogger();
	}

	public int getTestNodeCount() {
		String testNodeCountVar = System.getenv("TESTNODECOUNT");
		if (testNodeCountVar != null) {
			try {
				return Integer.parseInt(testNodeCountVar);
			} catch (NumberFormatException e) {
				logger.info("Invalid swarm node count. Defaulting to " + 3);
			}
		}
		return 3;
	}

	public int getSwarmNodeCount() {
		String swarmNodeCountVar = System.getenv("NODECOUNT");
		if (swarmNodeCountVar != null) {
			try {
				return Integer.parseInt(swarmNodeCountVar);
			} catch (NumberFormatException e) {
				logger.info("Invalid swarm node count. Defaulting to " + 5);
			}
		}
		return 5;
	}

	public String getOutputFolder() {
		String pathStr = System.getenv().getOrDefault("RESULT_PATH", ".");
		Path path = Paths.get(pathStr + "/" + date);

		try {
			Files.createDirectories(path);
		} catch (IOException e) {
			logger.info("Folder " + pathStr + " already exist");
		}

		return path.toString();
	}

	public boolean isSwarm() {
		String mode = System.getenv("MODE");
		return mode != null && mode.equalsIgnoreCase("SWARM");
	}

	public boolean isManualTest() {
		String mode = System.getenv("TEST");
		return mode != null && mode.equalsIgnoreCase("MANUAL");
	}

	public boolean enableChurn() {
		String mode = System.getenv().getOrDefault("CHURN", "FALSE");
		return Boolean.parseBoolean(mode);
	}

	public boolean enableRetry() {
		String mode = System.getenv().getOrDefault("RETRY", "FALSE");
		return Boolean.parseBoolean(mode);
	}

	public Long withStopGracePeriod() {
		String mode = System.getenv("STOPGRACEPERIOD");
		if (mode != null) {
			return Long.valueOf(mode);
		}

		return null;
	}

	public boolean prototypeGraceFullShutdown() {
		String mode = System.getenv().getOrDefault("GRACEFUL_SHUTDOWN", "TRUE");
		return Boolean.parseBoolean(mode);
	}

	public boolean keepAlive() {
		String mode = System.getenv().getOrDefault("KEEP_ALIVE", "TRUE");
		return Boolean.parseBoolean(mode);
	}

	public Integer prototypeGraceFullShutdownTimeout() {
		String mode = System.getenv().getOrDefault("GRACEFUL_SHUTDOWN_TIMEOUT", "600");
		if (mode != null) {
			return Integer.valueOf(mode);
		}

		return null;
	}

	public Integer getTestDuration() {
		String mode = System.getenv().getOrDefault("TEST_DURATION", "120");
		if (mode != null) {
			return Integer.valueOf(mode);
		}
		return null;
	}

	public Integer getTestThreads() {
		String mode = System.getenv().getOrDefault("TEST_THREADS", "150");
		if (mode != null) {
			return Integer.valueOf(mode);
		}
		return null;
	}

	public Integer getTestRampUpSeconds() {
		String mode = System.getenv().getOrDefault("TEST_RAMP_UP_SECONDS", "15");
		if (mode != null) {
			return Integer.valueOf(mode);
		}
		return null;
	}

	public void print() {

		logger.info("------------------ CONFIGURATION---------------------");
		logConf("SwarmNode count", getSwarmNodeCount());
		logConf("TestNode count", getTestNodeCount());
		logConf("Swarm deployment", isSwarm());
		logConf("Encrypted", isEncrypted());
		logConf("Manual test", isManualTest());
		logConf("Churn enabled", enableChurn());
		logConf("Churn type", getChurnType());
		logConf("Stop grace period", withStopGracePeriod());
		logConf("Gracefull shutdown", prototypeGraceFullShutdown());
		logConf("Gracefull shutdown timeout", prototypeGraceFullShutdownTimeout());
		logConf("Keep-alive", keepAlive());
		logConf("Test duration", getTestDuration());
		logConf("Test Threads", getTestThreads());
		logConf("Test rampup", getTestRampUpSeconds());
		logConf("Disable load balancer", disableLoadBalancer());
		logConf("Enable Retry", enableRetry());
		logConf("JMeter mode", getJMeterMode());
		logConf("Get service logs", followServiceLogs());

		logger.info("-----------------------------------------------------");
	}

	private void logConf(String string, Object value) {
		logger.info(String.format("%-30s %-5s", string, value));

	}

	public boolean isEncrypted() {
		String envEncrypted = System.getenv("ENCRYPTED");
		return envEncrypted != null && envEncrypted.equalsIgnoreCase(Boolean.TRUE.toString());
	}

	public boolean disableLoadBalancer() {
		String envDisableLB = System.getenv("DISABLE_LOADBALANCER");
		return envDisableLB != null && envDisableLB.equalsIgnoreCase(Boolean.TRUE.toString());
	}

	public String getChurnType() {
		return System.getenv().getOrDefault("CHURNTYPE", "GRACEFUL");
	}

	public static int getIterations() {
		String testNodeCountVar = System.getenv("ITERATIONS");
		if (testNodeCountVar != null) {
			try {
				return Integer.parseInt(testNodeCountVar);
			} catch (NumberFormatException e) {
				System.out.println("Invalid Iterations. Defaulting to " + 1);
			}
		}
		return 1;
	}

	public String getJMeterMode() {
		return System.getenv().getOrDefault("JMETERMODE", "StrippedBatch");
	}

	public boolean followServiceLogs() {
		String servideLogs = System.getenv("SERVICE_LOGS");
		return servideLogs != null && servideLogs.equalsIgnoreCase(Boolean.TRUE.toString());
	}
}
