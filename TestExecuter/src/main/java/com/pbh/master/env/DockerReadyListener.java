package com.pbh.master.env;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import com.amazonaws.services.ec2.model.Instance;
import com.pbh.master.env.log.ProgressLogger;

public class DockerReadyListener {

	private Factory factory;
	private ProgressLogger logger;

	public DockerReadyListener(Factory factory) {
		this.factory = factory;
		logger = factory.getProgressLogger();
	}

	public void waitForDockerReady(List<Instance> instances) {

		if (instances != null && !instances.isEmpty()) {

			try {
				logger.info("Waiting for all Docker instances to answer");

				ExecutorService executor = Executors.newFixedThreadPool(instances.size());
				List<DockerReadyCallable> collect = instances.stream().map(instance -> new DockerReadyCallable(factory, instance)).collect(Collectors.toList());
				executor.invokeAll(collect);
				executor.shutdown();

				logger.info("Docker ready on all instances");
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
