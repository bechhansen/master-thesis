package com.pbh.master.env.aws;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.DescribeInstanceStatusRequest;
import com.amazonaws.services.ec2.model.DescribeInstanceStatusResult;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.InstanceStatus;
import com.amazonaws.services.ec2.model.RunInstancesRequest;
import com.amazonaws.services.ec2.model.RunInstancesResult;
import com.amazonaws.services.ec2.model.TerminateInstancesRequest;
import com.pbh.master.env.Factory;
import com.pbh.master.env.log.ProgressLogger;

public class EC2Creator {

	private static final String IMAGE_ID = "ami-0837f073092226216";
	
	private AWSCredentialsProvider credentialsProvider;
	private List<Instance> allInstances = new ArrayList<>();
	private ProgressLogger logger;

	public EC2Creator(Factory factory, AWSCredentialsProvider credentialsProvider) {
		this.credentialsProvider = credentialsProvider;
		logger = factory.getProgressLogger();
	}


	public List<Instance> createSwarmInstances(int instanceCount) throws InterruptedException {
		AmazonEC2 client = createEC2Client();
		return createSwarmInstances(client, instanceCount);
	}
	
	public List<Instance> createJMeterInstances(int instanceCount) throws InterruptedException {
		AmazonEC2 client = createEC2Client();
		return createJMeterInstances(client, instanceCount);
	}


	private AmazonEC2 createEC2Client() {
		return AmazonEC2ClientBuilder.standard().withCredentials(credentialsProvider).withRegion(Regions.EU_WEST_1).build();
	}
	
	private List<Instance> createSwarmInstances(AmazonEC2 client, int instanceCount) throws InterruptedException {
		
		if (instanceCount == 0) {
			return Collections.emptyList();
		}
		
		RunInstancesRequest runInstancesRequest = new RunInstancesRequest();
		runInstancesRequest.withImageId(IMAGE_ID). //ami-0bbc25e23a7640b9b Default 
		//withInstanceType("t2.micro").
		//withInstanceType("t2.small").
		withInstanceType("c5.large").
		
		withMinCount(instanceCount).withMaxCount(instanceCount).
		//withSubnetId("subnet-05f5e12a71efa3259").  //a
		withSubnetId("subnet-04cdeeec704101edc").		//c
		withSecurityGroupIds("sg-0a3795da85c6e34b9").
		withKeyName("Master");
		
		logger.info("Run " + instanceCount + " EC2 instances for Docker Swarm cluster");
		RunInstancesResult result = client.runInstances(runInstancesRequest);
		
		List<Instance> instances = result.getReservation().getInstances();
		List<String> instanceIds = instances.stream().map(instance -> instance.getInstanceId()).collect(Collectors.toList());
		
		logger.info("EC2 Swarm Instances starting...");	
		waitUntilStarted(client, instanceIds); 	
		logger.info("EC2 Swarm Instances started");
			
		return getInstanceDescriptions(client, instanceIds);
	}
	
	private List<Instance> createJMeterInstances(AmazonEC2 client, int instanceCount) throws InterruptedException {
		
		if (instanceCount == 0) {
			return Collections.emptyList();
		}
		
		RunInstancesRequest runInstancesRequest = new RunInstancesRequest();
		runInstancesRequest.withImageId(IMAGE_ID). //ami-0bbc25e23a7640b9b Default 
		//withInstanceType("t2.micro").
		//withInstanceType("t2.medium").
		withInstanceType("c5.large").
		withMinCount(instanceCount).withMaxCount(instanceCount).
		//withSubnetId("subnet-05f5e12a71efa3259"). //a
		withSubnetId("subnet-04cdeeec704101edc").		//c
		withSecurityGroupIds("sg-03e434c408dfce334", "sg-0a3795da85c6e34b9").
		
		withKeyName("Master");
		
		logger.info("Run " + instanceCount + " EC2 instances for JMeter cluster");
		RunInstancesResult result = client.runInstances(runInstancesRequest);
		
		List<Instance> instances = result.getReservation().getInstances();
		List<String> instanceIds = instances.stream().map(instance -> instance.getInstanceId()).collect(Collectors.toList());
		
		logger.info("EC2 JMeter instances starting...");	
		waitUntilStarted(client, instanceIds); 	
		logger.info("Done!");
			
		return getInstanceDescriptions(client, instanceIds);
	}


	private List<Instance> getInstanceDescriptions(AmazonEC2 client, List<String> instanceIds) {
		DescribeInstancesRequest r = new DescribeInstancesRequest();
		r.withInstanceIds(instanceIds);
		
		DescribeInstancesResult describeInstances = client.describeInstances(r);
		List<Instance> collect = describeInstances.getReservations().stream().flatMap(reservation -> reservation.getInstances().stream()).collect(Collectors.toList());
		allInstances.addAll(collect);
		return collect;
	}


	private void waitUntilStarted(AmazonEC2 client, List<String> instanceIds) throws InterruptedException {
		
		DescribeInstanceStatusRequest request = new DescribeInstanceStatusRequest().withIncludeAllInstances(true).withInstanceIds(instanceIds);
		
		while (true) {
			
			DescribeInstanceStatusResult describeInstanceStatus = client.describeInstanceStatus(request);
			List<InstanceStatus> instanceStatuses = describeInstanceStatus.getInstanceStatuses();
			
			Optional<InstanceStatus> findAny = instanceStatuses.stream().filter(status -> !status.getInstanceState().getName().equals("running")).findAny();
			
			if (!findAny.isPresent()) {
				break;
			}
			//System.out.print(".");
			Thread.sleep(1000);
		}
	}

	public void removeInstances() {
		
		logger.info("Terminating all EC2 instances");
		
		AmazonEC2 client = createEC2Client();
		List<String> instanceIds = allInstances.stream().map(Instance::getInstanceId).collect(Collectors.toList());
		TerminateInstancesRequest terminateInstancesRequest = new TerminateInstancesRequest(instanceIds);
		client.terminateInstances(terminateInstancesRequest);
		
		logger.info("Terminated all EC2 instances");
	}
}
