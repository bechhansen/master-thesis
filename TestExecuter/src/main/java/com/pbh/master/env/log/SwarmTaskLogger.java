package com.pbh.master.env.log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.amazonaws.services.ec2.model.Instance;
import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.model.Task;
import com.github.dockerjava.api.model.TaskState;
import com.pbh.master.env.ConfigurationProvider;
import com.pbh.master.env.Factory;
import com.pbh.master.env.Service;

public class SwarmTaskLogger implements Runnable, TaskLogger {

	private Factory factory;
	private List<Instance> swarmInstances;
	private boolean logging;

	private Map<Service, Set<String>> historyTaskMap = createTaskMap();
	private ScheduledExecutorService scheduler;
	private ScheduledFuture<?> scheduleAtFixedRate;
	private ConfigurationProvider conf;
	private FileOutputStream outputStream;
	private Collection<Function<Collection<String>, Boolean>> fimageStatusListeners = new CopyOnWriteArrayList<>();
	private ProgressLogger logger;

	public SwarmTaskLogger(Factory factory, ConfigurationProvider conf, List<Instance> swarmInstances) {
		this.factory = factory;
		this.conf = conf;
		this.swarmInstances = swarmInstances;
		logger = factory.getProgressLogger();

		scheduler = Executors.newScheduledThreadPool(5);
	}

	public void startLogging() {
		logging = true;

		try {
			File file = new File(conf.getOutputFolder(), "TasksStatistiscs.csv");
			if (file.isFile() && file.exists()) {
				file.delete();
			}
			file.createNewFile();
			outputStream = new FileOutputStream(file);
			outputStream.write(getHeader());

			// Open file for writing and add header row
			scheduleAtFixedRate = scheduler.scheduleAtFixedRate(this, 0, 1, TimeUnit.SECONDS);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private byte[] getHeader() {

		StringBuilder sb = new StringBuilder();
		sb.append("Time");

		Stream.of(Service.values()).filter(service -> !service.isTest()).forEach(service -> {
			sb.append(";").append(service.readableName()).append(";").append(service.readableName() + " Sum");
		});

		return sb.append(System.lineSeparator()).toString().getBytes();
	}

	public void endLogging() {

		if (logging && !swarmInstances.isEmpty()) {

			scheduleAtFixedRate.cancel(false);

			DockerClient client = factory.getClient(swarmInstances.get(0));
			List<Task> tasks = client.listTasksCmd().exec();
			Map<Service, Long> taskCount = tasks.stream().collect(Collectors.groupingBy(task -> Service.parse(task.getSpec().getContainerSpec().getImage()), Collectors.counting()));
			logger.info("End log " + taskCount);

			try {
				outputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

			scheduler.shutdown();
		}
	}

	@Override
	public void run() {
		
		DockerClient client = factory.getClient(swarmInstances.get(0));
		List<Task> tasks = client.listTasksCmd().withStateFilter(TaskState.RUNNING).exec();

		Map<Service, Set<String>> currentTaskMap = createTaskMap();

		for (Task task : tasks) {
			String image = task.getSpec().getContainerSpec().getImage();

			Service service = Service.parse(image);

			historyTaskMap.get(service).add(task.getId());
			currentTaskMap.get(service).add(task.getId());
		}
		
		Collection<String> taskImages = tasks.stream().map(task -> task.getSpec().getContainerSpec().getImage()).collect(Collectors.toList());
		fimageStatusListeners.removeIf(filter -> filter.apply(taskImages));

		SwarmTaskLogRecord record = new SwarmTaskLogRecord(currentTaskMap, historyTaskMap);
		try {
			outputStream.write(record.toString().getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private Map<Service, Set<String>> createTaskMap() {
		return Arrays.stream(Service.values()).filter(service -> !service.isTest()).collect(Collectors.toMap(service -> service, service -> new HashSet<String>()));
	}

	public void addImageStatusListener(Function<Collection<String>, Boolean> listener) {
		fimageStatusListeners.add(listener);
	}
}
