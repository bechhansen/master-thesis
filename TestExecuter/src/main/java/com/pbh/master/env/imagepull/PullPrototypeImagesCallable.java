package com.pbh.master.env.imagepull;

import java.util.concurrent.Callable;

import com.amazonaws.services.ec2.model.Instance;
import com.github.dockerjava.api.DockerClient;
import com.pbh.master.env.Factory;
import com.pbh.master.env.Service;
import com.pbh.master.env.log.ProgressLogger;

public class PullPrototypeImagesCallable implements Callable<Void> {

	private Instance server;
	private Factory factory;
	private ProgressLogger logger;

	public PullPrototypeImagesCallable(Factory factory, Instance server) {
		this.factory = factory;
		this.server = server;
		logger = factory.getProgressLogger();
	}

	@Override
	public Void call() throws Exception {	
		logger.info("Starting to pull images for " + server.getPublicDnsName());	
		try {
			
			DockerClient dockerClient = factory.getClient(server);
			
			pullImage(dockerClient, Service.AUTH, "1");
			pullImage(dockerClient, Service.AUTH, "2");
			
			pullImage(dockerClient, Service.SERVICE1, "1");
			pullImage(dockerClient, Service.SERVICE1, "2");
			
			pullImage(dockerClient, Service.SERVICE2, "1");
			pullImage(dockerClient, Service.SERVICE2, "2");
			
			pullImage(dockerClient, Service.SERVICE3, "1");
			pullImage(dockerClient, Service.SERVICE3, "2");
			
			//pullImage(dockerClient, "portainer/portainer", "latest");
			//pullImage(dockerClient, "portainer/agent", "latest");
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	private void pullImage(DockerClient dockerClient, Service service, String tag) throws InterruptedException {
		dockerClient.pullImageCmd(service.getImage()).withTag(tag).exec(new MasterPullImageResultCallback(factory, service, tag, server)).awaitCompletion();	
	}
}
