package com.pbh.master.env.churn;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.function.Consumer;

import com.amazonaws.services.ec2.model.Instance;
import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.model.Container;
import com.pbh.master.env.Factory;
import com.pbh.master.env.Service;
import com.pbh.master.env.log.ProgressLogger;
import com.pbh.master.env.log.TaskLogger;

public class KillChurnRunable implements Runnable {

	private List<Instance> swarmInstances;
	private Factory factory;
	private TaskLogger taskLogger;
	private Service service;
	private Consumer<Service> endListener;
	private ProgressLogger logger;
	
	Random rn = new Random();

	public KillChurnRunable(List<Instance> swarmInstances, Factory factory, TaskLogger taskLogger, Service service, Consumer<Service> endListener) {
		this.swarmInstances = swarmInstances;
		this.factory = factory;
		this.taskLogger = taskLogger;
		this.service = service;
		this.endListener = endListener;
		logger = factory.getProgressLogger();
	}

	@Override
	public void run() {
		
		boolean foundImage = false;
		
		while (!foundImage) {
			Instance randomInstance = swarmInstances.get(rn.nextInt(swarmInstances.size()));
			DockerClient dockerClient = factory.getClient(randomInstance);
			
			List<Container> containers = dockerClient.listContainersCmd().withAncestorFilter(Collections.singleton(service.getImage() + ":1")).exec();
			if (!containers.isEmpty()) {
				Container container = containers.get(rn.nextInt(containers.size()));
				logger.info("Killing " + service + " + on " + randomInstance.getPublicDnsName());
				dockerClient.killContainerCmd(container.getId()).exec();
				foundImage = true;
			}
		}

		taskLogger.addImageStatusListener(status -> isAllImagesAtQuantity(status, service, swarmInstances.size()));
	}

	private boolean isAllImagesAtQuantity(Collection<String> imagesInService, Service service, int count) {	
		long deployedImages = imagesInService.stream().filter(image -> image.startsWith(service.getImage())).count();
		if (deployedImages >= count) {
			logger.info(service + " has been recreated " + deployedImages + "/" + count);
			endListener.accept(service);
			return true;
		}
		return false;
	}

}
