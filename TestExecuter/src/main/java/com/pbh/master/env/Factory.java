package com.pbh.master.env;

import java.util.List;
import java.util.Optional;

import com.amazonaws.services.ec2.model.Instance;
import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.DockerCmdExecFactory;
import com.github.dockerjava.core.DefaultDockerClientConfig;
import com.github.dockerjava.core.DockerClientBuilder;
import com.github.dockerjava.jaxrs.JerseyDockerCmdExecFactory;
import com.pbh.master.env.churn.ChurnGenerator;
import com.pbh.master.env.churn.GracefulChurnGenerator;
import com.pbh.master.env.churn.NOOPChurnGenerator;
import com.pbh.master.env.churn.UngracefulChurnGenerator;
import com.pbh.master.env.churn.UngracefullChurnExternalGenerator;
import com.pbh.master.env.churn.UngracefullChurnInternalGenerator;
import com.pbh.master.env.log.NOOPTaskLogger;
import com.pbh.master.env.log.ProgressLogger;
import com.pbh.master.env.log.ProgressLoggerImpl;
import com.pbh.master.env.log.SwarmTaskLogger;
import com.pbh.master.env.log.TaskLogger;

public class Factory {

	private ProgressLogger progressLogger;
	private ConfigurationProvider configurationProvider;

	public DockerClient getClient(Instance instance) {
		DefaultDockerClientConfig config = DefaultDockerClientConfig.createDefaultConfigBuilder().withDockerCertPath("/Users/pbh/Projects/Master/Hovedopgave/Project/AWS/cert").withDockerTlsVerify("1")
				.withDockerHost("tcp://" + instance.getPublicDnsName() + ":2376").build();
		
		DockerCmdExecFactory dockerCmdExecFactory = new JerseyDockerCmdExecFactory()
				  //.withReadTimeout(1000)
				  //.withConnectTimeout(1000)
				  .withMaxTotalConnections(1000)
				  .withMaxPerRouteConnections(1000);
		
		return DockerClientBuilder.getInstance(config).withDockerCmdExecFactory(dockerCmdExecFactory).build();
	}

	public ChurnGenerator createChurnGenerator(ConfigurationProvider conf, List<Instance> swarmInstances, Optional<ServiceDepoyerResponse> serviceDepoyerResponse, TaskLogger taskLogger) {
		if (conf.isSwarm() && conf.enableChurn() && conf.getChurnType().equals("GRACEFUL") && serviceDepoyerResponse.isPresent()) {
			return new GracefulChurnGenerator(this, swarmInstances, serviceDepoyerResponse.get(), taskLogger);
		}
		
		if (conf.isSwarm() && conf.enableChurn() && conf.getChurnType().equals("UNGRACEFUL") && serviceDepoyerResponse.isPresent()) {
			return new UngracefulChurnGenerator(this, swarmInstances, serviceDepoyerResponse.get(), taskLogger);
		}
		
		if (conf.isSwarm() && conf.enableChurn() && conf.getChurnType().equals("UNGRACEFUL_INTERNAL") && serviceDepoyerResponse.isPresent()) {
			return new UngracefullChurnInternalGenerator(this, swarmInstances, serviceDepoyerResponse.get(), taskLogger);
		}
		
		if (conf.isSwarm() && conf.enableChurn() && conf.getChurnType().equals("UNGRACEFUL_EXTERNAL") && serviceDepoyerResponse.isPresent()) {
			return new UngracefullChurnExternalGenerator(this, swarmInstances, serviceDepoyerResponse.get(), taskLogger);
		}

		return new NOOPChurnGenerator();
	}

	public TaskLogger createLogger(ConfigurationProvider conf, List<Instance> swarmInstances) {
		if (conf.isSwarm()) {
			return new SwarmTaskLogger(this, conf, swarmInstances);
		}

		return new NOOPTaskLogger();
	}

	public PrototypeDeployer createPrototypeDeployer(ConfigurationProvider conf, List<Instance> swarmInstances) {
		if (conf.isSwarm()) {
			return new PrototypeSwarmDeployer(conf, this, swarmInstances);
		} else {
			return new PrototypeStandaloneDeployer(this, swarmInstances);
		}
	}

	public TestDeployer createTestDeployer(ConfigurationProvider conf, List<Instance> testInstances) {
		if (conf.isManualTest()) {
			return new ManualTestDeployer(this);
		} else {
			return new DockerTestDeployer(conf, this, testInstances);
		}
	}

	public ProgressLogger getProgressLogger() {
		if(progressLogger == null) {
			progressLogger = new ProgressLoggerImpl(configurationProvider);
		}
		return progressLogger;
	}

	public void setConfigurationProvider(ConfigurationProvider configurationProvider) {
		this.configurationProvider = configurationProvider;
	}
}
