package com.pbh.master.env.log;

import java.util.Collection;
import java.util.function.Function;

public interface TaskLogger {

	void startLogging();

	void endLogging();

	public void addImageStatusListener(Function<Collection<String>, Boolean> listener);

}
