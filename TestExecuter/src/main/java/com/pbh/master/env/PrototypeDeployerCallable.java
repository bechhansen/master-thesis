package com.pbh.master.env;

import java.util.concurrent.Callable;

import com.amazonaws.services.ec2.model.Instance;
import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.CreateContainerResponse;
import com.github.dockerjava.api.model.ExposedPort;
import com.github.dockerjava.api.model.Ports;
import com.github.dockerjava.api.model.Ports.Binding;

public class PrototypeDeployerCallable implements Callable<Void> {
	
	private Instance instance;
	private Factory factory;

	public PrototypeDeployerCallable(Factory factory, Instance instance) {
		this.factory = factory;
		this.instance = instance;
	}

	@Override
	public Void call() throws Exception {
	
		DockerClient dockerClient = factory.getClient(instance);
		
		dockerClient.createNetworkCmd()
		.withName("Master")
		.exec();
		
		createImage(dockerClient, instance, Service.AUTH, "1", 8080);	
		createImage(dockerClient, instance, Service.SERVICE2, "1", 8082);
		createImage(dockerClient, instance, Service.SERVICE3, "1", 8083);
		createImage(dockerClient, instance, Service.SERVICE1, "1", 8081);
		
		return null;
	}

	@SuppressWarnings("deprecation")
	private String createImage(DockerClient dockerClient, Instance instance, Service service, String tag, Integer port) {
		
		ExposedPort ePort = ExposedPort.tcp(8080);
		Ports portBindings = new Ports();
		portBindings.bind(ePort, Binding.bindPort(port));
		
		CreateContainerResponse container = dockerClient.createContainerCmd(service.getImage() + ":" + tag)
		.withName(service.name().toLowerCase())
		.withEnv("DOCKERHOST=" + instance.getInstanceId())
		//.withPortSpecs(port + ":8080")
		.withExposedPorts(ePort)
		.withPortBindings(portBindings)
		.withNetworkMode("Master")
		.exec();
		
		dockerClient.startContainerCmd(container.getId()).exec();
		return container.getId();
	}
}
