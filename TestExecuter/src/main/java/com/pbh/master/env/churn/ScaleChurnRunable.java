package com.pbh.master.env.churn;

import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;

import com.amazonaws.services.ec2.model.Instance;
import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.model.ServiceSpec;
import com.pbh.master.env.Factory;
import com.pbh.master.env.Service;
import com.pbh.master.env.ServiceDepoyerResponse;
import com.pbh.master.env.log.ProgressLogger;
import com.pbh.master.env.log.TaskLogger;

public class ScaleChurnRunable implements Runnable {

	private List<Instance> swarmInstances;
	private Factory factory;
	private TaskLogger taskLogger;
	private Service service;
	private ServiceDepoyerResponse services;
	private Consumer<Service> endListener;
	private ProgressLogger logger;
	private int newReplicas;

	public ScaleChurnRunable(List<Instance> swarmInstances, Factory factory, TaskLogger taskLogger, Service service, ServiceDepoyerResponse services, Consumer<Service> endListener, int newReplicas) {
		this.swarmInstances = swarmInstances;
		this.factory = factory;
		this.taskLogger = taskLogger;
		this.service = service;
		this.services = services;
		this.endListener = endListener;
		this.newReplicas = newReplicas;
		logger = factory.getProgressLogger();
	}

	@Override
	public void run() {
		DockerClient dockerClient = factory.getClient(swarmInstances.get(0));

		String serviceId = services.getServiceId(service);
		com.github.dockerjava.api.model.Service dockerService = dockerClient.inspectServiceCmd(serviceId).exec();
		ServiceSpec spec = dockerService.getSpec();
		spec.getMode().getReplicated().withReplicas(newReplicas);

		logger.info("Scale " + service + " to " + newReplicas);
		dockerClient.updateServiceCmd(serviceId, spec).withVersion(dockerService.getVersion().getIndex()).exec();

		taskLogger.addImageStatusListener(status -> isAllImagesAtQuantity(status, service, newReplicas));
	}

	private boolean isAllImagesAtQuantity(Collection<String> imagesInService, Service service, int count) {	
		long deployedImages = imagesInService.stream().filter(image -> image.startsWith(service.getImage())).count();
		if (deployedImages == count) {
			logger.info(service + " scaled");
			endListener.accept(service);
			return true;
		}
		return false;
	}
}
