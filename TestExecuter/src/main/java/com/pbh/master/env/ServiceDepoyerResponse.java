package com.pbh.master.env;

public class ServiceDepoyerResponse {

	private String authId;
	private String service1Id;
	private String service2Id;
	private String service3Id;

	public ServiceDepoyerResponse(String authId, String service1Id, String service2Id, String service3Id) {
		this.authId = authId;
		this.service1Id = service1Id;
		this.service2Id = service2Id;
		this.service3Id = service3Id;
	}

	public String getAuthId() {
		return authId;
	}

	public String getService1Id() {
		return service1Id;
	}

	public String getService2Id() {
		return service2Id;
	}

	public String getService3Id() {
		return service3Id;
	}

	public String getServiceId(Service service) {
		switch (service) {
		case SERVICE1:
			return getService1Id();
		case SERVICE2:
			return getService2Id();
		case SERVICE3:
			return getService3Id();
		case AUTH:
			return getAuthId();

		default:
			return null;
		}
	}

}
