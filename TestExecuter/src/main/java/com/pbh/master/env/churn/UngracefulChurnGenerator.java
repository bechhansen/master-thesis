package com.pbh.master.env.churn;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.amazonaws.services.ec2.model.Instance;
import com.pbh.master.env.Factory;
import com.pbh.master.env.Service;
import com.pbh.master.env.ServiceDepoyerResponse;
import com.pbh.master.env.log.TaskLogger;

public class UngracefulChurnGenerator implements ChurnGenerator {

	private List<Instance> swarmInstances;
	private Factory factory;
	private TaskLogger taskLogger;
	private ScheduledExecutorService scheduler;

	public UngracefulChurnGenerator(Factory factory, List<Instance> swarmInstances, ServiceDepoyerResponse services, TaskLogger taskLogger) {
		this.factory = factory;
		this.swarmInstances = swarmInstances;
		this.taskLogger = taskLogger;
		
		factory.getProgressLogger().info("Start UngracefulChurnGenerator churn");
		scheduler = Executors.newScheduledThreadPool(4);
	}
	
	public void stopChurn() {
		scheduler.shutdown();
	}

	public void generateChurn() {
		scheduleKill(Service.SERVICE1, 30);
		scheduleKill(Service.SERVICE2, 35);
		scheduleKill(Service.SERVICE3, 40);
		scheduleKill(Service.AUTH, 45);
	}

	private void scheduleKill(Service service, int seconds) {
		KillChurnRunable killer = new KillChurnRunable(
				swarmInstances, 
				factory, 
				taskLogger, 
				service, 
				s -> scheduleKill(s, 25));
		
		scheduler.schedule(killer, seconds, TimeUnit.SECONDS);
	}
}
