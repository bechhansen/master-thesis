package com.pbh.master.env;

public interface TestDeployer {

	void deploy(String loadBalancerDNSName);

	void getTestResult();

}
