package com.pbh.master.env.log;

import java.util.ArrayList;
import java.util.List;

import com.amazonaws.services.ec2.model.Instance;
import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.ListContainersCmd;
import com.github.dockerjava.api.command.LogContainerCmd;
import com.github.dockerjava.api.exception.NotFoundException;
import com.github.dockerjava.api.model.Container;
import com.pbh.master.env.ConfigurationProvider;
import com.pbh.master.env.Factory;

public class DockerLogRetriever {

	private List<Instance> instances;
	private Factory factory;
	private ConfigurationProvider conf;
	private ProgressLogger logger;

	public DockerLogRetriever(ConfigurationProvider conf, Factory factory, List<Instance> swarmInstances, List<Instance> testInstances) {
		this.conf = conf;
		this.factory = factory;
		logger = factory.getProgressLogger();

		List<Instance> instances = new ArrayList<>();
		instances.addAll(testInstances);
		instances.addAll(swarmInstances);
		this.instances = instances;
	}

	public void getLogs() throws InterruptedException {

		for (Instance instance : instances) {

			DockerClient dockerClient = factory.getClient(instance);

			ListContainersCmd listContainersCmd = dockerClient.listContainersCmd();
			List<Container> containers = listContainersCmd.withShowAll(true).exec();

			for (Container container : containers) {
				try {
					LogContainerCmd logContainerCmd = dockerClient.logContainerCmd(container.getId()).withStdOut(true).withStdErr(true).withTimestamps(false);
					LogContainerCallback exec = logContainerCmd.exec(new LogContainerCallback(factory, conf, container));
					exec.awaitCompletion();
				} catch (NotFoundException e) {
					logger.info("Container with id " + container.getId() + " not found");
				}
			}
		}
	}
}
