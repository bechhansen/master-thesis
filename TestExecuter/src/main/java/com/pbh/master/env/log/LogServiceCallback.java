package com.pbh.master.env.log;

import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import com.github.dockerjava.api.model.Frame;
import com.github.dockerjava.core.command.LogContainerResultCallback;
import com.pbh.master.env.ConfigurationProvider;
import com.pbh.master.env.Factory;
import com.pbh.master.env.Service;

public class LogServiceCallback extends LogContainerResultCallback {

	private ConfigurationProvider conf;
	private FileOutputStream outputStream;
	private ProgressLogger logger;
	private Service service;

	public LogServiceCallback(Factory factory, ConfigurationProvider conf, Service service) {
		this.conf = conf;
		this.service = service;
		logger = factory.getProgressLogger();
	}

	@Override
	public void onNext(Frame item) {
		try {
			outputStream.write(item.getPayload());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onComplete() {
		
		try {
			outputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		super.onComplete();
	}

	@Override
	public void onStart(Closeable stream) {
		try {
			
			String fileName = service + ".log";
			
			File file = new File(conf.getOutputFolder(), fileName);
			file.createNewFile();
			outputStream = new FileOutputStream(file);
			
			logger.info("Creating service log: " + file.getName());
			
			super.onStart(stream);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
