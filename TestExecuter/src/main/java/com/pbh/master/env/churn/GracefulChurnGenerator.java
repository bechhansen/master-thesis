package com.pbh.master.env.churn;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.amazonaws.services.ec2.model.Instance;
import com.pbh.master.env.Factory;
import com.pbh.master.env.Service;
import com.pbh.master.env.ServiceDepoyerResponse;
import com.pbh.master.env.log.TaskLogger;

public class GracefulChurnGenerator implements ChurnGenerator {

	private List<Instance> swarmInstances;
	private Factory factory;
	private ServiceDepoyerResponse services;
	private TaskLogger taskLogger;
	private ScheduledExecutorService scheduler;

	public GracefulChurnGenerator(Factory factory, List<Instance> swarmInstances, ServiceDepoyerResponse services, TaskLogger taskLogger) {
		this.factory = factory;
		this.swarmInstances = swarmInstances;
		this.services = services;
		this.taskLogger = taskLogger;
		
		factory.getProgressLogger().info("Start GracefulChurnGenerator");
		scheduler = Executors.newScheduledThreadPool(10);
	}
	
	public void stopChurn() {
		scheduler.shutdown();
	}

	public void generateChurn() {
		scheduleRollingUpdate(Service.SERVICE1, 30);
		scheduleScaling(Service.SERVICE2, 35, true);
		scheduleRollingUpdate(Service.SERVICE3, 40);
		scheduleScaling(Service.AUTH, 45, true);
	}

	private void scheduleRollingUpdate(Service service, int seconds) {
		RollingUpdateChurnRunable rollingUpdater = new RollingUpdateChurnRunable(
				swarmInstances, factory, taskLogger, service, services, 
				s -> scheduleScaling(s, 5, true));
		
		scheduler.schedule(rollingUpdater, seconds, TimeUnit.SECONDS);
	}
	
	private void scheduleScaling(Service service, int seconds, boolean scaleUp) {
		ScaleChurnRunable scaler = new ScaleChurnRunable(
				swarmInstances, factory, taskLogger, service, services, 
				scaleUp ? s -> scheduleScaling(s, 30, false) : s -> scheduleRollingUpdate(s, 5), 
				scaleUp ? 11 : 10);
		
		scheduler.schedule(scaler, seconds, TimeUnit.SECONDS);
	}
}
