package com.pbh.master.env;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider;
import com.amazonaws.services.ec2.model.Instance;
import com.pbh.master.env.aws.ApplicationLoadbalancerCreator;
import com.pbh.master.env.aws.EC2Creator;
import com.pbh.master.env.aws.LoadbalancerCreator;
import com.pbh.master.env.churn.ChurnGenerator;
import com.pbh.master.env.imagepull.DockerPuller;
import com.pbh.master.env.log.DockerLogRetriever;
import com.pbh.master.env.log.TaskLogger;

public class TestExecutor {

	public static void main(String[] args) {

		int iterations = ConfigurationProvider.getIterations();
		for (int i = 0; i < iterations; i++) {
			runTest();	
		}
	}

	private static void runTest() {
		AWSCredentialsProvider credentialsProvider = new EnvironmentVariableCredentialsProvider();
		
		Factory factory = new Factory();
		ConfigurationProvider conf = new ConfigurationProvider(factory);
		
		conf.print();
		
		int swarmNodeCount = conf.getSwarmNodeCount();
		int testNodeCount = conf.getTestNodeCount();
		
		try {
			EC2Creator ec2Creator = new EC2Creator(factory, credentialsProvider);
			List<Instance> swarmInstances = ec2Creator.createSwarmInstances(swarmNodeCount);
			List<Instance> testInstances = conf.isManualTest() ? Collections.emptyList() : ec2Creator.createJMeterInstances(testNodeCount);
			
			LoadbalancerCreator lbCreator = new ApplicationLoadbalancerCreator(factory, credentialsProvider);
			String loadBalancerDNSName = lbCreator.createLoadbalancer(swarmInstances);
			
			DockerReadyListener ready = new DockerReadyListener(factory);
			ready.waitForDockerReady(swarmInstances);
			
			SwarmCreator swarmCreator = new SwarmCreator(factory, swarmInstances);
			swarmCreator.createSwarm();
			
			DockerPuller puller = new DockerPuller(factory);
			puller.pullPrototypeContainers(swarmInstances);
			
			TaskLogger taskLogger = factory.createLogger(conf, swarmInstances);
			taskLogger.startLogging();
			
			PrototypeDeployer deployer = factory.createPrototypeDeployer(conf, swarmInstances);
			Optional<ServiceDepoyerResponse> services = deployer.deploy();
			
			lbCreator.waitUntilLoadBalancerReady();
				
			ready.waitForDockerReady(testInstances);	
			puller.pullTestContainers(testInstances);
						
			ChurnGenerator churnGenerator = factory.createChurnGenerator(conf, swarmInstances, services, taskLogger);
			churnGenerator.generateChurn();
				
			TestDeployer testDeployer = factory.createTestDeployer(conf, testInstances);
			
			if (conf.disableLoadBalancer()) {
				testDeployer.deploy(swarmInstances.get(swarmInstances.size() - 1).getPublicDnsName());
			} else {
				testDeployer.deploy(loadBalancerDNSName);
			}
			
			testDeployer.getTestResult();
			
			DockerLogRetriever retriever = new DockerLogRetriever(conf, factory, swarmInstances, testInstances);
			retriever.getLogs();
			
			taskLogger.endLogging();
			churnGenerator.stopChurn();
			
			lbCreator.removeLoadBalancer();
			ec2Creator.removeInstances();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}
}
