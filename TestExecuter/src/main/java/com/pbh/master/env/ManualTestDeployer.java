package com.pbh.master.env;

import java.io.IOException;

import com.pbh.master.env.log.ProgressLogger;

public class ManualTestDeployer implements TestDeployer {

	private ProgressLogger logger;

	public ManualTestDeployer(Factory factory) {
		logger = factory.getProgressLogger();
	}
	
	@Override
	public void deploy(String loadBalancerDNSName) {
		logger.info("Environment is ready for manual test agains " + loadBalancerDNSName);
		try {
			System.in.read();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void getTestResult() {
		
	}
}
