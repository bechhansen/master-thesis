package com.pbh.master.env.imagepull;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import com.amazonaws.services.ec2.model.Instance;
import com.pbh.master.env.Factory;
import com.pbh.master.env.log.ProgressLogger;

public class DockerPuller {

	private Factory factory;
	private ProgressLogger logger;

	public DockerPuller(Factory factory) {
		this.factory = factory;
		logger = factory.getProgressLogger();
	}

	public void pullPrototypeContainers(List<Instance> instances) {
		if (instances != null && !instances.isEmpty()) {

			try {
				logger.info("Starting pulling prototype images for all servers");

				ExecutorService executor = Executors.newFixedThreadPool(instances.size());
				List<PullPrototypeImagesCallable> collect = instances.stream().map(instance -> new PullPrototypeImagesCallable(factory, instance)).collect(Collectors.toList());
				executor.invokeAll(collect);
				executor.shutdown();

				logger.info("Finished pulling prototype images for all servers");
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public void pullTestContainers(List<Instance> instances) {
		if (instances != null && !instances.isEmpty()) {

			try {
				logger.info("Starting pulling test images for all servers");

				ExecutorService executor = Executors.newFixedThreadPool(instances.size());
				List<PullTestImagesCallable> collect = instances.stream().map(instance -> new PullTestImagesCallable(factory, instance)).collect(Collectors.toList());
				executor.invokeAll(collect);
				executor.shutdown();

				logger.info("Finished pulling test images for all servers");
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
