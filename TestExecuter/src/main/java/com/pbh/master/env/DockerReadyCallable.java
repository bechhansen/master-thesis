package com.pbh.master.env;

import java.util.concurrent.Callable;

import com.amazonaws.services.ec2.model.Instance;
import com.github.dockerjava.api.DockerClient;

public class DockerReadyCallable implements Callable<Void> {

	private Instance server;
	private Factory factory;

	public DockerReadyCallable(Factory factory, Instance server) {
		this.factory = factory;
		this.server = server;
	}

	@Override
	public Void call() throws Exception {

		DockerClient dockerClient = factory.getClient(server);

		while (true) {
			try {
				
				dockerClient.pingCmd().exec();
				break;
			} catch (Exception e) {
				Thread.sleep(1000);
			}
		}

		return null;
	}
}
