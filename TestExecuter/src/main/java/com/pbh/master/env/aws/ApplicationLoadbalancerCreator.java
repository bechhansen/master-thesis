package com.pbh.master.env.aws;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.elasticloadbalancingv2.AmazonElasticLoadBalancing;
import com.amazonaws.services.elasticloadbalancingv2.AmazonElasticLoadBalancingClientBuilder;
import com.amazonaws.services.elasticloadbalancingv2.model.Action;
import com.amazonaws.services.elasticloadbalancingv2.model.ActionTypeEnum;
import com.amazonaws.services.elasticloadbalancingv2.model.CreateListenerRequest;
import com.amazonaws.services.elasticloadbalancingv2.model.CreateLoadBalancerRequest;
import com.amazonaws.services.elasticloadbalancingv2.model.CreateLoadBalancerResult;
import com.amazonaws.services.elasticloadbalancingv2.model.DeleteLoadBalancerRequest;
import com.amazonaws.services.elasticloadbalancingv2.model.DescribeTargetHealthRequest;
import com.amazonaws.services.elasticloadbalancingv2.model.DescribeTargetHealthResult;
import com.amazonaws.services.elasticloadbalancingv2.model.LoadBalancer;
import com.amazonaws.services.elasticloadbalancingv2.model.LoadBalancerAttribute;
import com.amazonaws.services.elasticloadbalancingv2.model.LoadBalancerTypeEnum;
import com.amazonaws.services.elasticloadbalancingv2.model.ModifyLoadBalancerAttributesRequest;
import com.amazonaws.services.elasticloadbalancingv2.model.ProtocolEnum;
import com.amazonaws.services.elasticloadbalancingv2.model.RegisterTargetsRequest;
import com.amazonaws.services.elasticloadbalancingv2.model.TargetDescription;
import com.amazonaws.services.elasticloadbalancingv2.model.TargetHealthStateEnum;
import com.pbh.master.env.Factory;
import com.pbh.master.env.log.ProgressLogger;

public class ApplicationLoadbalancerCreator implements LoadbalancerCreator {

	private static final String LOADBALANCER_NAME = "Master";
	private AWSCredentialsProvider credentialsProvider;
	private LoadBalancer loadBalancer;
	private ProgressLogger logger;

	public ApplicationLoadbalancerCreator(Factory factory, AWSCredentialsProvider credentialsProvider) {
		this.credentialsProvider = credentialsProvider;
		logger = factory.getProgressLogger();
	}

	public String createLoadbalancer(List<Instance> instances) {

		if (instances.isEmpty()) {
			return "localhost";
		}

		AmazonElasticLoadBalancing loadbalancerClient = createLoadbalancerClient();
		String loadBalancerName = createLoadbalancer(loadbalancerClient);
		registerInstancesInLoadbalancer(loadbalancerClient, instances);
		return loadBalancerName;
	}

	private String createLoadbalancer(AmazonElasticLoadBalancing loadbalancerClient) {
		logger.info("Create loadbalancer");
		CreateLoadBalancerRequest request = new CreateLoadBalancerRequest();
		request.setName(LOADBALANCER_NAME);

		request.setType(LoadBalancerTypeEnum.Application);

		Collection<String> securityGroups = new ArrayList<>();
		securityGroups.add("sg-0a3795da85c6e34b9");
		request.setSecurityGroups(securityGroups);
		// request.setAvailabilityZones(Collections.singleton("eu-west-1a"));

		Collection<String> subNets = new ArrayList<>();

		
		subNets.add("subnet-054cef61ed50a8c84"); //b
		subNets.add("subnet-04cdeeec704101edc"); //c
		//subNets.add("subnet-05f5e12a71efa3259"); //a
		
		request.setSubnets(subNets);

		CreateLoadBalancerResult createLoadBalancer = loadbalancerClient.createLoadBalancer(request);
		loadBalancer = createLoadBalancer.getLoadBalancers().get(0);

		ModifyLoadBalancerAttributesRequest modifyLoadBalancerAttributesRequest = new ModifyLoadBalancerAttributesRequest();
		modifyLoadBalancerAttributesRequest.setLoadBalancerArn(loadBalancer.getLoadBalancerArn());

		Collection<LoadBalancerAttribute> attributes = new ArrayList<>();
		//attributes.add(new LoadBalancerAttribute().withKey("access_logs.s3.enabled").withValue("true"));
		//attributes.add(new LoadBalancerAttribute().withKey("access_logs.s3.bucket").withValue("master-lb-log"));

		attributes.add(new LoadBalancerAttribute().withKey("idle_timeout.timeout_seconds").withValue("600"));
		attributes.add(new LoadBalancerAttribute().withKey("routing.http2.enabled").withValue("false"));
		
		modifyLoadBalancerAttributesRequest.setAttributes(attributes);
		loadbalancerClient.modifyLoadBalancerAttributes(modifyLoadBalancerAttributesRequest);

		String dnsName = loadBalancer.getDNSName();
		logger.info("Created loadbalancer: " + dnsName);

		Action authAction = new Action();
		authAction.setType(ActionTypeEnum.Forward);
		authAction.setTargetGroupArn("arn:aws:elasticloadbalancing:eu-west-1:607415850830:targetgroup/auth/c3fd113c5ce61503");

		CreateListenerRequest createAuthListenerRequest = new CreateListenerRequest();
		createAuthListenerRequest.setPort(8080);
		createAuthListenerRequest.setProtocol(ProtocolEnum.HTTP);
		createAuthListenerRequest.setLoadBalancerArn(loadBalancer.getLoadBalancerArn());
		createAuthListenerRequest.setDefaultActions(Collections.singleton(authAction));
		loadbalancerClient.createListener(createAuthListenerRequest);

		Action service1Action = new Action();
		service1Action.setType(ActionTypeEnum.Forward);
		service1Action.setTargetGroupArn("arn:aws:elasticloadbalancing:eu-west-1:607415850830:targetgroup/Service1/69c0a6db7a972099");

		CreateListenerRequest createService1ListenerRequest = new CreateListenerRequest();
		createService1ListenerRequest.setPort(8081);
		createService1ListenerRequest.setProtocol(ProtocolEnum.HTTP);
		createService1ListenerRequest.setLoadBalancerArn(loadBalancer.getLoadBalancerArn());
		createService1ListenerRequest.setDefaultActions(Collections.singleton(service1Action));
		loadbalancerClient.createListener(createService1ListenerRequest);

		// Collection<Listener> listeners = Stream.of(authListener,
		// service1Listener).collect(Collectors.toSet());
		// request.setListeners(listeners);
		/*
		 * ConfigureHealthCheckRequest configureHealthCheckRequest = new
		 * ConfigureHealthCheckRequest();
		 * configureHealthCheckRequest.setLoadBalancerName(LOADBALANCER_NAME); HealthCheck
		 * healthCheck = new HealthCheck(); healthCheck.setHealthyThreshold(2);
		 * healthCheck.setTimeout(5); healthCheck.setUnhealthyThreshold(10);
		 * healthCheck.setInterval(10); healthCheck.setTarget("HTTP:8081/healthcheck");
		 * 
		 * configureHealthCheckRequest.setHealthCheck(healthCheck);
		 * loadbalancerClient.configureHealthCheck(configureHealthCheckRequest);
		 * logger.info("Configured loadbalancer healthcheck");
		 */

		return dnsName;
	}

	private void registerInstancesInLoadbalancer(AmazonElasticLoadBalancing loadbalancerClient, List<Instance> runningInstances) {
		logger.info("Adding instances to loadbalancer");

		RegisterTargetsRequest authRegisterTargetsRequest = new RegisterTargetsRequest();

		List<TargetDescription> collect = runningInstances.stream().map(inst -> new TargetDescription().withId(inst.getInstanceId())).collect(Collectors.toList());

		authRegisterTargetsRequest.setTargets(collect);
		authRegisterTargetsRequest.setTargetGroupArn("arn:aws:elasticloadbalancing:eu-west-1:607415850830:targetgroup/auth/c3fd113c5ce61503");
		loadbalancerClient.registerTargets(authRegisterTargetsRequest);

		authRegisterTargetsRequest.setTargets(collect);
		authRegisterTargetsRequest.setTargetGroupArn("arn:aws:elasticloadbalancing:eu-west-1:607415850830:targetgroup/Service1/69c0a6db7a972099");
		loadbalancerClient.registerTargets(authRegisterTargetsRequest);

		logger.info("Added " + runningInstances.size() + " instances to loadbalancer");

	}

	private AmazonElasticLoadBalancing createLoadbalancerClient() {
		return AmazonElasticLoadBalancingClientBuilder.standard().withCredentials(credentialsProvider).withRegion(Regions.EU_WEST_1).build();

	}

	public void waitUntilLoadBalancerReady() {

		AmazonElasticLoadBalancing client = createLoadbalancerClient();
		DescribeTargetHealthRequest authTargetHealthRequest = new DescribeTargetHealthRequest();
		authTargetHealthRequest.withTargetGroupArn("arn:aws:elasticloadbalancing:eu-west-1:607415850830:targetgroup/auth/c3fd113c5ce61503");

		DescribeTargetHealthRequest service1TargetHealthRequest = new DescribeTargetHealthRequest();
		service1TargetHealthRequest.withTargetGroupArn("arn:aws:elasticloadbalancing:eu-west-1:607415850830:targetgroup/Service1/69c0a6db7a972099");

		logger.info("Wait until all instances are Healthy");

		waitForHealthyTargets(client, authTargetHealthRequest);
		waitForHealthyTargets(client, service1TargetHealthRequest);

		logger.info("All instances are InService for loadbalancer");
	}

	private void waitForHealthyTargets(AmazonElasticLoadBalancing client, DescribeTargetHealthRequest request) {
		while (true) {

			DescribeTargetHealthResult authHealth = client.describeTargetHealth(request);

			List<TargetDescription> collect = authHealth.getTargetHealthDescriptions().stream()
					.filter(h -> !TargetHealthStateEnum.fromValue(h.getTargetHealth().getState()).equals(TargetHealthStateEnum.Healthy)).map(h -> h.getTarget()).collect(Collectors.toList());

			if (collect.isEmpty()) {
				break;
			} else {
				try {
					request.setTargets(collect);
					Thread.sleep(1000);
					//System.out.print(".");
				} catch (InterruptedException e) {
				}
			}
		}
	}

	public void removeLoadBalancer() {
		if (loadBalancer != null) {
			AmazonElasticLoadBalancing client = createLoadbalancerClient();
			DeleteLoadBalancerRequest deleteLoadBalancerRequest = new DeleteLoadBalancerRequest();
			deleteLoadBalancerRequest.withLoadBalancerArn(loadBalancer.getLoadBalancerArn());
			client.deleteLoadBalancer(deleteLoadBalancerRequest);
		}
	}
}
