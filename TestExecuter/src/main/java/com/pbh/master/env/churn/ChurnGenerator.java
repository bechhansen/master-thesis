package com.pbh.master.env.churn;

public interface ChurnGenerator {

	void stopChurn();

	void generateChurn();

}
