package com.pbh.master.env.churn;

import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;

import com.amazonaws.services.ec2.model.Instance;
import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.model.ServiceSpec;
import com.pbh.master.env.Factory;
import com.pbh.master.env.Service;
import com.pbh.master.env.ServiceDepoyerResponse;
import com.pbh.master.env.log.ProgressLogger;
import com.pbh.master.env.log.TaskLogger;

public class RollingUpdateChurnRunable implements Runnable {

	private Factory factory;
	private TaskLogger taskLogger;
	private final Service service;
	private ServiceDepoyerResponse services;
	private List<Instance> swarmInstances;
	private Consumer<Service> endListener;
	private ProgressLogger logger;

	public RollingUpdateChurnRunable(List<Instance> swarmInstances, Factory factory, TaskLogger taskLogger, Service service, ServiceDepoyerResponse services, Consumer<Service> endListener) {
		this.swarmInstances = swarmInstances;
		this.factory = factory;
		this.taskLogger = taskLogger;
		this.service = service;
		this.services = services;
		this.endListener = endListener;
		logger = factory.getProgressLogger();
	}

	@Override
	public void run() {
		DockerClient dockerClient = factory.getClient(swarmInstances.get(0));

		String serviceId = services.getServiceId(service);

		com.github.dockerjava.api.model.Service dockerService = dockerClient.inspectServiceCmd(serviceId).exec();
		ServiceSpec spec = dockerService.getSpec();

		String image = spec.getTaskTemplate().getContainerSpec().getImage();
		String newImage = changeVersion(image);
		spec.getTaskTemplate().getContainerSpec().withImage(newImage);

		logger.info("Rollingupdate " + service + " to " + newImage + " - Order: " + service.getUpdateOrder() + ". Parallelism: " + service.getUpdateParallelism());
		dockerClient.updateServiceCmd(serviceId, spec).withVersion(dockerService.getVersion().getIndex()).exec();

		taskLogger.addImageStatusListener(status -> isAllImagesUpdated(status, service, image));
	}

	private boolean isAllImagesUpdated(Collection<String> imagesInService, Service service, String oldImage) {
		if (!imagesInService.contains(oldImage)) {
			logger.info(service + " rolled");
			endListener.accept(service);
			return true;
		}
		return false;
	}

	private String changeVersion(String image) {
		String[] split = image.split(":");
		String version = split[1];
		version = version.equals("1") ? "2" : "1";
		return split[0] + ":" + version;
	}
}
