package com.pbh.master.env.log;

import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import com.github.dockerjava.api.model.Container;
import com.github.dockerjava.api.model.Frame;
import com.github.dockerjava.core.command.LogContainerResultCallback;
import com.pbh.master.env.ConfigurationProvider;
import com.pbh.master.env.Factory;

public class LogContainerCallback extends LogContainerResultCallback {

	private Container container;
	private ConfigurationProvider conf;
	private FileOutputStream outputStream;
	private ProgressLogger logger;

	public LogContainerCallback(Factory factory, ConfigurationProvider conf, Container container) {
		this.conf = conf;
		this.container = container;
		logger = factory.getProgressLogger();
	}

	@Override
	public void onNext(Frame item) {
		try {
			outputStream.write(item.getPayload());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onComplete() {
		
		try {
			outputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		super.onComplete();
	}

	@Override
	public void onStart(Closeable stream) {
		try {
			//Long created = container.getCreated();
			String image = container.getImage().replace(":", "_").replace("/", "-");
			String state = container.getState();
			String id = container.getId();
			
			String fileName = image + "-" + state + "-" + id + ".log";
			
			File file = new File(conf.getOutputFolder(), fileName);
			file.createNewFile();
			outputStream = new FileOutputStream(file);
			
			logger.info("Getting Docker log: " + file.getName());
			
			super.onStart(stream);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
