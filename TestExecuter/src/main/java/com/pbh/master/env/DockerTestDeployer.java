package com.pbh.master.env;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.amazonaws.services.ec2.model.Instance;
import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.CreateContainerResponse;
import com.github.dockerjava.api.model.ExposedPort;
import com.github.dockerjava.api.model.Ports;
import com.github.dockerjava.api.model.Ports.Binding;
import com.github.dockerjava.core.command.WaitContainerResultCallback;
import com.pbh.master.env.log.ProgressLogger;

public class DockerTestDeployer implements TestDeployer {

	private List<Instance> instances;
	private Factory dockerClientFactory;
	private ConfigurationProvider conf;
	private String clientContainerid;
	private ProgressLogger logger;

	public DockerTestDeployer(ConfigurationProvider conf, Factory dockerClient, List<Instance> instances) {
		this.conf = conf;
		this.dockerClientFactory = dockerClient;
		this.instances = instances;
		logger = dockerClient.getProgressLogger();
	}

	public void deploy(String loadBalancerDNSName) {

		logger.info("Starting test against " + loadBalancerDNSName);

		for (int i = 0; i < instances.size() - 1; i++) {
			Instance slaveInstance = instances.get(i);

			Ports portBindings = createPortBindings(slaveInstance);
			
			List<String> slaveEnv = createSlaveEnv(loadBalancerDNSName, slaveInstance.getPublicDnsName());

			DockerClient client = dockerClientFactory.getClient(slaveInstance);

			CreateContainerResponse container = client
					.createContainerCmd("bechhansen/master-slave:latest")
					.withNetworkMode("host")
					.withPortBindings(portBindings)
					.withExposedPorts(ExposedPort.tcp(1099),ExposedPort.tcp(1100),ExposedPort.tcp(1101),ExposedPort.tcp(1102), ExposedPort.tcp(60000))
					.withEnv(slaveEnv).exec();

			clientContainerid = container.getId();

			logger.info("Start test slave " + (i + 1) + " on " + slaveInstance.getPublicDnsName());
			client.startContainerCmd(container.getId()).exec();

		}

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		
		Instance clientInstance = instances.get(instances.size() - 1);
		Ports portBindings = createPortBindings(clientInstance);
		DockerClient client = dockerClientFactory.getClient(clientInstance);

		List<String> clientEnv = createClientEnv(loadBalancerDNSName, clientInstance.getPublicDnsName());

		CreateContainerResponse container = client
				.createContainerCmd("bechhansen/master-client:latest")
				.withPortBindings(portBindings)
				.withNetworkMode("host")
				.withExposedPorts(ExposedPort.tcp(1099),ExposedPort.tcp(1100),ExposedPort.tcp(1101),ExposedPort.tcp(1102), ExposedPort.tcp(60000))
				.withEnv(clientEnv).exec();

		clientContainerid = container.getId();

		logger.info("Client instance is " + clientInstance.getPublicDnsName());
		
		logger.info("Running test...");
		client.startContainerCmd(container.getId()).exec();
		WaitContainerResultCallback exec = client.waitContainerCmd(container.getId()).exec(new WaitContainerResultCallback());
		try {
			exec.awaitCompletion();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		logger.info("Test Done");
	}

	private Ports createPortBindings(Instance clientInstance) {
		
		Ports portBindings = new Ports();
		portBindings.bind(ExposedPort.tcp(1099), Binding.bindPort(1099));
		portBindings.bind(ExposedPort.tcp(1100), Binding.bindPort(1100));
		portBindings.bind(ExposedPort.tcp(1101), Binding.bindPort(1101));
		portBindings.bind(ExposedPort.tcp(1102), Binding.bindPort(1102));
		portBindings.bind(ExposedPort.tcp(60000), Binding.bindPort(60000));
		
		return portBindings;
	}

	private List<String> createClientEnv(String loadBalancerDNSName, String ip) {
		List<String> env = createEnv(loadBalancerDNSName);
		env.add("IP=" + ip);

		List<Instance> subList = instances.subList(0, instances.size() - 1);
		if (!subList.isEmpty()) {
			String slaves = subList.stream().map(inst -> inst.getPublicDnsName()).map(inst -> inst + ":1099").collect(Collectors.joining(","));
			env.add("JMETER_ENV=-R" + slaves + " -X -Jserver.rmi.ssl.disable=true -Jclient.rmi.localport=60000 -Jmode=" + conf.getJMeterMode());
		}
		return env;
	}

	private List<String> createSlaveEnv(String loadBalancerDNSName, String ip) {
		List<String> env = createEnv(loadBalancerDNSName);
		env.add("IP=" + ip);
		env.add("JMETER_ENV=-Jserver.rmi.ssl.disable=true -Jserver.rmi.localport=1100 -Jclient.rmi.localport=60000 -Jmode=" + conf.getJMeterMode());
		
		// -Jserver.exitaftertest=true

		return env;
	}

	private List<String> createEnv(String loadBalancerDNSName) {
		List<String> env = new ArrayList<>();
		env.add("TEST_HOST=" + loadBalancerDNSName);
		env.add("TEST_DURATION=" + conf.getTestDuration());
		env.add("TEST_THREADS=" + conf.getTestThreads());
		env.add("TEST_RAMP_UP_SECONDS=" + conf.getTestRampUpSeconds());
		return env;
	}

	public void getTestResult() {

		try {
			File file = new File(conf.getOutputFolder(), "results.jtl");
			Instance clientInstance = instances.get(instances.size() - 1);
			InputStream resultStream = dockerClientFactory.getClient(clientInstance).copyArchiveFromContainerCmd(clientContainerid, "/data/results.jtl").exec();
			java.nio.file.Files.copy(resultStream, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException /*| NotFoundException*/ e) {
			e.printStackTrace();
		}
	}
}
