package com.pbh.master.env.aws;

import java.util.List;

import com.amazonaws.services.ec2.model.Instance;

public interface LoadbalancerCreator {

	String createLoadbalancer(List<Instance> instances);

	void waitUntilLoadBalancerReady();

	void removeLoadBalancer();

}
