package com.pbh.master.env;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import com.amazonaws.services.ec2.model.Instance;
import com.pbh.master.env.log.ProgressLogger;

public class PrototypeStandaloneDeployer implements PrototypeDeployer {

	private List<Instance> instances;
	private Factory factory;
	private ProgressLogger logger;

	public PrototypeStandaloneDeployer(Factory factory, List<Instance> instances) {
		this.factory = factory;
		this.instances = instances;
		logger = factory.getProgressLogger();
	}

	public Optional<ServiceDepoyerResponse> deploy() {
		
		try {
			logger.info("Starting standalone containers as standalone container on every node");
			
			ExecutorService executor = Executors.newFixedThreadPool(instances.size());
			List<PrototypeDeployerCallable> collect = instances.stream().map(instance-> new PrototypeDeployerCallable(factory, instance)).collect(Collectors.toList());
			executor.invokeAll(collect);
			executor.shutdown();
			
			logger.info("Finished starting standalone containers for all servers");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		return Optional.empty();
		
	}

}
