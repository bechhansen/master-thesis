package com.pbh.master.env.churn;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.amazonaws.services.ec2.model.Instance;
import com.pbh.master.env.Factory;
import com.pbh.master.env.Service;
import com.pbh.master.env.ServiceDepoyerResponse;
import com.pbh.master.env.log.TaskLogger;

public class UngracefullChurnInternalGenerator implements ChurnGenerator {

	private List<Instance> swarmInstances;
	private Factory factory;
	private TaskLogger taskLogger;
	private ScheduledExecutorService scheduler;

	public UngracefullChurnInternalGenerator(Factory factory, List<Instance> swarmInstances, ServiceDepoyerResponse services, TaskLogger taskLogger) {
		this.factory = factory;
		this.swarmInstances = swarmInstances;
		this.taskLogger = taskLogger;
		
		factory.getProgressLogger().info("Start UngracefullChurnInternalGenerator churn");
		scheduler = Executors.newScheduledThreadPool(4);
	}
	
	public void stopChurn() {
		scheduler.shutdown();
	}

	public void generateChurn() {
		scheduleKill(Service.SERVICE2, 30);
		scheduleKill(Service.SERVICE3, 35);
	}

	private void scheduleKill(Service service, int seconds) {
		KillChurnRunable killer = new KillChurnRunable(
				swarmInstances, 
				factory, 
				taskLogger, 
				service, 
				s -> scheduleKill(s, 25));
		
		scheduler.schedule(killer, seconds, TimeUnit.SECONDS);
	}
}
