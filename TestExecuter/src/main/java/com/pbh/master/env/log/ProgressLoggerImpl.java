package com.pbh.master.env.log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import com.pbh.master.env.ConfigurationProvider;

public class ProgressLoggerImpl implements ProgressLogger {
	
	private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("mm:ss");

	private FileOutputStream outputStream;
	private long currentTimeMillis;

	public ProgressLoggerImpl(ConfigurationProvider configurationProvider) {
		
		currentTimeMillis = System.currentTimeMillis();

		try {
			File file = new File(configurationProvider.getOutputFolder(), "TestLog.txt");
			if (file.isFile() && file.exists()) {
				file.delete();
			}
			file.createNewFile();
			outputStream = new FileOutputStream(file);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void info(String info) {
		
		long elapsedTime = System.currentTimeMillis() - currentTimeMillis;
		
		LocalDateTime date =
			    Instant.ofEpochMilli(elapsedTime).atZone(ZoneId.systemDefault()).toLocalDateTime();
		
		String timeString = date.format(formatter);
		
		StringBuilder b = new StringBuilder();
		b.append(timeString)
		.append(" ")
		.append(info).append(System.lineSeparator());
		
		String out = b.toString();
		System.out.print(out);
		
		try {
			outputStream.write(out.getBytes());
			outputStream.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
