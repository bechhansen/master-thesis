package com.pbh.master.env;

import com.github.dockerjava.api.model.UpdateOrder;

public enum Service {

	SERVICE1("bechhansen/master-service1", false, UpdateOrder.START_FIRST, 2),
	SERVICE2("bechhansen/master-service2", false, UpdateOrder.START_FIRST, 2),
	SERVICE3("bechhansen/master-service3", false, UpdateOrder.START_FIRST, 2),
	AUTH("bechhansen/master-auth", false, UpdateOrder.START_FIRST, 1),
	
	TEST_SLAVE("bechhansen/master-slave", true, UpdateOrder.STOP_FIRST, 1),
	TEST_CLIENT("bechhansen/master-client", true, UpdateOrder.STOP_FIRST, 1);
	
	private String image;
	private boolean isTest;
	private UpdateOrder order;
	private int updateParallelism;

	Service(String image, boolean isTest, UpdateOrder order, int updateParallelism) {
		this.image = image;
		this.isTest = isTest;
		this.order = order;
		this.updateParallelism = updateParallelism;
	}

	public String getImage() {
		return image;
	}

	public static Service parse(String image) {

		String[] split = image.split(":");
		
		for (Service service : Service.values()) {
			if (service.getImage().equals(split[0])) {
				return service;
			}
		}
		return null;
	}

	public boolean isTest() {
		return isTest;
	}

	public String readableName() {
		return name().substring(0, 1).toUpperCase() + name().substring(1);
	}

	public UpdateOrder getUpdateOrder() {
		return order;
	}

	public int getUpdateParallelism() {
		return updateParallelism;
	}
}
