package com.pbh.master.analyser;

public class MemoryInfo {

	private long usage;
	private long free;
	private long total;
	private long max;
	
	public MemoryInfo() {
		//Default constructor
	}

	public MemoryInfo(long usage, long free, long total, long max) {
		this.setUsage(usage);
		this.setFree(free);
		this.setTotal(total);
		this.setMax(max);
	}

	public long getUsage() {
		return usage;
	}

	public void setUsage(long usage) {
		this.usage = usage;
	}

	public long getFree() {
		return free;
	}

	public void setFree(long free) {
		this.free = free;
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public long getMax() {
		return max;
	}

	public void setMax(long max) {
		this.max = max;
	}
}
