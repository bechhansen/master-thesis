package com.pbh.master.analyser;

import org.apache.commons.lang3.tuple.Pair;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class Result {

    private Map<Pair<String, String>, AtomicInteger> hostConnections = new HashMap<>();
    private Map<Pair<String, String>, AtomicInteger> containerConnections = new HashMap<>();

    public void addHostConnection(String s1, String s2) {
       add(s1, s2, hostConnections);
    }

    public void addContainerConnection(String s1, String s2) {
        add(s1, s2, containerConnections);
    }

    private void add(String s1, String s2, Map<Pair<String, String>, AtomicInteger> map) {

        Pair<String, String> key = Pair.of(s1, s2);
        AtomicInteger atomicInteger = map.get(key);
        if (atomicInteger == null) {
            atomicInteger = new AtomicInteger();
            map.put(key, atomicInteger);
        }
        atomicInteger.incrementAndGet();
    }

	public Map<Pair<String, String>, AtomicInteger> getHostConnections() {
		return hostConnections;
	}

	public Map<Pair<String, String>, AtomicInteger> getContainerConnections() {
		return containerConnections;
	}
}