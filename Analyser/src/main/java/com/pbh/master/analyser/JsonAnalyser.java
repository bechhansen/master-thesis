package com.pbh.master.analyser;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.Pair;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonAnalyser {

	public static void main(String[] args) {

		String folder = args[0];

		JsonAnalyser jsonAnalyser = new JsonAnalyser();
		jsonAnalyser.analyse(folder);

	}

	private void analyse(String folder) {

		ObjectMapper mapper = new ObjectMapper();

		Path folderPath = Paths.get(folder);

		Result result = new Result();

		try (Stream<Path> walk = Files.walk(folderPath)) {

			Stream<Path> fileStream = walk.filter(Files::isRegularFile).filter(path -> path.getFileName().toString().endsWith(".json"));

			ContainerNameConverter cnc = new ContainerNameConverter();

			fileStream.forEach(path -> {

				try (InputStream stream = new FileInputStream(path.toFile())) {

					List<LogResult> logs = mapper.readValue(stream, mapper.getTypeFactory().constructCollectionType(List.class, LogResult.class));

					for (LogResult log : logs) {

						String remoteHost = log.getRemoteHost();
						if (remoteHost != null) {
							String localName = log.getHostName();
							String service = log.getService();
							String remoteService = log.getRemoteService();
							result.addContainerConnection(service + "-" + cnc.convert(service, localName), remoteService + "-" + cnc.convert(remoteService, remoteHost));
						}

						String remoteDockerHost = log.getRemoteDockerHost();
						if (remoteDockerHost != null) {
							String localDockerHost = log.getDockerHost();
							result.addHostConnection(cnc.convert(localDockerHost), cnc.convert(remoteDockerHost));
						}
					}

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			});

			writeToDisk(folderPath, "ContainerConnections.csv", result.getContainerConnections());
			writeToDisk(folderPath, "NodeConnections.csv", result.getHostConnections());

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void writeToDisk(Path folderPath, String string, Map<Pair<String, String>, AtomicInteger> connections) throws IOException {

		Path path = Path.of(folderPath.toString(), string);

		try (BufferedWriter writer = Files.newBufferedWriter(path)) {

			for (Entry<Pair<String, String>, AtomicInteger> connection : connections.entrySet()) {
				Pair<String, String> key = connection.getKey();
				writer.write(key.getLeft() + "," + key.getRight() + "," + connection.getValue().get() + System.lineSeparator());
			}
		}
	}
}