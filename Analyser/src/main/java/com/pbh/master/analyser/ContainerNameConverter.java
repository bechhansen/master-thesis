package com.pbh.master.analyser;

import java.util.HashMap;
import java.util.Map;

public class ContainerNameConverter {
	
	private Map<String, Map<String, String>> names = new HashMap<>();
	
	private Map<String, String> hostNames = new HashMap<>();

	public String convert(String serviceName, String containerName) {
		
		Map<String, String> map = names.get(serviceName);
		if(map == null) {
			map = new HashMap<>();
			names.put(serviceName, map);
		}
		
		String string = map.get(containerName);
		if (string == null) {
			string = "" + (map.size() + 1);
			map.put(containerName, string);
		}
		return string;
	}
	
	public String convert(String hostName) {

		String string = hostNames.get(hostName);
		if (string == null) {
			string = "Node-" + (hostNames.size() + 1);
			hostNames.put(hostName, string);
		}
		return string;
	}
}
