package com.pbh.master.analyser;

public class LogResult {

	private String service;
	private String message;
	private String correlationId;
	private Integer level;
	private String hostName;
	private String version;
	private MemoryInfo memInfo;
	private String dockerHost;
	private String remoteDockerHost;
	private String remoteHost;
	private Integer httpCode;
	private String remoteService;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getCorrelationId() {
		return correlationId;
	}

	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public MemoryInfo getMemInfo() {
		return memInfo;
	}

	public void setMemInfo(MemoryInfo memInfo) {
		this.memInfo = memInfo;
	}

	public String getDockerHost() {
		return dockerHost;
	}

	public void setDockerHost(String dockerHost) {
		this.dockerHost = dockerHost;
	}

	public String getRemoteDockerHost() {
		return remoteDockerHost;
	}

	public void setRemoteDockerHost(String remoteDockerHost) {
		this.remoteDockerHost = remoteDockerHost;
	}

	public String getRemoteHost() {
		return remoteHost;
	}

	public void setRemoteHost(String remoteHost) {
		this.remoteHost = remoteHost;
	}

	public Integer getHttpCode() {
		return httpCode;
	}

	public void setHttpCode(Integer httpCode) {
		this.httpCode = httpCode;
	}

	public String getRemoteService() {
		return remoteService;
	}

	public void setRemoteService(String remoteService) {
		this.remoteService = remoteService;
	}
}
