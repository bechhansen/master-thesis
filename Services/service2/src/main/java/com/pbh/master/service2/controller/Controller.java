package com.pbh.master.service2.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pbh.master.shared.Logger;
import com.pbh.master.shared.ServiceFaildException;
import com.pbh.master.shared.SharedService;
import com.pbh.master.shared.model.LogResult;

@RestController
public class Controller {

	@Autowired
	private SharedService sharedService;

	@Autowired
	private Logger logger;

	@GetMapping("/endpoint1")
	public ResponseEntity<Collection<LogResult>> endpoint1() {

		logger.info("Endpoint1 called. Sleeping for 150-200 ms");
		sharedService.sleep(150, 200);

		try {
			sharedService.callService("service3", "endpoint1");
			return ResponseEntity.ok().headers(sharedService.getResultHeaders()).body(logger.getLogResults());

		} catch (ServiceFaildException e) {
			return ResponseEntity.status(e.getStatus()).headers(sharedService.getResultHeaders()).body(logger.getLogResults());
		}
	}
}