package com.pbh.master.service1.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pbh.master.shared.Logger;
import com.pbh.master.shared.ServiceFaildException;
import com.pbh.master.shared.SharedService;
import com.pbh.master.shared.model.LogResult;

@RestController
public class Controller {

	@Autowired
	private SharedService sharedService;

	@Autowired
	private Logger logger;

	@GetMapping("/endpoint1")
	public ResponseEntity<Collection<LogResult>> endpoint1() {
		logger.info("Endpoint1 called. Sleeping for 80-100 ms");
		sharedService.sleep(80, 100);

		try {
			if (sharedService.verifyAccessToken()) {
				sharedService.callService("service2", "endpoint1");
				sharedService.callService("service3", "endpoint2");
				return ResponseEntity.ok().headers(sharedService.getResultHeaders()).body(logger.getLogResults());
			} else {
				return ResponseEntity.status(HttpStatus.FORBIDDEN).headers(sharedService.getResultHeaders()).body(logger.getLogResults());
			}

		} catch (ServiceFaildException e) {
			return ResponseEntity.status(e.getStatus()).headers(sharedService.getResultHeaders()).body(logger.getLogResults());
		}
	}
}