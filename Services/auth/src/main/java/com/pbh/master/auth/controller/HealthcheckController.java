package com.pbh.master.auth.controller;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pbh.master.shared.Logger;
import com.pbh.master.shared.model.MemoryInfo;
import com.pbh.master.shared.model.StatusInfo;

@RestController
public class HealthcheckController {
	
	@Autowired
	private Logger logger;
	
	private static final org.slf4j.Logger log = LoggerFactory.getLogger(HealthcheckController.class);

	@RequestMapping("/healthcheck")
	public ResponseEntity<String> healthcheck() {
		return new ResponseEntity<String>(HttpStatus.OK);
	}
	
	@RequestMapping("/status")
	public ResponseEntity<StatusInfo> status() {
		StatusInfo info = logger.getStatusInfo();
		return ResponseEntity.ok().body(info);
	}
}
