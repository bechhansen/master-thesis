package com.pbh.master.auth.controller;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pbh.master.shared.HeaderProvider;
import com.pbh.master.shared.Logger;
import com.pbh.master.shared.SharedService;
import com.pbh.master.shared.model.LogResult;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@RestController
public class AuthController {

	@Autowired
	private SharedService sharedService;

	@Autowired
	private Logger logger;

	@GetMapping("/login")
	public ResponseEntity<Collection<LogResult>> login(@RequestParam String userName) {
		Date now = new Date();
		Date accessExpiryDate = new Date(now.getTime() + 86400000L);

		String accessToken = Jwts.builder().setSubject(userName).setIssuedAt(new Date()).setExpiration(accessExpiryDate).signWith(SignatureAlgorithm.HS512, "Secret").compact();

		logger.info("User " + userName + " logged in");
		
		HttpHeaders headers = sharedService.getResultHeaders();
		headers.add("Authorization", "Bearer " + accessToken);
		return ResponseEntity.ok().headers(headers).body(logger.getLogResults());
	}

	@GetMapping("/verify")
	public ResponseEntity<Collection<LogResult>> verify() {

		HeaderProvider headerProvider = sharedService.getHeaderProvider();

		try {
			String authorization = headerProvider.getAuthorization();

			if (authorization != null && authorization.startsWith("Bearer ")) {
				authorization = authorization.substring("Bearer ".length());

				Claims claims = Jwts.parser().setSigningKey("Secret").parseClaimsJws(authorization).getBody();
				String subject = claims.getSubject();

				logger.info("Authorization successfull for user " + subject);

				return ResponseEntity.ok().headers(sharedService.getResultHeaders()).body(logger.getLogResults());
			}

			logger.info("No JWT token found");

		} catch (Exception e) {
			logger.info("Authorization failed. " + e.getMessage());
			e.printStackTrace();
		}

		return ResponseEntity.status(HttpStatus.FORBIDDEN).headers(sharedService.getResultHeaders()).body(logger.getLogResults());
	}

}