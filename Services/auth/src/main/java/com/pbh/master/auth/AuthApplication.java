package com.pbh.master.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.retry.annotation.EnableRetry;

//@EnableConfigurationProperties(AppProperties.class)
@SpringBootApplication(scanBasePackages = { "com.pbh.master" })
@EnableRetry
public class AuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuthApplication.class, args);
	}

}
