package com.pbh.master.shared.model;

public class StatusInfo {

	private MemoryInfo memoryInfo;
	private String hostName;
	private String dockerHost;
	private String authIp;
	private String service1Ip;
	private String service2Ip;
	private String service3Ip;

	public StatusInfo(MemoryInfo memoryInfo, String hostName, String dockerHost, String authIp, String service1Ip, String service2Ip, String service3Ip) {
		this.setAuthIp(authIp);
		this.setService1Ip(service1Ip);
		this.setService2Ip(service2Ip);
		this.setService3Ip(service3Ip);
		this.setMemoryInfo(memoryInfo);
		this.setHostName(hostName);
		this.setDockerHost(dockerHost);
	}

	public MemoryInfo getMemoryInfo() {
		return memoryInfo;
	}

	public void setMemoryInfo(MemoryInfo memoryInfo) {
		this.memoryInfo = memoryInfo;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getDockerHost() {
		return dockerHost;
	}

	public void setDockerHost(String dockerHost) {
		this.dockerHost = dockerHost;
	}

	public String getAuthIp() {
		return authIp;
	}

	public void setAuthIp(String authIp) {
		this.authIp = authIp;
	}

	public String getService1Ip() {
		return service1Ip;
	}

	public void setService1Ip(String service1Ip) {
		this.service1Ip = service1Ip;
	}

	public String getService2Ip() {
		return service2Ip;
	}

	public void setService2Ip(String service2Ip) {
		this.service2Ip = service2Ip;
	}

	public String getService3Ip() {
		return service3Ip;
	}

	public void setService3Ip(String service3Ip) {
		this.service3Ip = service3Ip;
	}

}
