package com.pbh.master.shared;

public interface HeaderProvider {

	String getAuthorization();

	Integer getLevel();

	String getCorrelationId();

}
