package com.pbh.master.shared;

import java.util.Collection;

import com.pbh.master.shared.model.LogResult;
import com.pbh.master.shared.model.MemoryInfo;
import com.pbh.master.shared.model.StatusInfo;

public interface Logger {

	void info(String message);

	void info(Collection<LogResult> logs);

	void infoRestCall(String message, String remoteDockerhost, String remoteHost, String remoteService, Integer returnCode);

	Collection<LogResult> getLogResults();
	
	MemoryInfo getMemoryInfo();
	
	StatusInfo getStatusInfo();

}
