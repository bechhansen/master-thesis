package com.pbh.master.shared;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.RequestScope;

import com.pbh.master.shared.model.LogResult;
import com.pbh.master.shared.model.MemoryInfo;
import com.pbh.master.shared.model.StatusInfo;

@Service
@RequestScope
public class LoggerImpl implements Logger {

	private static int mb = 1024 * 1024;

	@Value("${serviceName:Unknown Service}")
	private String serviceName;

	@Value("${version:}")
	private String version;
	
	@Value("${dockerHost:}")
	private String dockerHost;

	@Autowired
	private SharedService headerService;

	private Collection<LogResult> logResults = new ArrayList<>();

	@Override
	public void info(String message) {

		MemoryInfo memInfo = getMemoryInfo();
		String hostName = getHostName();
		HeaderProvider headerProvider = headerService.getHeaderProvider();
		logResults.add(new LogResult(serviceName, version, message, dockerHost, hostName, memInfo, headerProvider));
	}
	
	@Override
	public void infoRestCall(String message, String remoteDockerhost, String remoteHost, String remoteService, Integer returnCode) {

		MemoryInfo memInfo = getMemoryInfo();
		String hostName = getHostName();
		HeaderProvider headerProvider = headerService.getHeaderProvider();
		logResults.add(new LogResult(serviceName, version, message, dockerHost, hostName, remoteDockerhost, remoteHost, remoteService, returnCode, memInfo, headerProvider));
	}

	private String getHostName() {
		try {
			return InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			return "NoHost";
		}
	}

	@Override
	public Collection<LogResult> getLogResults() {
		return logResults;
	}

	@Override
	public void info(Collection<LogResult> logs) {
		logResults.addAll(logs);
	}

	public MemoryInfo getMemoryInfo() {
		Runtime runtime = Runtime.getRuntime();

		long usage = (runtime.totalMemory() - runtime.freeMemory()) / mb;
		long free = runtime.freeMemory() / mb;
		long total = runtime.totalMemory() / mb;
		long max = runtime.maxMemory() / mb;
		
		return new MemoryInfo(usage, free, total, max);
	}
	
	public StatusInfo getStatusInfo() {
		
		Optional<String> auth = getInetAddres("auth");
		Optional<String> service1 = getInetAddres("service1");
		Optional<String> service2 = getInetAddres("service2");
		Optional<String> service3 = getInetAddres("service3");
		
		return new StatusInfo(getMemoryInfo(), 
				getHostName(), 
				dockerHost, 
				auth.orElseGet(() -> "None"), 
				service1.orElseGet(() -> "None"), 
				service2.orElseGet(() -> "None"), 
				service3.orElseGet(() -> "None")); 
	}
	
	private Optional<String> getInetAddres(String host) {
		try {
			InetAddress byName = InetAddress.getByName(host);
			return Optional.of(byName.getHostAddress());
		} catch (UnknownHostException e) {
			return Optional.empty();
		}
	}
}
