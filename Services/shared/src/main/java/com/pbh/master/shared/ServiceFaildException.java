package com.pbh.master.shared;

import org.springframework.http.HttpStatus;

public class ServiceFaildException extends Exception {

	private HttpStatus status;

	public ServiceFaildException(HttpStatus status) {
		this.status = status;
	}

	public HttpStatus getStatus() {
		return status;
	}

	private static final long serialVersionUID = 1L;

}
