package com.pbh.master.shared;

import org.slf4j.LoggerFactory;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.RetryCallback;
import org.springframework.retry.RetryContext;
import org.springframework.retry.RetryListener;
import org.springframework.retry.backoff.FixedBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;

@Configuration
public class SharedConfiguration implements RetryListener  {
	
	private static final org.slf4j.Logger log = LoggerFactory.getLogger(SharedConfiguration.class);
	
	@Bean
    public GracefulShutdown gracefulShutdown() {
        return new GracefulShutdown();
    }

    @Bean
    public ConfigurableServletWebServerFactory webServerFactory(final GracefulShutdown gracefulShutdown) {
        
    	String dnsTTL = java.security.Security.getProperty("networkaddress.cache.ttl");
    	
    	int gracefulTimmeout = Integer.parseInt(System.getenv().getOrDefault("GRACEFUL_SHUTDOWN_TIMEOUT", "30"));
    	boolean graceful = Boolean.parseBoolean(System.getenv().getOrDefault("GRACEFUL_SHUTDOWN", "FALSE"));
    	boolean retry = Boolean.parseBoolean(System.getenv().getOrDefault("RETRY", "FALSE"));
    	log.info("GRACEFUL_SHUTDOWN is " + graceful + " in " + gracefulTimmeout);
    	log.info("RETRY             is " + retry);
    	
    	log.info("DNS-TTL           is " + dnsTTL);
    	
    	TomcatServletWebServerFactory factory = new TomcatServletWebServerFactory();
        factory.addConnectorCustomizers(gracefulShutdown);
        return factory;
    }
    
	@Bean
	public RetryTemplate retryTemplate() {
		log.info("Configure retry");
		RetryTemplate retryTemplate = new RetryTemplate();
		
		FixedBackOffPolicy fixedBackOffPolicy = new FixedBackOffPolicy();
		fixedBackOffPolicy.setBackOffPeriod(100l);
		retryTemplate.setBackOffPolicy(fixedBackOffPolicy);
		
		SimpleRetryPolicy retryPolicy = new SimpleRetryPolicy();
		retryPolicy.setMaxAttempts(5);
		retryTemplate.setRetryPolicy(retryPolicy);
		
		retryTemplate.registerListener(this);
		
		return retryTemplate;
	}

	@Override
	public <T, E extends Throwable> boolean open(RetryContext context, RetryCallback<T, E> callback) {
		//log.info("RetryTemplate Open");
		return true;
	}

	@Override
	public <T, E extends Throwable> void close(RetryContext context, RetryCallback<T, E> callback, Throwable throwable) {
		//log.info("RetryTemplate Close");	
	}

	@Override
	public <T, E extends Throwable> void onError(RetryContext context, RetryCallback<T, E> callback, Throwable throwable) {
		int retryCount = context.getRetryCount();
		
		log.info("RetryTemplate Error: Count=" + retryCount);
	}
}