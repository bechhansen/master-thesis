package com.pbh.master.shared;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Random;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.RetryCallback;
import org.springframework.retry.RetryContext;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.annotation.RequestScope;

import com.pbh.master.shared.model.LogResult;

@Service
@RequestScope
public class SharedService {

	private static final String CONNECTION_HEADER = "Connection";

	@Autowired
	private Logger logger;
	
	@Autowired
	RetryTemplate retryTemplate;

	@Autowired
	private HttpServletRequest request;

	@Value("${serviceName:Unknown Service}")
	private String serviceName;

	@Value("${dockerHost:}")
	private String dockerHost;

	public static final String AUTHORIZATION = "Authorization";
	public static final String CORRELATION_ID = "CorrelationId";
	public static final String HOST_ID = "Host";
	public static final String DOCKEHOST_ID = "DockerHost";
	public static final String SERVICE_ID = "Service";
	public static final String LEVEL = "Level";
	
	private boolean keepAlive = Boolean.parseBoolean(System.getenv().getOrDefault("KEEP_ALIVE", "FALSE"));
	private boolean retryEnabled = Boolean.parseBoolean(System.getenv().getOrDefault("RETRY", "FALSE"));
	
	private RestTemplate restTemplate;

	private HeaderProvider headerProvider;
	
	private static final org.slf4j.Logger log = LoggerFactory.getLogger(SharedService.class);

	public SharedService(RestTemplateBuilder restTemplateBuilder) {
		this.restTemplate = restTemplateBuilder.build();
	}

	public HttpHeaders createClientHeader() {

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

		String correlationId = getHeaderProvider().getCorrelationId();
		headers.set(CORRELATION_ID, correlationId);

		String authorization = getHeaderProvider().getAuthorization();
		if (authorization != null) {
			headers.set(AUTHORIZATION, authorization);
		}

		Integer level = getHeaderProvider().getLevel();
		headers.set(LEVEL, "" + (level.intValue() + 1));
		
		if(!keepAlive) {
			headers.add(CONNECTION_HEADER, "close");
		}

		return headers;
	}

	public HeaderProvider getHeaderProvider() {

		if (headerProvider == null) {
			String auth = request.getHeader(AUTHORIZATION);

			String correlationId = getCorrelationId(request);

			String step = request.getHeader(LEVEL);
			if (step == null) {
				step = "0";
			}

			Integer stepValue = Integer.parseInt(step);

			headerProvider = new HeaderProvider() {

				@Override
				public String getAuthorization() {
					return auth;
				}

				@Override
				public Integer getLevel() {
					return stepValue;
				}

				@Override
				public String getCorrelationId() {
					return correlationId;
				}
			};
		}

		return headerProvider;
	}

	private String getCorrelationId(HttpServletRequest request) {
		String correlationId = request.getHeader(CORRELATION_ID);
		if (correlationId == null) {
			correlationId = UUID.randomUUID().toString();
		}
		return correlationId;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void callService(String serviceHost, String serviceName) throws ServiceFaildException {
		String url = "http://" + serviceHost + ":8080/" + serviceName;
		
		try {
			logger.info("Calling " + serviceName + " at " + serviceHost);
			HttpHeaders headers = createClientHeader();
			HttpEntity<?> entity = new HttpEntity<>(headers);
			
			HttpStatus status = callRestEndpointWithRetry(retryEnabled, serviceHost, serviceName, entity, url);
			
			if (!status.is2xxSuccessful()) {
				throw new ServiceFaildException(status);
			}
		
		} catch (ServiceFaildException e) {
			logger.info("Faild calling " + url + ". Exception: " + e.toString() + ". StatusCode: " + e.getStatus());
			throw e;
		} catch (Exception e) {
			
			log.error("Faild calling " + url, e);
			
			logger.info("Faild calling " + url + ". Exception: " + e.toString() + ". Status: " + e.getMessage());
			throw new ServiceFaildException(HttpStatus.FAILED_DEPENDENCY);
		}
	}
	
	private HttpStatus callRestEndpointWithRetry(boolean retryEnabled, 
			String serviceHost, String serviceName, 
			HttpEntity<?> entity, String url) {
		
		if (retryEnabled) {
			return retryTemplate.execute(new RetryCallback<HttpStatus, RuntimeException>() {
   
				@Override
			    public HttpStatus doWithRetry(RetryContext context) {
			    	return callRestEndpoint(serviceHost, serviceName, entity, url);
			    }
			});
		} else {
			return callRestEndpoint(serviceHost, serviceName, entity, url);
		}
	}

	private HttpStatus callRestEndpoint(String serviceHost, String serviceName, HttpEntity<?> entity, String url) {
		try {
			ResponseEntity<Collection> exchange = restTemplate.exchange(url, HttpMethod.GET, entity, Collection.class);
	
			int statusCodeValue = exchange.getStatusCodeValue();
			HttpHeaders resultheaders = exchange.getHeaders();
			String remoteHost = resultheaders.getFirst(HOST_ID);
			String remoteDockerHost = resultheaders.getFirst(DOCKEHOST_ID);
			String remoteService = resultheaders.getFirst(SERVICE_ID);
	
			Collection<LogResult> body = exchange.getBody();
			logger.info(body);
			logger.infoRestCall("Calling " + serviceName + " at " + serviceHost + " done", remoteDockerHost, remoteHost, remoteService, statusCodeValue);
		
			return exchange.getStatusCode();
			
		} catch (HttpClientErrorException | HttpServerErrorException e) {
				
			log.error("callRestEndpoint Faild calling " + url, e);
			
			logger.info(e.getResponseBodyAsString());
			logger.info("Calling " + serviceName + " at " + serviceHost + " done with errors");
			return e.getStatusCode();
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public boolean verifyAccessToken() {

		HttpHeaders headers = createClientHeader();
		HttpEntity<?> entity = new HttpEntity<>(headers);

		try {
			ResponseEntity<Collection> exchange = restTemplate.exchange("http://auth:8080/verify", HttpMethod.GET, entity, Collection.class);
			logger.info(exchange.getBody());

			int statusCodeValue = exchange.getStatusCodeValue();
			HttpHeaders resultheaders = exchange.getHeaders();
			String remoteHost = resultheaders.getFirst(HOST_ID);
			String remoteDockerHost = resultheaders.getFirst(DOCKEHOST_ID);
			String remoteService = resultheaders.getFirst(SERVICE_ID);

			logger.infoRestCall("Verified access token successful from Auth service", remoteDockerHost, remoteHost, remoteService, statusCodeValue);
			return true;
		} catch (HttpClientErrorException | HttpServerErrorException e) {
			logger.info("Verification of access token faild at Auth service. Status: " + e.getRawStatusCode());
			return false;
		} catch (Exception e) {
			logger.info("Unable to verify access token " + e.getMessage());
			return false;
		}
	}

	public void sleep(int min, int max) {
		Random r = new Random();
		int sleepTime = r.nextInt(max - min) + min;

		try {
			Thread.sleep(sleepTime);
		} catch (InterruptedException e) {
			logger.info("Sleep was interrupted");
		}
	}

	public HttpHeaders getResultHeaders() {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(HOST_ID, getHostName());
		httpHeaders.add(DOCKEHOST_ID, dockerHost);
		httpHeaders.add(SERVICE_ID, serviceName);
		
		if(!keepAlive) {
			httpHeaders.add(CONNECTION_HEADER, "close");
		}
		
		return httpHeaders;
	}

	private String getHostName() {
		try {
			return InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			return "NoHost";
		}
	}
}
