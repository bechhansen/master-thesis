package com.pbh.master.shared;

import org.apache.coyote.http11.AbstractHttp11Protocol;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TomcatCustomizer implements WebServerFactoryCustomizer<TomcatServletWebServerFactory> {

	String keepAliveEnv = System.getenv().getOrDefault("KEEP_ALIVE", "FALSE");
	private boolean keepAlive = Boolean.parseBoolean(keepAliveEnv);
	private static final org.slf4j.Logger log = LoggerFactory.getLogger(TomcatCustomizer.class);

    @Override
    public void customize(TomcatServletWebServerFactory factory) {
        factory.addConnectorCustomizers(connector -> {
            @SuppressWarnings("rawtypes")
			AbstractHttp11Protocol protocol = (AbstractHttp11Protocol) connector.getProtocolHandler();

            if(!keepAlive) {
            	protocol.setMaxKeepAliveRequests(1);
            	protocol.setKeepAliveTimeout(0);
            	protocol.setAcceptCount(10000);
            	protocol.setAcceptorThreadPriority(Thread.MAX_PRIORITY);
            	protocol.setConnectionTimeout(600000);
            }
            
            log.info("####################################################################################");
            log.info("# Keepalive = " + keepAlive);
            log.info("# maxKeepAliveRequests {}", protocol.getMaxKeepAliveRequests());
            log.info("# keepalive timeout: {} ms", protocol.getKeepAliveTimeout());
            log.info("# connection timeout: {} ms", protocol.getConnectionTimeout());
            log.info("# max connections: {}", protocol.getMaxConnections());
            log.info("####################################################################################");

        });
    }
}