package com.pbh.master.shared;

import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.catalina.connector.Connector;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.embedded.tomcat.TomcatConnectorCustomizer;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;

public class GracefulShutdown implements TomcatConnectorCustomizer, ApplicationListener<ContextClosedEvent> {

	private static final org.slf4j.Logger log = LoggerFactory.getLogger(GracefulShutdown.class);
	private static final int TIMEOUT = 600;
	private volatile Connector connector;

	@Override
	public void customize(Connector connector) {
		this.connector = connector;
	}

	@Override
	public void onApplicationEvent(ContextClosedEvent event) {

		String gracefulEnv = System.getenv().getOrDefault("GRACEFUL_SHUTDOWN", "FALSE");
		String timeoutEnv = System.getenv().getOrDefault("GRACEFUL_SHUTDOWN_TIMEOUT", "30");
		boolean graceful = Boolean.parseBoolean(gracefulEnv);
		int gracefulTimeout = Integer.parseInt(timeoutEnv);
		
		long currentTimeMillis = System.currentTimeMillis();
		log.info("Shutting down. Gracefully = " + graceful + " in " + gracefulTimeout + "seconds");
		
		if (graceful) {
			
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			this.connector.pause();
			Executor executor = this.connector.getProtocolHandler().getExecutor();
			if (executor instanceof ThreadPoolExecutor) {
				try {
					log.info("Shutting down. Waiting for thread pool shutdown");
					ThreadPoolExecutor threadPoolExecutor = (ThreadPoolExecutor) executor;			
					threadPoolExecutor.shutdown();
					if (!threadPoolExecutor.awaitTermination(gracefulTimeout, TimeUnit.SECONDS)) {
						 log.warn("Tomcat thread pool did not shut down gracefully within "
						 + TIMEOUT + " seconds. Proceeding with forceful shutdown");

						threadPoolExecutor.shutdownNow();
						if (!threadPoolExecutor.awaitTermination(gracefulTimeout, TimeUnit.SECONDS)) {
							log.error("Tomcat thread pool did not terminate");
						}
					}
				} catch (InterruptedException ex) {
					log.error("InterruptedException", ex);
					Thread.currentThread().interrupt();
				}
			}
			
			long timeUsed = System.currentTimeMillis() - currentTimeMillis;
			log.info("Shutting down. Succes in " + timeUsed + " milliseconds");
		}
	}
}