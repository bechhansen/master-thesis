package com.pbh.master.service3.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pbh.master.shared.Logger;
import com.pbh.master.shared.SharedService;
import com.pbh.master.shared.model.LogResult;

@RestController
public class Controller {

	@Autowired
	private SharedService sharedService;

	@Autowired
	private Logger logger;

	@GetMapping("/endpoint1")
	public ResponseEntity<Collection<LogResult>> endpoint1() {

		logger.info("Endpoint1 called. Sleeping for 200-220 ms");
		sharedService.sleep(200, 220);

		if (sharedService.verifyAccessToken()) {
			return ResponseEntity.ok().headers(sharedService.getResultHeaders()).body(logger.getLogResults());
		} else {
			return ResponseEntity.status(HttpStatus.FORBIDDEN).headers(sharedService.getResultHeaders()).body(logger.getLogResults());
		}
	}

	@GetMapping("/endpoint2")
	public ResponseEntity<Collection<LogResult>> endpoint2() {
		
		logger.info("Endpoint2 called. Sleeping for 300-320 ms");
		sharedService.sleep(300, 320);
		
		return ResponseEntity.ok().headers(sharedService.getResultHeaders()).body(logger.getLogResults());
	}
}