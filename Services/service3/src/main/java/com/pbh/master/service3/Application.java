package com.pbh.master.service3;

import java.util.Optional;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.retry.annotation.EnableRetry;

@SpringBootApplication(scanBasePackages = { "com.pbh.master" })
@EnableRetry
public class Application {

	public static void main(String[] args) {
		
		Optional<Integer> dnsTTL = Optional.ofNullable(System.getenv().get("DNS_TTL")).map(Integer::parseInt);
		if (dnsTTL.isPresent()) {
			java.security.Security.setProperty("networkaddress.cache.ttl" , dnsTTL.get().toString());
		}
		
		SpringApplication.run(Application.class, args);
	}

}
